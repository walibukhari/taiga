<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorLoginController extends Controller
{
    public function doctorLoginForm() {
        return view('auth.doctorAuth.login');
    }
    public function doctorLogin(Request $request) {
        if(Auth::guard('doctor')->attempt(['email' => $request->email , 'password' => $request->password])) {
            return redirect(RouteServiceProvider::DOCTOR_HOME);
        } else {
            return back()->with('error_message','Credtional\'s Not Match');
        }
    }

    public function logoutDoctor()
    {
        $this->guard()->logout();
        $this->invalidSession();
        return redirect('/doctor-login');
    }

    public function guard(){
        return Auth::guard('doctor');
    }

    public function invalidSession(){
        session()->flush();
        return true;
    }
}
