<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index() {
        return view('doctor.doctor_dashboard');
    }

    public function patientList() {
        return view('doctor.doctor_patient_list');
    }

    public function profile() {
        return view('doctor.doctor_profile');
    }
}
