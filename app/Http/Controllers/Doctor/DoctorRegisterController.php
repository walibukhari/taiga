<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorRegisterController extends Controller
{
    public function doctorSignUpForm() {
        return view('auth.doctorAuth.register');
    }

    public function doctorRegister(Request $request){
        Doctor::registerDoctor($request->all());
        return redirect('/doctor-login');
    }
}
