<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Requests\PatientForm;

class PaitentController extends Controller
{

    public function CreatePatient()
    {
        return view('superadmin.create-patient');
    }

    public function AllPatients()
    {
        $patient = Patient::all();
        return view('superadmin.all-patients')->with('patient',$patient);
    }

    public function postPatients(PatientForm $request)
    {
        $data = Patient::createPatients($request->all());
        return back()->with('success_message','Patient Created Successfully');
    }

    public function deletePatients($id)
    {
        Patient::where('id','=',$id)->delete();
        return back()->with('success_message','Patient Deleted Successfully');
    }

    public function detailPatients($id)
    {
        $patients = Patient::where('id','=',$id)->first();
        return view('superadmin.patient_details',[
            'patients' => $patients
        ]);
    }
}
