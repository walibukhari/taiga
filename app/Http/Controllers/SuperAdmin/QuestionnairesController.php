<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Questionnair;
use App\Models\QuestionnaireQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionnairesController extends Controller
{
    public function index(){
        $questionnaire = Questionnair::where('folder_id','=',null)->get();
        $questionnaires_questions = QuestionnaireQuestion::all();
        return view('superadmin.questionnaire.index',[
            'questionnaire' => $questionnaire,
            'questionnaires_questions' => $questionnaires_questions
        ]);
    }

    public function getPreviousId($id){
        $questionnaire = Questionnair::where('id','=',$id)->first();
        return collect([
           'status' => true,
           'questionnaire' => $questionnaire
        ]);
    }

    public function getFolderData($id){
        $folderData = Questionnair::where('folder_id','=',$id)->get();
        $questionnaire = QuestionnaireQuestion::where('folder_id','=',$id)->get();
        return collect([
           'status' => true,
           'data' => $folderData,
           'questionnaire_questions' => $questionnaire
        ]);
    }

    public function addFolder(Request $request){
        if(Auth::user()) {
            $user_role = 'Super Admin';
        }
        Questionnair::create([
            'folder_name' => $request->folder_name,
            'folder_created_by' => $user_role,
            'slug' => $request->folder_name
        ]);
        $questionnaires = Questionnair::where('folder_id','=',null)->get();
        return collect([
            'status' => true,
            'data' => $questionnaires
        ]);
    }

    public function addFolderInsideFolder(Request $request){
        if(Auth::user()) {
            $user_role = 'Super Admin';
        }
        Questionnair::create([
            'folder_name' => $request->folder_name,
            'folder_created_by' => $user_role,
            'slug' => $request->folder_name,
            'folder_id' => $request->folder_id
        ]);
        $questionnaires = Questionnair::where('folder_id','=',null)->get();
        return collect([
            'status' => true,
            'data' => $questionnaires
        ]);
    }

    public function postQuestionnaire($id){
        return view('superadmin.questionnaire.add',[
            'id' => $id
        ]);
    }

    public function postFolderQuestionnaire($id,$folder_id){
        return view('superadmin.questionnaire.add',[
            'id' => $id,
            'folder_id' => $folder_id
        ]);
    }

    public function renameFolder(Request $request){
        Questionnair::where('id','=',$request->folder_id)->update([
            'folder_name' => $request->folder_name,
            'slug' => $request->folder_name
        ]);
        $questionnaires = Questionnair::where('folder_id','=',null)->get();
        return collect([
            'status' => true,
            'data' => $questionnaires
        ]);
    }

    public function deleteFolder($id){
        Questionnair::where('id','=',$id)->delete();
        $questionnaires = Questionnair::where('folder_id','=',null)->get();
        return collect([
            'status' => true,
            'data' => $questionnaires
        ]);
    }

    public function postAddQuestionnaire(Request $request){
        $data = QuestionnaireQuestion::createQuestionnaire($request->all());
        return back()->with('success_message','Questionnaire Added Successfully');
    }

    public function editQuestionnaire($id){
        $data = QuestionnaireQuestion::where('id','=',$id)->first();
        return view('superadmin.questionnaire.edit',[
            'data' => $data,
            'id' => \Auth::user()->id,
            'folder_id' => $data->folder_id
        ]);
    }

    public function updateQuestionnaire($id , Request $request){
        QuestionnaireQuestion::updateQuestionnaire($id , $request->all());
        return back()->with('success_message','Questionnaire Updated Successfully');
    }

    public function duplicateQuestionnaire(Request $request) {
        $data = $this->getQuestionnaireData($request->id);
        $d = [
            'questionnaire_name' => $data->name,
            'questionnaire_tag' => $data->tags,
            'folder_id' => $data->folder_id,
            'user_id' => $data->user_id,
            'pdf_id' => $data->pdf_id,
            'pdf_email' => $data->pdf_email,
            'pdf_fax_number' => $data->pdf_fax_number
        ];
        QuestionnaireQuestion::createQuestionnaire($d);
        $questionnaires_questions = QuestionnaireQuestion::all();
        return collect([
           'status' => true,
           'questionnaires_questions' => $questionnaires_questions
        ]);
    }

    public function getQuestionnaireData($id){
        return QuestionnaireQuestion::where('id','=',$id)->first();
    }

    public function actionPerformQuestionnaire(Request $request){
        $data = [];
        $status = false;
        $message = 'Something went Wrong';
        $action = '';
        if($request->action == 'delete') {
            QuestionnaireQuestion::where('id', '=', $request->id)->delete();
            $status = true;
            $message = 'Questionnaire Deleted Successfully';
            $action = $request->action;
            $data = QuestionnaireQuestion::all();
        }
        if($request->action == 'move_folder') {
            QuestionnaireQuestion::where('id','=',$request->id)->update([
               'folder_id' => $request->folder_id
            ]);
            $status = true;
            $message = 'Folder Moved Successfully';
            $action = $request->action;
            $data = QuestionnaireQuestion::where('folder_id','=',$request->folder_id)->get();
        }
        return collect([
           'status' => $status,
           'message' => $message,
           'data' => $data,
           'action' => $action
        ]);
    }

    public function treeData(){
        $treeData = Questionnair::with('children')->where('folder_id','=',null)->get();
        return response()->json($treeData);
    }

}
