<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;
use App\Http\Requests\DoctorForm;

class DoctorController extends Controller
{

    public function CreateDoctor()
    {
        return view('superadmin.create-doctor');
    }

    public function allDoctor()
    {
        $doctor = Doctor::all();
        return view('superadmin.all-doctor',[
            'doctor' => $doctor
        ]);
    }

    public function doctorDetail($id)
    {
        $doctor_detail = Doctor::where('id','=',$id)->first();
        return view('superadmin.docter_details',[
            'doctor_detail' => $doctor_detail
        ]);
    }

    public function doctorDelete($id)
    {
        Doctor::where('id','=',$id)->delete();
        return back()->with('message','Doctor Deleted Successfully');
    }

    public function postDoctor(DoctorForm $request)
    {
        $data = Doctor::createDoctor($request->all());
        return back()->with('success_message','Doctor Created Successfully');
    }
}
