<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\SeoSetting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('superadmin.dashboard');
    }

    public function Createinvoice()
    {
        return view('superadmin.create-invoice');
    }

    public function Allinvoices()
    {
        return view('superadmin.all-invoices');
    }

    public function InvoiceDetail()
    {
        return view('superadmin.invoice-detail');
    }

    public function CreatePage()
    {
        return view('superadmin.create-new-page');
    }


    public function CreatePagePost(Request $request)
    {
        $create_page = new Page();
        $create_page->page_title = $request->page_title;
        $create_page->page_slug = $request->page_slug;
        $create_page->page_content = $request->page_content;
        $create_page->status = 1;
        $create_page->save();

        $save_seo_settings = new SeoSetting();
        $save_seo_settings->page_id = $create_page->id;
        $save_seo_settings->save();
        return back()->with('success_message','Page Created Successfully');
    }



    public function AllPages()
    {
        $pages = Page::all();
        return view('superadmin.all-pages',[
            'pages' => $pages
        ]);
    }


    public function seoDetails()
    {
        $Seo_setting = SeoSetting::all();
        return view('superadmin.seo-details',[
            'Seo_setting' => $Seo_setting
        ]);
    }


    public function seopost(Request $request)
    {

        $update_settings = SeoSetting::where('id', $request->id)->first();
        $update_settings->meta_author = $request->meta_author;
        $update_settings->meta_description = $request->meta_description;
        $update_settings->keywords = $request->keywords;
        $update_settings->save();
        return back()->with('success_message','Updated Successfully');
    }

}
