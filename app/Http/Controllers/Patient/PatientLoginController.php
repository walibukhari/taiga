<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PatientLoginController extends Controller
{
    public function patientLoginForm() {
        return view('auth.patientAuth.login');
    }
    public function patientLogin(Request $request) {
        if(Auth::guard('patient')->attempt(['email' => $request->email , 'password' => $request->password])) {
            return redirect(RouteServiceProvider::PATIENT_HOME);
        } else {
            return back()->with('error_message','Credtional\'s Not Match');
        }
    }

    public function logoutPatient()
    {
        $this->guard()->logout();
        $this->invalidSession();
        return redirect('/patient-login');
    }

    public function guard(){
        return Auth::guard('patient');
    }

    public function invalidSession(){
        session()->flush();
        return true;
    }
}
