<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientRegisterController extends Controller
{
    public function patientSignUpForm() {
        return view('auth.patientAuth.register');
    }

    public function patientRegister(Request $request){
        Patient::registerPatient($request->all());
        return redirect('/patient-login');
    }
}
