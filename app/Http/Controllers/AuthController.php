<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Patient;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function userLoginForm(){
        return view('auth.doctorPatientAuth.login');
    }
    public function userLoginPost(Request $request) {
        $guard = $this->checkUserIsPatientOrDoctor($request->all());
        if($guard == 'false') {
            return back()->with('error_message','Credtional\'s Not Match');
        }
        if(Auth::guard($guard)->attempt(['email' => $request->email , 'password' => $request->password])) {
            if($guard == 'doctor') {
                return redirect(RouteServiceProvider::DOCTOR_HOME);
            } else {
                return redirect(RouteServiceProvider::PATIENT_HOME);
            }
        } else {
            return back()->with('error_message','Credtional\'s Not Match');
        }
    }

    public function checkUserIsPatientOrDoctor($request){
        $doctor = Doctor::where('email','=',$request['email'])->first();
        $patient = Patient::where('email','=',$request['email'])->first();
        if(!is_null($doctor)) {
            if(Hash::check($request['password'],$doctor->password)) {
                return 'doctor';
            }
        } else if(!is_null($patient)) {
            if(Hash::check($request['password'],$patient->password)) {
                return 'patient';
            }
        } else {
            return 'false';
        }
    }

    public function userLogout(Request $request)
    {
        $guard = $request->guard;
        $this->guard($guard)->logout();
        $this->invalidSession();
        return redirect('/login');
    }

    public function guard($guard){
        return Auth::guard($guard);
    }

    public function invalidSession(){
        session()->flush();
        return true;
    }
}
