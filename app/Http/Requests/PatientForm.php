<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'email' => 'required',
            'password' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'year_of_birth' => 'required',
            'month_of_birth' => 'required',
            'day_of_birth' => 'required',
            'cell_phone' => 'required',
            'address' => 'required',
            'zip_code' => 'required',
            'gender' => 'required',
            'health_card_number' => 'required',
            'pharmacy_name' => 'required',
            'pharmacy_fax' => 'required',
        ];
    }
}
