<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required',
            'clinic_organization' => 'required',
            'country' => 'required',
            'state' => 'required',
            'address' => 'required',
            'city' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'cell_phone' => 'required',
        ];
    }
}
