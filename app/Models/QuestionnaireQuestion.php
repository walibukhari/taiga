<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionnaireQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
      'name','tags','folder_id','user_id','pdf_id','pdf_email','pdf_fax_number'
    ];

    public static function createQuestionnaire($request){
        return self::create([
           'name' => $request['questionnaire_name'],
           'tags' => $request['questionnaire_tag'],
           'folder_id' => $request['folder_id'],
           'user_id' => $request['user_id'],
           'pdf_id' => $request['pdf_id'],
           'pdf_email' => $request['pdf_email'],
           'pdf_fax_number' => $request['pdf_fax_number'],
        ]);
    }

    public static function updateQuestionnaire($id,$request){
        return self::where('id','=',$id)->update([
           'name' => $request['questionnaire_name'],
           'tags' => $request['questionnaire_tag'],
           'folder_id' => $request['folder_id'],
           'user_id' => $request['user_id'],
           'pdf_id' => $request['pdf_id'],
           'pdf_email' => $request['pdf_email'],
           'pdf_fax_number' => $request['pdf_fax_number'],
        ]);
    }
}
