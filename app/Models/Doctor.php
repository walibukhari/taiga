<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class Doctor extends Model implements Authenticatable
{
    use HasFactory , AuthenticableTrait;

    protected $fillable = [
        'email',
        'password',
        'clinic_name',
        'country',
        'state',
        'address',
        'city',
        'first_name',
        'last_name',
        'cell_phone',
        'clinic_phone',
        'full_name',
        'terms_check'
    ];

    public static function createDoctor($request) {
        return self::create([
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'clinic_name' => $request['clinic_organization'],
            'country' => $request['country'],
            'state' => $request['state'],
            'address' => $request['address'],
            'city' => $request['city'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'cell_phone' => $request['cell_phone'],
            'clinic_phone' => $request['clinic_phone_number'],
        ]);
    }

    public static function registerDoctor($request){
        return self::create([
           'full_name' => $request['full_name'],
           'email' => $request['email'],
           'password' => bcrypt($request['password']),
           'terms_check' => isset($request['terms_check']) ? 1 : 0
        ]);
    }
}
