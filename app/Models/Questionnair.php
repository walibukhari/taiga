<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questionnair extends Model
{
    use HasFactory;

    protected $fillable = [
      'folder_name','folder_created_by','slug','folder_id'
    ];

    protected $appends = [
      'label'
    ];

    public function getLabelAttribute(){
        return $this->attributes['folder_name'];
    }

    public function questionnaire_questions(){
        return $this->hasMany(Questionnair::class,'id','folder_id');
    }

    public function child(){
        return $this->hasMany(self::class,'folder_id');
    }
    public function children(){
        return $this->child()->with('children');
    }
}
