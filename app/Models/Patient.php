<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class Patient extends Model implements Authenticatable
{
    use HasFactory , AuthenticableTrait;

    protected $fillable = [
        'email',
        'password',
        'country',
        'state',
        'address',
        'city',
        'first_name',
        'last_name',
        'middle_name',
        'year_of_birth',
        'month_of_birth',
        'day_of_birth',
        'cell_phone_number',
        'zip_code',
        'health_care_number',
        'gender',
        'pharmacy_name',
        'pharmacy_fax_number',
        'full_name',
        'terms_check'
    ];

    public static function createPatients($request){
        return self::create([
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'country' => $request['country'],
            'state' => $request['state'],
            'address' => $request['address'],
            'city' => $request['city'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'middle_name' => $request['middle_name'],
            'year_of_birth' => $request['year_of_birth'],
            'month_of_birth' => $request['month_of_birth'],
            'day_of_birth' => $request['day_of_birth'],
            'cell_phone_number' => $request['cell_phone'],
            'zip_code' => $request['zip_code'],
            'health_care_number' => $request['health_card_number'],
            'gender' => $request['gender'],
            'pharmacy_name' => $request['pharmacy_name'],
            'pharmacy_fax_number' => $request['pharmacy_fax'],
        ]);
    }

    public static function registerPatient($request){
        return self::create([
            'full_name' => $request['full_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'terms_check' => isset($request['terms_check']) ? 1 : 0
        ]);
    }
}
