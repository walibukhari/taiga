<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoSetting extends Model
{

    use HasFactory;

   protected $table = 'seo_settings';

   public function Page_title() {
        return $this->hasOne(Page::class,  'id' , 'page_id');
    }

}
