
<!doctype html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Taiga &#8211; Online appointments with Canadian doctors</title>
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="{{url('/landing_page')}}/assets/images/favicon.png" />
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/bootstrap.min.css">
    
    <!--magnific popup css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/magnific-popup.css">
    <!--icofont css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/icofont.min.css">
    <!--animate css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/animate.css">
    <!--main css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/style.css">
    <!--responsive css-->
    <link rel="stylesheet" type="text/css" href="{{url('/landing_page')}}/assets/css/responsive.css">


   

</head>

<body>

    <div class="loading preloader"  style="animation-duration: 1.60s;animation-fill-mode: both;animation-name: Eb;">
            <div class="wrapper h-100">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <div class="loading-content">
                        <div class="logo logo-primary"><img style="animation-duration: 1.50s;animation-fill-mode: both;animation-name: ob;" class="animated zoomin" src="{{url('/landing_page')}}/img/logo-dark.svg" alt="Malex" /></div>
                    </div>
                </div>
            </div>
        </div>

    <!--end preloader-->

    <!--start header-->
    <header id="header">
        <div class="container-fluid">
            <nav class="navbar navbar-light navbar-expand-lg">
                <a class="logo" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
                <a class="logo2" href="index.html"><img src="{{url('/landing_page')}}/img/logo-dark.svg" alt="logo"></a>
                <button class="navbar-toggler srch ml-1" type="button">
                    <a class="nav-link" data-toggle="modal" data-target="#exampleModal-search" href="#"><i style="color: #fff;margin-left: -3px;" class="icofont-ui-search"></i></a>
                </button>
                <button class="navbar-toggler ml-1" type="button" data-toggle="modal" data-target="#exampleModal-menu">
                    
                    <span class="icofont-navigation-menu"></span>
                </button>

                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="NavbarContent">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-scroll-nav="0">SERVICES</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="1">ABOUT US</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="2">TEAM</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="3">PRICING</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="4">TESTIMONIALS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="5">REGISTER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="6">CONTACTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{url('/login')}}" >LOGIN</a>
                        </li>
                        
                    </ul>
                    <ul class="nav navbar-nav flex-row justify-content-center flex-nowrap search-btn">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#exampleModal-search" href="#"><i class="icofont-ui-search"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
            
    </header>
    <!--end header-->


    <!-- search-modal here -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal-search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
      <div class="modal-dialog mymodal" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
                <a class="logo" title="Malex" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="search-container">
                            <form action="/action_page.php">
                              <input type="text" placeholder="Search" name="search">
                              <button type="submit"><i class="icofont-ui-search"></i></button>
                            </form>
                          </div>
                       
                    </div>
                </div>
            </div>
          </div>

          <div class="modal-end text-left">
            <h5>© 2020 Taiga Get high quality medical care from anywhere. Available on any device. </h5>

            <a href="#" class="eng">EN</a>
            
          </div>
          
        </div>
      </div>
    </div>

    <div class="modal fade" id="exampleModal-menu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
      <div class="modal-dialog mymodal" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
                <a class="logo" title="Malex" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
            </h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="menu">
                            <ul>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" data-scroll-nav="0">SERVICES</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="1">ABOUT US</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="2">TEAM</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="3">PRICING</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="4">TESTIMONIALS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="5">NEWS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-scroll-nav="6">CONTACTS</a>
                                </li>    
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
          </div>


          <div class="modal-end text-left">
            <p>575 Crescent Avenue<br>PA 18951<br>United Kingdom</p>
            <p style="color: #fff;"> <a href="tel:+43253312523">+432 533 12 523</a><br><a href="mailto:info@domain.com">info@domain.com</a></p>
            <br>
            <br>
            <h5>© 2020 Virtual Care & Note Automation Netblue IT </h5>

            <a href="#" class="eng">EN</a>
            
          </div>
          
        </div>
      </div>
    </div>




    <!--start home area-->
    <section id="home-area" style="background-image: url({{url('/landing_page')}}/img/01_img.jpg)">
        <div class="overly-bnr">
            <div class="container">
                <div class="row">
                    <!--start caption-->
                    <div class="col-lg-7 col-md-7">
                        <div class="caption d-table">
                            <div class="d-table-cell align-middle">
                                <h1>Online appointments<br>with Canadian doctors</h1>
                                <p>See a doctor from wherever you are via smart phone, tablet or computer. No offices, no exposure, no wait. 100% OHIP covered</p>
                                <a href="#" class="learn">Book Now</a>
                                 <a href="#service-area" class="scrollanimation"><i class="icofont-long-arrow-down"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--end caption-->


                    <!--start caption blank-->
                    <div class="col-lg-5 col-md-5">
                        <div class="caption-eng text-center d-table">
                            <div class="d-table-cell align-middle">

                                <a href="#" class="eng">EN</a>
                            </div>
                        </div>
                    </div>
                    <!--end caption blank-->
                </div>
            </div>
        </div>
    </section>
    <!--end home area-->

    <!--start service area-->
    <section id="service-area" data-scroll-index="0">
        <div class="container-fluid">
            <div class="row">
                <!--start service info single-->
                <div class="col-md-4">
                    <div class="service-info-single">
                        <i class="icofont-horse-head-2"></i>
                        <h3>Book an Appointment</h3>
                        <p>No more waiting for hours in a walk-in clinic, or weeks to see your family doctor. Doctors on your schedule, book online. </p>
                        <br>
                        <a class="arowfade" href="#"><i class="icofont-long-arrow-right"></i><span class="read"> BOOK NOW</span></a>
                    </div>
                </div>
                <!--end service info single-->
                <!--start service info single-->
                <div class="col-md-4">
                    <div class="service-info-single">
                        <i class="icofont-unique-idea"></i>
                        <h3>Speak to a Doctor</h3>
                        <p>Speak directly with doctors in Canada by phone, video, or secure messaging, on any device. You're in control and your time is valuable. </p>
                        <a class="arowfade" href="#"><i class="icofont-long-arrow-right"></i><span class="read"> VIEW DOCTORS</span></a>
                    </div>
                </div>
                <!--end service info single-->
                <!--start service info single-->
                <div class="col-md-4">
                    <div class="service-info-single">
                        <i class="icofont-spreadsheet"></i>
                        <h3>Wrap Up </h3>
                        <p>Receive referrals, requisitions and any other documents you need, all online. Free prescription delivery in Canada. </p>
                        <br>
                        <a class="arowfade" href="#"><i class="icofont-long-arrow-right"></i><span class="read"> GET STARTED</span></a>
                    </div>
                </div>
                <!--end service info single-->
            </div>
        </div>
    </section>
    <!--end service area-->

    <!--start about area-->
    <section id="about-area" data-scroll-index="1">
        <div class="container-fluid">
            <div class="row">
                <!--start about content-->
                <div class="col-lg-6 col-md-7">
                    <div class="about-content">
                        <h5>ABOUT US</h5>
                        <h2>We're bringing healthcare to the 21st century</h2>
                        <div class="row about-info">
                            <!--start about info single-->
                            <div class="col-md-12">
                                <div class="about-info-image">

                                </div>
                            
                                <div class="about-info-single">
                                    <h4>Get high quality medical care from anywhere.Available on any device. </h4>
 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end about content-->
                <!--start about image-->
                <div class="col-lg-6 col-md-5">
                    <div class="about-right">
                        <p>Lorem ipsum dolor sit amet, conseccctetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                        <p style="margin-bottom: 30px;">Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis</p>
                    </div>
                </div>
                <!--end about image-->
            </div>


            <hr>

            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="serv2">
                        <div class="arrows">
                            <i class="icofont-check"></i>
                        </div>
                        <div class="arrow-txt">
                            <h4>Automate Medical Documentation </h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="serv2">
                        <div class="arrows">
                            <i class="icofont-check"></i>
                        </div>
                        <div class="arrow-txt">
                            <h4>Improved Care and Efficiency  </h4>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="serv2">
                        <div class="arrows">
                            <i class="icofont-check"></i>
                        </div>
                        <div class="arrow-txt">
                            <h4>Reduce Administrative Expenses </h4>
                        </div>
                    </div>
                </div>

                 <div class="col-lg-3 col-md-3">
                    <div class="serv2">
                        <div class="arrows">
                            <i class="icofont-check"></i>
                        </div>
                        <div class="arrow-txt">
                            <h4>30 Days Money Back Warranty </h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about-bnr">
                <img src="{{url('/landing_page')}}/img/abt.jpg" alt="image">
            </div>

            <div class="row no-gutters">
                <div class="col-lg-3 col-md-3">
                    <div class="process-content">
                        <h6 class="process-small-t-head">How it Works?</h6>
                        <h2 class="process-t-head">Build your Virtual Practice</h2>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="process-content process1">
                        <h4 class="process-t-head">Automate <br> Notes</h4>
                        <p class="process-description">Using intelligent medical questionnaires to collect data from patients, we automate medical notes using artificial intelligence.</p>
                    </div>
                    <div class="process-number">
                        01
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="process-content process2">
                        <h4 class="process-t-head">Virtual<br> Care</h4>
                        <p class="process-description">See your patients via video or phone consults and improve patient access while opening new revenue streams!</p>
                    </div>
                    <div class="process-number process-number2">
                        02
                    </div>
                </div>

                 <div class="col-lg-3 col-md-3">
                    <div class="process-content process3">
                        <br>
                        <h4 class="process-t-head">Update<br> your CPP</h4>
                        <p class="process-description">Tired of double or triple entering data? We can keep your cumulative patient profiles up to date!</p>
                    </div>
                    <div class="process-number process-number3">
                        03
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--end about area-->

    <!--start team area-->
    <section id="team-area" data-scroll-index="2">
        <div class="container-fluid">
            <div class="team-content">
                <h6>meet our team</h6>
                <h2>A hand picked network of doctors<br> and specialists ready to help you.</h2>
            </div>

             <div class="team-number">
                Team
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="team">
                        <img src="{{url('/landing_page')}}/img/03_img.jpg" alt="image">
                        <h6>Book NOW</h6>
                        <h4>Dr. Gita Kruger</h4>
                        <p>Dr. Gita Kruger is a practicing family medicine and urgent care physician in Calgary, Alberta. Her pre-medical education includes Masters of Science (MSc) degrees in both Audiology and Speech Pathology from</p>

                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="team">
                        <img src="{{url('/landing_page')}}/img/04_img.jpg" alt="image">
                        <h6>Book NOW</h6>
                        <h4>Dr. Garth Slysz</h4>
                        <p>Dr. Gita Kruger is a practicing family medicine and urgent care physician in Calgary, Alberta. Her pre-medical education includes Masters of Science (MSc) degrees in both Audiology and Speech Pathology from</p>

                    </div>
                </div>

                 <div class="col-lg-4 col-md-4">
                    <div class="team">
                        <img src="{{url('/landing_page')}}/img/05_img.jpg" alt="image">
                        <h6>Book NOW</h6>
                        <h4>Dr. Aleksandra Ferenc</h4>
                        <p>Dr. Gita Kruger is a practicing family medicine and urgent care physician in Calgary, Alberta. Her pre-medical education includes Masters of Science (MSc) degrees in both Audiology and Speech Pathology from</p>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--end team area-->

    <!--start product area-->
    <section id="pricing-area" data-scroll-index="3">
        <div class="container-fluid">
            <div class="team-content">
                <h6>Price Plans</h6>
                <h2>Choose the Best Plan for You</h2>
            </div>

             <div class="price-number">
                Plans
            </div>
            <div class="row">
                <!--start product single-->
                <div class="col-lg-4 col-md-6 col-md-4">
                    <div class="product-single">
                        <div class="prod-details">
                            <h3>Basic Plan</h3>
                            <p>This is Basic Plan</p>
                            <h2> 
                                <span class="currency">$</span> 
                                <span class="price">79</span> 
                                <span class="period">/ Monthly</span>
                            </h2>

                            <ul>
                                <li><span>Branding</span> <i class="icofont-check"></i></li>
                                <li><span>20 hours of consultation </span> <i class="icofont-check"></i></li>
                                <li><span> 15 hours if theory </span> <i class="icofont-check"></i></li>
                                <li><span> Skype video call </span> <i class="icofont-check"></i></li>
                                <li style="border-bottom: none;"><span> Support 24/7 </span> <i class="icofont-check"></i></li>
                            </ul>

                            <div class="text-center">
                                <a href="#" data-toggle="modal" data-target="#exampleModal">Choose Plan</a>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
                                  <div class="modal-dialog mymodal" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            <a class="logo" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="container">
            
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="contact-form text-left" style="background: #000;padding-top: 0;">

                                                        <h2 style="text-align: left;padding-left: 0;color: #fff;padding-top: 0;">Order Basic Plan</h2>

                                                        <form id="ajax-contact" action="contact.php" method="post">
                                                           
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="First name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Last name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" data-error="valid email is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Company" required="required" data-error="company is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Phone no." required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>                 
                                                            
                                                            <div class="form-group">
                                                                <textarea class="form-control" id="message" name="message" rows="10" placeholder="Message" required="required" data-error="Please, leave us a message."></textarea>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="contact-btn">
                                                                <button type="submit">Submit</button>
                                                            </div>
                                                            <div class="messages"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>


                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--end product single-->
                <!--start product single-->
                <div class="col-lg-4 col-md-6 col-md-4">
                    <div class="product-single">
                        <div class="prod-details">
                             <h3>Standard Plan</h3>
                            <p>This is Standard Plan</p>
                            <h2> 
                                <span class="currency">$</span> 
                                <span class="price">129</span> 
                                <span class="period">/ Monthly</span>
                            </h2>

                            <ul>
                                <li><span>Branding</span> <i class="icofont-check"></i></li>
                                <li><span>20 hours of consultation </span> <i class="icofont-check"></i></li>
                                <li><span> 15 hours if theory </span> <i class="icofont-check"></i></li>
                                <li><span> Skype video call </span> <i class="icofont-check"></i></li>
                                <li style="border-bottom: none;"><span> Support 24/7 </span> <i class="icofont-check"></i></li>
                            </ul>

                            <div class="text-center">
                                <a href="#" data-toggle="modal" data-target="#exampleModal2">Choose Plan</a>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
                                  <div class="modal-dialog mymodal" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            <a class="logo" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="container">
            
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="contact-form text-left" style="background: #000;padding-top: 0;">

                                                        <h2 style="text-align: left;padding-left: 0;color: #fff;padding-top: 0;">Order Standard Plan</h2>

                                                        <form id="ajax-contact" action="contact.php" method="post">
                                                           
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="First name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Last name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" data-error="valid email is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Company" required="required" data-error="company is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Phone no." required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>                 
                                                            
                                                            <div class="form-group">
                                                                <textarea class="form-control" id="message" name="message" rows="10" placeholder="Message" required="required" data-error="Please, leave us a message."></textarea>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="contact-btn">
                                                                <button type="submit">Submit</button>
                                                            </div>
                                                            <div class="messages"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>


                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--end product single-->
                <!--start product single-->
                <div class="col-lg-4 col-md-6 col-md-4">
                    <div class="product-single">
                        <div class="ribbon">Popular</div>

                        <div class="prod-details">
                            <h3>Premium Plan</h3>
                            <p>This is Premium Plan</p>
                            <h2> 
                                <span class="currency">$</span> 
                                <span class="price">259</span> 
                                <span class="period">/ Monthly</span>
                            </h2>

                            <ul>
                                <li><span>Branding</span> <i class="icofont-check"></i></li>
                                <li><span>20 hours of consultation </span> <i class="icofont-check"></i></li>
                                <li><span> 15 hours if theory </span> <i class="icofont-check"></i></li>
                                <li><span> Skype video call </span> <i class="icofont-check"></i></li>
                                <li style="border-bottom: none;"><span> Support 24/7 </span> <i class="icofont-check"></i></li>
                            </ul>

                            <div class="text-center">
                                <a href="#" data-toggle="modal" data-target="#exampleModal3">Choose Plan</a>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
                                  <div class="modal-dialog mymodal" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            <a class="logo" href="index.html"><img src="{{url('/landing_page')}}/img/logo-light.svg" alt="logo"></a>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="container">
            
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="contact-form text-left" style="background: #000;padding-top: 0;">

                                                        <h2 style="text-align: left;padding-left: 0;color: #fff;padding-top: 0;">Order Premium Plan</h2>

                                                        <form id="ajax-contact" action="contact.php" method="post">
                                                           
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="First name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Last name" required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" data-error="valid email is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Company" required="required" data-error="company is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Phone no." required="required" data-error="name is required.">
                                                                <div class="help-block with-errors"></div>
                                                            </div>                 
                                                            
                                                            <div class="form-group">
                                                                <textarea class="form-control" id="message" name="message" rows="10" placeholder="Message" required="required" data-error="Please, leave us a message."></textarea>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                            <div class="contact-btn">
                                                                <button type="submit">Submit</button>
                                                            </div>
                                                            <div class="messages"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end product single-->
                
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="accordion" role="tablist">

                        <h3>Faq's</h3>
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header active" role="tab" id="faq1">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapse1" aria-expanded="true" aria-controls="collapse1">How can I buy the product?</a>
                                </h5>
                            </div>
                            <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="faq1" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq2">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">How can I order the product?</a>
                                </h5>
                            </div>
                            <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="faq2" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq3">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapse3">How much price of the product?</a>
                                </h5>
                            </div>
                            <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="faq3" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq4">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapse4">How can I get refund?</a>
                                </h5>
                            </div>
                            <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="faq4" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq5">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapse5">How can I order the product?</a>
                                </h5>
                            </div>
                            <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="faq5" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq6">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse6" aria-expanded="false" aria-controls="collapse6">How much price of the product?</a>
                                </h5>
                            </div>
                            <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="faq6" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                        <!--start faq single-->
                        <div class="card">
                            <div class="card-header" role="tab" id="faq7">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapse7" aria-expanded="false" aria-controls="collapse7">How can I get refund?</a>
                                </h5>
                            </div>
                            <div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="faq7" data-parent="#accordion">
                                <div class="card-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                            </div>
                        </div>
                        <!--end faq single-->
                    </div>
                
                </div>

            </div>
        </div>

    </section>

    <div class="video-box">
        <div class="video-content text-center" style="background: url({{url('/landing_page')}}/img/vdo.jpg);background-repeat: no-repeat;background-size: cover;">
            <div class="video-overlay"></div>
            <a class="" data-toggle="modal" data-target="#exampleModal-enter" href="#"><i class="icofont-ui-play"></i></a>

           <!--  <a href="#" data-toggle="modal" data-target="#exampleModal">Choose Plan</a> -->

            <!-- Modal -->
            <div class="modal fade" id="exampleModal-enter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="background: #000;">
              <div class="modal-dialog mymodal" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-12">
                                <iframe width="800" height="500" src="https://www.youtube-nocookie.com/embed/kffacxfA7G4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                               
                            </div>
                        </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>

        </div>
    </div>
    <!--end product area-->

    <!--start testimonial area-->
    <section id="testimonial-area" data-scroll-index="4">
        <div class="container-fluid">
            <div class="team-content">
                <h6>Testimonials</h6>
                <h2 style="color: #fff;margin-bottom: 40px;">Our Happy Customers Reviews</h2>
            </div>

             <div class="test-number">
                <i class="icofont-quote-right"></i>
            </div>

            <h3>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</h3>

            <div class="row mt-5">
                <div class="col-lg-3">
                    <ul>
                        <li><img src="{{url('/landing_page')}}/img/12_img.png" alt="image"></li>
                        <li><img src="{{url('/landing_page')}}/img/13_img.png" alt="image"></li>
                        <li><img src="{{url('/landing_page')}}/img/14_img.png" alt="image"></li>
                    </ul>
                </div>
                <div class="col-lg-9">
                    <div class="name">
                        <h4>Tommy Andersen</h4>
                        <h5>CEO at company</h5>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--end testimonial area-->
 
    <!--start blog area-->
    <section id="register-area" data-scroll-index="5">
        <div class="container-fluid">
            <div class="row">
                <!--start blog single-->
                <div class="col-md-4">
                    <div class="register">
                        <img src="{{url('/landing_page')}}/img/phonebig.png" alt="image">
                    </div>
                </div>
                <!--end register single-->
                <!--start register single-->
                <div class="col-md-8">
                        
                    <div class="register-content">
                       
                        <h3 class="m-0"><a href="#">YOUR PERSONAL PATIENT PORTAL</a></h3>
                        <h2>Sign up For Taiga Health today and book when you are ready ! </h2>
                        <h4>By registering now, you will have access to our network of doctors and your own private portal.See your history of appointments, monitoring programs and prescriptions.</h4>

                        <div class="text-left view">
                            <a href="#">Register Now</a>
                        </div>
                    </div>
                </div>
                <!--end register single-->
                
            </div>
        </div>
    </section>
    <!--end blog area-->
    <section id="contact-area" data-scroll-index="6">
        <div class="container-fluid">
            <!--start section heading-->
            <div class="team-content">
                <h6>Contacts</h6>
                <h2>Any Questions? Contact us Freely and<br> We’ll Get Back to You Shortly</h2>
            </div>

             <div class="team-number">
                News
            </div>

            <div class="row pb-5">
                <div class="col-lg-4 col-md-4">
                    <div class="contact1">
                        <p><i class="icofont-google-map"></i></p>
                        <h5>Location</h5>
                        <p>Location: 575 Crescent Avenue<br>Quakertown, PA 18951</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="contact1">
                        <p><i class="icofont-phone"></i></p>
                        <h5>Phone no.</h5>
                        <p>Mobile: <a href="tel:+43253312523">+432 533 12 523</a><br> Fax: <a href="tel:+53222212523">+532 222 12 523</a></p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="contact1">
                        <p><i class="icofont-telegram"></i></p>
                        <h5>Email</h5>
                        <p>Info: <a href="mailto:info@domain.com">info@domain.com</a><br> CEO: <a href="mailto:ceo@domain.com">ceo@domain.com</a></p>
                    </div>
                </div>


            </div>
            
        </div>

        <div class="container">
            
            <div class="row">
                <div class="col-lg-12">

                    <div class="contact-form">

                        <h2>You Can Write Us</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> When an unknown printer took a galley.</p>


                        <form id="ajax-contact" action="contact.php" method="post">
                           
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="required" data-error="name is required.">
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" data-error="valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>                 
                            
                            <div class="form-group">
                                <textarea class="form-control" id="message" name="message" rows="10" placeholder="Message" required="required" data-error="Please, leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="contact-btn">
                                <button type="submit">Send Message</button>
                            </div>
                            <div class="messages"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="back-to-top"> 
            <a  href="#home-area" class="scrollanimation"><i class="icofont-long-arrow-up"></i></a>
        </div>

    </section>

    <footer id="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 falg">
                    <img src="{{url('/landing_page')}}/img/logo-light.svg">
                </div>
                <div class="col-lg-6">
                    <p class="m-0 text-white">&copy; © 2020 Virtual Care & Note Automation by  <a href="https://netblue.nl">Netblue IT</a></p>
                </div>
                <div class="col-lg-3 fank">
                    <a href="#"><i class="icofont-linkedin"></i></a>
                    <a href="#"><i class="icofont-facebook"></i></a>
                </div>
            </div>
            
        </div>
    </footer>





    <!--jQuery js-->
    <script src="{{url('/landing_page')}}/assets/js/jquery-3.3.1.min.js"></script>
    <!--proper js-->
    <script src="{{url('/landing_page')}}/assets/js/popper.min.js"></script>
    <!--bootstrap js-->
    <script src="{{url('/landing_page')}}/assets/js/bootstrap.min.js"></script>
    <!--magnic popup js-->
    <script src="{{url('/landing_page')}}/assets/js/magnific-popup.min.js"></script>
    <!--scrollIt js-->
    <script src="{{url('/landing_page')}}/assets/js/scrollIt.min.js"></script>
    <!--validator js-->
    <script src="{{url('/landing_page')}}/assets/js/validator.min.js"></script>
    <!--contact js-->
    <script src="{{url('/landing_page')}}/assets/js/contact.js"></script>
    <!--main js-->
    <script src="{{url('/landing_page')}}/assets/js/custom.js"></script>
</body>


</html>
