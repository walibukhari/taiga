@extends('layouts.doctor_layout')
@push('css')
    <link rel="stylesheet" href="{{asset('doctor_patient/statistics.css')}}">
@endpush
@section('content')
    <div class="col-xl-9 offset-xl-3 col-md-12 custom-column-account custom-column-statistics">
        <div class="statistics-single-item">
            <div class="commmon-title statistics-title">
                <h1 class="common-title-d">Dashboard</h1>
            </div>
            <div class="statistics-btn">
                <!-- Example single danger button -->
                <div class="btn-group">
                    <button type="button" class="btn  dropdown-toggle custom-dropdown-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Past month <span><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
      </svg></span>
                    </button>
                </div>
                <a href="#"> <span><i class="fas fa-print"></i></span></a>
                <a href="#"> <span><i class="fas fa-download"></i></span></a>
            </div>
        </div>



        <div class="row">
            <div class="col-md-4">
                <div class="statistics-content">

                    <h1>Welcome Charles</h1>
                    <p>It looks like you have no upcoming appointments.would you like to make one?</p>

                </div>

                <div class="agenda-single-item statistics-common-single-item">
                    <div class="agenda-single-title statistics-single-title">


                        <div class="agenda-date">
                            <p>Metting</p>
                            <small>11.00am-1.00pm</small>
                            <span><i class="far fa-trash-alt"></i></span>
                        </div>
                    </div>


                </div>
            </div>


            <div class="col-md-8">
                <div class="row">
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Metting</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Birthday Party</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Dave Ceremony</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-md-4">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Metting</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Birthday Party</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Dave Ceremony</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Metting</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Birthday Party</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-md-6">
                        <div class="agenda-single-item statistics-common-single-title ">
                            <div class="agenda-single-title statistics-single-title">
                                <div class="agenda-date">
                                    <p>Dave Ceremony</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single item end here  -->

                </div>

            </div>
        </div>

        <!-- ====================================
                   chart start here
        =========================================== -->

        <div class="chart-btn">
            <a href="#" class="btn"> <span><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-left-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"/>
                          </svg></span> November 2020 <span><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg></span></a>
        </div>

        <div>
            <canvas id="myChart">
                <div>
                    <canvas id="myChart"></canvas>
                </div>
            </canvas>
        </div>

    </div>
@endsection
@push('js')
@endpush
