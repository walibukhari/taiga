@extends('layouts.doctor_layout')

@push('css')
    <link rel="stylesheet" href="{{asset('doctor_patient/account.css')}}">
@endpush

@section('content')
    <div class="col-xl-9 offset-xl-3 col-md-12 custom-column-account">
        <div class="account-profile-single-item">
            <div class="commmon-title account-item">
                <h1 class="common-title-d">Settings</h1>
            </div>
            <div class="row">

                <div class="col-xl-3 col-lg-4 col-md-4" style="padding-right:0;">
                    <div class="single-item-pills">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"> <span><i class="far fa-user"></i></span> Edit Profile</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"> <span><i class="fas fa-lock"></i></span> Change Password</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"> <span><i class="fas fa-money-check"></i></span> Billing</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"> <span><i class="far fa-bell"></i></span> Notification</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-8">
                    <div class="single-item-tab">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0;">
                                        <div class="account-form-single-item">
                                            <div class="account-profile">
                                                <div class="profile-img">
                                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                                </div>
                                                <div class="profile-btn">
                                                    <a href="#" class="btn">Change Avatar</a>
                                                    <a href="#" class="btn">Remove</a>

                                                </div>
                                            </div>
                                            <!-- =======================================
                                              form start here
                                            ============================================= -->


                                            <form>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputName">*Name</label>
                                                        <input type="text" class="form-control" id="inputName" name="f-name" placeholder="Charlie Howard">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="userName">*Username</label>
                                                        <input type="text" class="form-control" id="userName" name="userName" placeholder="Charlie">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="userEmail">*Email</label>
                                                        <input type="email" class="form-control" id="userEmail" name="userEmail" placeholder="Charlie.Howard@themnaite.com">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="example-date-input">Date Of Birth</label>

                                                        <input class="form-control" type="date" value="" id="example-date-input" placeholder="select date">

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="phoneNumber">Phone Number</label>
                                                        <input type="tel" class="form-control" id="phoneNumber" placeholder="+44(1532)135 792">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputWeb">Website</label>
                                                        <input type="text" class="form-control" id="inputWeb" name="inputWeb">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress">Address</label>
                                                    <input type="text" class="form-control" id="inputAddress">
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputCity">City</label>
                                                        <input type="text" class="form-control" id="inputCity">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="inputZip">Post Code</label>
                                                        <input type="text" class="form-control" id="inputZip">
                                                    </div>
                                                </div>

                                                <div class="history-btn">
                                                    <a href="" class="btn">Message</a>
                                                    <a href="" class="btn">Request</a>
                                                    <a href="" class="btn">Follow</a>


                                                </div>
                                            </form>
                                            <!-- =======================================
                                              form end here
                                            ============================================= -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <!-- notification single item start here  -->
                                    <div class="col-md-12">
                                        <div class="notification-item">
                                            <h4 class="notify-title">Notification</h4>

                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="far fa-bookmark"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content">
                                                            <h4>Mention</h4>
                                                            <p>You will receive an notification when someone was mentioned you</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="far fa-user"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content">
                                                            <h4>Follow</h4>
                                                            <p>You will receive alert when someone is following you</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="far fa-comments"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content extra-notify-content">
                                                            <h4>Comment</h4>
                                                            <p>You will receive an notification when someone comment in your post</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="far fa-envelope"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content extra-notify-content">
                                                            <h4>Email</h4>
                                                            <p>You will receive daily email notification</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="fas fa-shopping-bag"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content">
                                                            <h4>New Product</h4>
                                                            <p>You will receive an notification when new product arrive</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon ">
                                                            <span><i class="fas fa-user-friends"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content extra-notify-content-1">
                                                            <h4>Group Invitation</h4>
                                                            <p>You will receive notification when group invite you</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->
                                            <!-- single item start here  -->
                                            <div class="notification-content">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="notify-icon">
                                                            <span><i class="fas fa-tasks"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-11">
                                                        <div class="notify-content extra-notify-content">
                                                            <h4>Task</h4>
                                                            <p>You will receive an notification when someone assign task</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="notify-toggle">

                                                    <label class="switch notify-switch">
                                                        <input type="checkbox">
                                                        <span class="slider"></span>
                                                    </label>

                                                </div>
                                            </div>
                                            <!-- single item end here  -->


                                        </div>
                                    </div>
                                    <!-- notification single item end here  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =================================
     Account single item end here
   ======================================= -->


        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#myTab a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        })
    </script>
@endpush
