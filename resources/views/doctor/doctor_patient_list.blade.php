@extends('layouts.doctor_layout')
@push('css')
@endpush
@section('content')
    <div class="col-xl-9 offset-xl-3 col-md-12 custom-style-column">
        <div class="history-header-single-item">
            <div class="commmon-title history-item">
                <h1 class="common-title-d">Dashboard</h1>
            </div>
            <div class="history-btn">
                <a href="" class="btn">Message</a>
                <a href="" class="btn">Request</a>
                <a href="" class="btn">Follow</a>


            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="common-single-item-history">
                    <div class="history-search ">
                        <form action="#" method="#">
                            <div class="form-row">
                                <div class="col-md-6 col-sm-6">
                                    <input type="search" id="search" class="form-control custom-form-control  " name="search " placeholder="First name "><span class="search-icon-h"><i class="fas fa-search"></i></span>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="search" id="search" class="form-control custom-form-control  " name="search " placeholder="Last name"><span class="search-icon-h"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>



                <!-- another row start here  -->
                <div class="row">
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6  ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->
                    <!-- single item start here  -->
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 ">
                        <div class="card-single-item">
                            <div class="card history-card">
                                <div class="history-card-img">
                                    <img src="{{asset('doctor_patient/image/man.jpg')}}" class="img-fluid" alt="man">
                                </div>
                                <div class="history-card-title">
                                    <p>Wendy Kenn</p>
                                    <p>Frelancer</p>
                                </div>
                                <div class="history-card-icon">
                                    <a href="#"><i class="fas fa-phone"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single item end here  -->


                    <div class="col-md-12">
                        <div class="pagination-single-item">
                            <div class="pagination-content">
                                <p>Showing 1 to 10 of 28 entries</p>
                            </div>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pagination-custom ">
                                    <li class="page-item">
                                        <a class="page-link custom-page-link" href="#" aria-label="Previous">
                                                            <span aria-hidden="true"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                                                          </svg></span>
                                        </a>
                                    </li>
                                    <li class="page-item "><a class="page-link custom-page-link" href="#">1</a>
                                        <span class="sr-only">(current)</span></li>
                                    <li class="page-item"><a class="page-link custom-page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link custom-page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link custom-page-link" href="#" aria-label="Next">
                                                            <span aria-hidden="true"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                                                          </svg></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
                <!-- another row end here  -->

            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush
