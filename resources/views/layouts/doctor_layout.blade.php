<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('partials.doctor.headerScripts')
    @stack('css')
    <title>Dashboard</title>
</head>
<body>
<!-- ===============
dashboard start here
====================== -->
<section class="dashboard">
    <div class="container-fluid">
        <div class="row ">
            <!-- tablet screen start here -->
            <div class="wrapper">
                <section class="nav">
                    @include('partials.doctor.sideBar')
                </section>
                <button type="button" class="play-btn" id="play-btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <!--tablet screen end here -->
            <!--tablet screen end here -->
            <!-- =================================
                history single item start here
            ======================================= -->
                <!-- single item start here  -->
                @yield('content')
                <!-- single item end here  -->
            <!-- =================================
                history single item end here
            ======================================= -->
        </div>
    </div>
</section>
<!-- ===============
dashboard end here
====================== -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- jquery plugins start here  -->
@include('partials.doctor.footerScripts')
@stack('js')
</body>
</html>
