<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    fonts link start here-->
    @include('partials.patient.headerScripts')
    @stack('css')
</head>

<body>
<!-- ===============
dashboard start here
====================== -->
<section class="dashboard">
    <div class="container-fluid" style="padding-left: 0;">
        <div class="row ">
            <!-- single item start here  -->


            <!-- dashboard info end here  -->
        </div>

        <!--          tablet screen start here -->

        <div class="wrapper">
            <section class="nav">
                @include('partials.patient.sideBar')
            </section>
            <button type="button" class="play-btn" id="play-btn">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <main class="main-content">
            </main>

        </div>
        <!-- mobile menu end here -->
        <!--tablet screen end here -->
        @yield('content')
        <!-- dashboard content end here  -->
    </div>

</section>
<!-- ===============
dashboard end here
====================== -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- jquery plugins start here  -->
@include('partials.patient.footerScripts')
@stack('js')
</body>

</html>
