<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <!--META CHARSET -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--PAGE TITLE -->
    <title>Super Admin Login</title>
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- FAVICON -->
    <link rel="shortcut icon" href="http://www.themacart.com/demo/melvin/html/img/favicon.ico">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/application.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('assets/css/inputmask.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.mCustomScrollbar.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('assets/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fullcalendar.print.min.css')}}" media="print">
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/ionicons.min.css')}}" rel="stylesheet" type="text/css">
    <!-- FONTS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,400italic,700'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600">
    <!-- TWITTER META -->
    <meta name="twitter:site" content="@themacart">
    <meta name="twitter:creator" content="@themacart">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Register page">
    <meta name="twitter:description" content="Template for register. Fully responsive and many options.">
    <meta name="twitter:image" content="http://www.themacart.com/demo/melvin/html/img/presentation-1.jpg">
    <!-- OPEN GRAPH META -->
    <meta property="og:url" content="http://www.themacart.com/demo/melvin/html/register.html">
    <meta property="og:title" content="Register page">
    <meta property="og:description" content="Template for register. Fully responsive and many options.">
    <meta property="og:image" content="http://www.themacart.com/demo/melvin/html/img/presentation-1.jpg">
    <meta property="og:type" content="article">
    <meta property="fb:admins" content="1777918968915806" />
    <!-- OTHER META -->
    <meta name="description" content="Template for register. Fully responsive and many options.">
    <meta name="author" content="themacart.com">
    @stack('css')
</head>
<!--BEGIN BODY -->
<!-- change color of link in the page by default -->
<body class="text-gray-600 body-link-gray-800 bg-light">
<div class="row row-eq-columns no-gutters">
    @yield('content')
</div>
<!-- MOMENT JAVASCRIPT -->
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/moment-timezone.min.js')}}"></script>
<script src="{{asset('assets/js/moment-timezone-with-data-2012-2022.min.js')}}"></script>
<!-- JQUERY JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- BLOCK UI FOR RELOAD X-CARD EVENT JAVASCRIPT -->
<script src="{{asset('assets/js/blockUI.min.js')}}"></script>
<!-- I18NEXT TRANSLATION SYSTEM JAVASCRIPT -->
<script src="{{asset('assets/js/i18next.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-i18next.min.js')}}"></script>
<script src="{{asset('assets/js/i18nextXHRBackend.min.js')}}"></script>
<script src="{{asset('assets/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<!--DROPZONE JAVASCRIPT -->
<script src="{{asset('assets/js/dropzone.js')}}"></script>
<script>
    Dropzone.autoDiscover = false;
</script>
<!-- JQUERY DRAG AND DROP JAVASCRIPT -->
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
<!-- CALENDAR JAVASCRIPT -->
<script src="{{asset('assets/js/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/js/gcal.min.js')}}"></script>
<script src="{{asset('assets/js/locale-all.js')}}"></script>
<!-- DATE PICKER JAVASCRIPT -->
<script src="{{asset('assets/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- INPUT MASK JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<!-- IE COVER JAVASCRIPT -->
<script src="{{asset('assets/js/modernizr-custom.min.js')}}"></script>
<!-- SPARKLINE JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.sparkline.min.js')}}"></script>
<!-- BOOTSTRAP AND APPLICATION JAVASCRIPT -->
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/application.js')}}"></script>
<!-- CUSTOM SCROLLBAR JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- CHART.JS JAVASCRIPT -->
<script src="{{asset('assets/js/Chart.bundle.min.js')}}"></script>
<!-- TOASTR FEEDBACK JAVASCRIPT -->
<script src="{{asset('assets/js/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/promise.min.js')}}"></script>
<!-- SWEETALERT FEEDBACK JAVASCRIPT -->
<script src="{{asset('assets/js/sweetalert2.min.js')}}"></script>
@stack('js')
<!--END BODY -->
</body>
</html>
