<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <!--META CHARSET -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--PAGE TITLE -->
    <title>Template for admin style with sidebar</title>
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- FAVICON -->
    <link rel="shortcut icon" href="http://www.themacart.com/demo/melvin/html/img/favicon.ico">
    @include('partials.headerScripts')
    @stack('css')
</head>
<!--BEGIN BODY -->
<!-- change color of link in the page by default -->
<body class="text-gray-600 body-link-gray-800 bg-page app sidebar-mini sidenav-toggled" itemscope itemtype="https://schema.org/WebPage">
<!-- BEGIN HEADER FIXED -->
@include('partials.header')
<!-- END HEADER FIXED -->

<!-- BEGIN SIDEBAR MENU-->
@include('partials.sideBar')
<!-- END SIDEBAR MENU -->

<main class="app-content">
    <!-- BEGIN BREADCRUMB -->
@include('partials.breadCrumb')
<!--end ./ -->
    <!-- END BREADCRUMB -->
    <div class="page">
        <div class="page-body">
            @yield('content')
        </div>
    </div>
    @include('partials.footer')
</main>
@include('partials.collapse_able')
@include('partials.footerScripts')
@stack('js')
<!--END BODY -->
</body>
</html>
