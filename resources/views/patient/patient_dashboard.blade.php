@extends('layouts.patient_layout')
@push('css')
@endpush

@section('content')
    <div class="col-xl-9 offset-xl-3 col-md-12">
        <!-- single item start here  -->
        <div class="w-single-item">


            <div class="w-content d-info-content-btn">
                <h2>Welcome back, Charles</h2>
                <p>It looks like you have no upcoming <br> appointments.would you like to make one? </p>
                <!-- d info content sigle item start here  -->
                <div class="btn-con">
                    <button type="button" class="see-dctr-btn mb-4">See Doctore</button>
                    <div class="upload-btn-wrapper">
                        <button class="btn">Upload a record</button>
                        <input type="file" name="myfile" />
                    </div>
                </div>


                <!-- d-info single item end here  -->
            </div>
            <div class="w-agenda">
                <h3>Agenda</h3>
                <div class="row">
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 ">
                        <div class="agenda-single-item">
                            <div class="agenda-single-title">
                                <p><i class="far fa-calendar"></i><span>02 November</span>
                                </p>

                                <div class="agenda-date">
                                    <p>Metting</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- single item end here  -->

                </div>
                <div class="row">
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 ">
                        <div class="agenda-single-item">
                            <div class="agenda-single-title">
                                <p><i class="far fa-calendar"></i><span>05 November</span>
                                </p>
                                <!-- single item  -->
                                <div class="agenda-date">
                                    <p>Birthday Party</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>

                                <!-- single item end  -->
                                <!-- single item  -->
                                <div class="agenda-date">
                                    <p>Designer Metting</p>
                                    <small>11.00am-1.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>

                                <!-- single item end  -->
                            </div>

                        </div>
                    </div>

                    <!-- single item end here  -->

                </div>


                <div class="row">
                    <!-- single item start here  -->
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 ">
                        <div class="agenda-single-item">
                            <div class="agenda-single-title">
                                <p><i class="far fa-calendar"></i><span>20 November</span>
                                </p>

                                <div class="agenda-date">
                                    <p>Dave Ceremony </p>
                                    <small>2.00pm-5.00pm</small>
                                    <span><i class="far fa-trash-alt"></i></span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- single item end here  -->

                </div>
            </div>
        </div>

        <!-- single item end here  -->
    </div>
@endsection

@push('js')
@endpush
