<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
<!--    fonts link end here-->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('doctor_patient/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('doctor_patient/all.min.css')}}">
<link rel="stylesheet" href="{{asset('doctor_patient/style.css')}}">
<link rel="stylesheet" href="{{asset('doctor_patient/media.css')}}">
<title>Dashboard</title>
