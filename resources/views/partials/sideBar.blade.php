<style>
    .sidebar-mini.sidenav-toggled .sidebar-menu-item{
        font-weight: 350 !important;
    }
    .left-sidebar-dark .treeview-item.active, .left-sidebar-dark .treeview-item:focus, .left-sidebar-dark .treeview-item:hover{
        font-weight: 350 !important;
    }
</style>
<div class="left-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="left-sidebar left-sidebar-dark mCS_disabled" itemscope itemtype="https://schema.org/Organization" id="leftSidebar">
    <ul class="sidebar-menu" itemscope itemtype="https://schema.org/SiteNavigationElement">
        <!-- BEGIN DASHBOARD ITEM -->
        <li class="treeview is-expanded">
            <a class="sidebar-menu-item" href="{{url('/super-admin/dashboard')}}" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-speedometer-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.dashboard"></span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item active" href="{{url('/super-admin/dashboard')}}" itemprop="url" data-i18n="sidebar.dashboard">
                    </a>
                </li>
            </ul>
        </li>
        <!-- END DASHBOARD ITEM -->
        <!-- BEGIN Doctors ITEM -->
        <!-- <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-flag-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.translation"></span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item" href="index.html?lng=fr-FR" itemprop="url" data-i18n="sidebar.frenchdashboard">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="index.html?lng=en-GB" itemprop="url" data-i18n="sidebar.englishdashboard">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="index.html?lng=de-DE" itemprop="url" data-i18n="sidebar.germandashboard">
                    </a>
                </li>
                <li>
                    <a class="treeview-item " href="calendar.html?lng=fr-FR" itemprop="url" data-i18n="sidebar.frenchcalendar">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="calendar.html?lng=en-GB" itemprop="url" data-i18n="sidebar.englishcalendar">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="calendar.html?lng=de-DE" itemprop="url" data-i18n="sidebar.germancalendar">
                    </a>
                </li>
                <li>
                    <a class="treeview-item " href="multi-lang-tables.html?lng=fr-FR" itemprop="url" data-i18n="sidebar.frenchtable">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="multi-lang-tables.html?lng=en-GB" itemprop="url" data-i18n="sidebar.englishtable">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="multi-lang-tables.html?lng=de-DE" itemprop="url" data-i18n="sidebar.germantable">
                    </a>
                </li>
            </ul>
        </li> -->

<li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-flag-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.doctor">Doctors</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item" href="{{url('/super-admin/create-doctor')}}" itemprop="url" data-i18n="sidebar.CreateDoctor">Create Doctor</a>
                </li>
                <li>
                    <a class="treeview-item" href="{{url('/super-admin/all-doctors')}}" itemprop="url" data-i18n="sidebar.AllDoctors">All Doctors</a>
                </li>

            </ul>
        </li>
        <!-- END Doctors ITEM -->
        <!-- Patients ITEM -->
        <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-speedometer-outline"></i>
                <span class="sidebar-menu-label">Patients</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>

                    <a class="treeview-item" href="{{url('/super-admin/create-Patient')}}" itemprop="url" data-i18n="sidebar.Create Patients">Create Patients</a>
                </li>
                <li>
                    <a class="treeview-item" href="{{url('/super-admin/all-patients')}}" itemprop="url" data-i18n="sidebar.All Patients">All Patients</a>
                </li>





            </ul>
        </li>
        <!-- END Patients ITEM -->
        <!-- BEGIN Invoice ITEM -->
        <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-email-outline"></i>
                <span class="sidebar-menu-label">
          <span data-i18n="sidebar.Invoice">Invoices</span>

        </span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="{{url('/super-admin/create-invoice')}}" itemprop="url" data-i18n="sidebar.CreateInvoice">Create Invoice</a>
                </li>
                <li>
                    <a class="treeview-item " href="{{url('/super-admin/all-invoices')}}" itemprop="url" data-i18n="sidebar.AllInvoices">All Invoices</a>
                </li>
            </ul>
        </li>
        <!-- END Invoice ITEM -->
        <!-- BEGIN Multi reports ITEM -->
        <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-chatboxes-outline"></i>
                <span class="sidebar-menu-label">
          <span data-i18n="sidebar.MultiReports">Multi Reports</span>

        </span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="chat.html" itemprop="url" data-i18n="sidebar.CreateMultiReport">Create Multi Report</a>
                </li>
                <li>
                    <a class="treeview-item " href="chat.html" itemprop="url" data-i18n="sidebar.AllMultiReports">All Multi Reports</a>
                </li>
            </ul>
        </li>
        <!-- END Multi reports ITEM -->
        <!-- BEGIN Questionnaires ITEM -->
        <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-calendar-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.Questionnaires">Questionnaires</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="{{route('questionnaire_index')}}" itemprop="url" data-i18n="sidebar.AllQuestionnaires">Questionnaires</a>
                </li>
            </ul>
        </li>
        <!-- END Questionnaires ITEM -->
        <!-- BEGIN Pages ITEM -->
       <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-paw-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.Pages">Pages</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="{{url('/super-admin/create-new-page')}}" itemprop="url" data-i18n="sidebar.CreateNewPages">Create New Pages</a>
                </li>
                <li>
                    <a class="treeview-item " href="{{url('/super-admin/all-pages')}}" itemprop="url" data-i18n="sidebar.AllPages">All Pages</a>
                </li>
            </ul>
        </li>
        <!-- END Pages ITEM -->
        <!-- BEGIN FAQ ITEM -->
        <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-cart-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.FAQFrondend">FAQ Frondend</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="shopping-cart.html" itemprop="url" data-i18n="sidebar.FAQFrondend">FAQ Frondend</a>
                </li>

            </ul>
        </li>
        <!-- END FAQ ITEM -->
        <!-- BEGIN Booking appointment form ITEM -->
        <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-pulse"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.BookingAppointment">Booking Appointment</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item  " href="bar-charts.html" itemprop="url" data-i18n="sidebar.CreateBookingAppointmentForm">Create Booking Appointment Form</a>
                </li>
                <li>
                    <a class="treeview-item " href="line-charts.html" itemprop="url" data-i18n="sidebar.AllBookingAppointmentForm">All Booking Appointment Form</a>
                </li>

            </ul>
        </li>
        <!-- END Booking appointment form ITEM -->
        <!-- BEGIN tickets ITEM -->
        <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-list-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.SupportTickets">Support Tickets</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="basic-tables.html" itemprop="url" data-i18n="sidebar.AllTickets">All Tickets</a>
                </li>
            </ul>
        </li>
        <!-- END tickets ITEM -->
        <!-- BEGIN Website settings ITEM -->
        <li class="treeview is-expanded">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-paper-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.WebsiteSettings">Website Settings</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="form-components.html" itemprop="url" data-i18n="sidebar.General">General</a>
                </li>
                <li>
                    <a class="treeview-item " href="{{route('seo-details')}}" itemprop="url" data-i18n="sidebar.SEO">SEO</a>
                </li>
                <li>
                    <a class="treeview-item " href="summernote.html" itemprop="url" data-i18n="sidebar.ZoomIntegration">Zoom Integration</a>
                </li>
                <li>
                    <a class="treeview-item " href="dropzone.html" itemprop="url" data-i18n="sidebar.Stripe">Stripe</a>
                </li>
                <li>
                    <a class="treeview-item " href="inputmask.html" itemprop="url" data-i18n="sidebar.Localisation">Localisation</a>
                </li>
                <li>
                    <a class="treeview-item " href="bootstrap-slider.html" itemprop="url" data-i18n="sidebar.Twilio">Twilio</a>
                </li>
                <li>
                    <a class="treeview-item " href="ion-range-slider.html" itemprop="url" data-i18n="sidebar.Messages">Messages</a>
                </li>
            </ul>
        </li>
        <!-- END Website settings ITEM -->
        <!-- BEGIN FEEDBACK ITEM -->
        <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-bell-outline"></i>
                <span class="sidebar-menu-label">
          <span data-i18n="sidebar.Email Templates">Email Templates</span>

        </span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="toastr.html" itemprop="url" data-i18n="sidebar.Activcation Email">Activcation Email</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.New Patient Registration">New Patient Registration</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.New Doctor Registration">New Doctor Registration</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.New Booking Appoiment">New Booking Appoiment</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.Pharamy Emails">Pharamy Emails</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.Lost Password Email">Lost Password Email</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.Recover Password Email">Recover Password Email</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.Revcive Payments">Revcive Payments</a>
                </li>
                <li>
                    <a class="treeview-item " href="sweetalert.html" itemprop="url" data-i18n="sidebar.Invoice Email">Invoice Email</a>
                </li>
            </ul>
        </li>
        <!-- END FEEDBACK ITEM -->
        <!-- BEGIN Settings ITEM -->
       <li class="treeview">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-location-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.Settings">Settings</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="clickable-maps.html" itemprop="url" data-i18n="sidebar.Profile Setting">Profile Setting</a>
                </li>
                <li>
                    <a class="treeview-item " href="google-maps.html" itemprop="url" data-i18n="sidebar.Password">Password</a>
                </li>
            </ul>
        </li>
        <!-- END Settings ITEM -->
        <!-- BEGIN UI ELEMENTS ITEM -->
        <!-- <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-barcode-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.uielements">Ui Elements</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item " href="color-shades.html" itemprop="url">
                        Color Shades</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-alerts.html" itemprop="url">
                        Alerts</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-badges.html" itemprop="url">
                        Badges</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-boxes.html" itemprop="url">
                        Boxes</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-buttons.html" itemprop="url">
                        Buttons</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-cards.html" itemprop="url">
                        Cards</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-collapse.html" itemprop="url">
                        Collapse</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-lines.html" itemprop="url">
                        Lines</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-navs.html" itemprop="url">
                        Navigation</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-pagination.html" itemprop="url">
                        Pagination</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-popovers.html" itemprop="url">
                        Popovers</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-progress.html" itemprop="url">
                        Progress Bars</a>
                </li>
                <li>
                    <a class="treeview-item " href="shortcode-tooltips.html" itemprop="url">
                        Tooltips</a>
                </li>
            </ul>
        </li> -->
        <!-- END UI ELEMENTS ITEM -->
        <!-- BEGIN EXTRA PAGES ITEM -->
       <!--  <li class="treeview ">
            <a class="sidebar-menu-item" href="#" data-toggle="treeview">
                <i class="sidebar-menu-icon ion ion-ios-albums-outline"></i>
                <span class="sidebar-menu-label" data-i18n="sidebar.extrapages"></span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item" href="login.html" itemprop="url" data-i18n="sidebar.login">
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="register.html" itemprop="url" data-i18n="sidebar.register">
                    </a>
                </li>
                <li>
                    <a class="treeview-item " href="error-404.html" itemprop="url" data-i18n="sidebar.error404">
                    </a>
                </li>
                <li>
                    <a class="treeview-item " href="under-construction.html" itemprop="url" data-i18n="sidebar.underconstruction">
                    </a>
                </li>
                <li class="pb-5">
                    <a class="treeview-item " href="blank.html" itemprop="url" data-i18n="sidebar.blankpage">
                    </a>
                </li> -->
                <!-- END EXTRA PAGES ITEM -->
            </ul>
        </li>
    </ul>
</aside>
