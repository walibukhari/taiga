<!-- MOMENT JAVASCRIPT -->
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/moment-timezone.min.js')}}"></script>
<script src="{{asset('assets/js/moment-timezone-with-data-2012-2022.min.js')}}"></script>
<!-- JQUERY JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- BLOCK UI FOR RELOAD X-CARD EVENT JAVASCRIPT -->
<script src="{{asset('assets/js/blockUI.min.js')}}"></script>
<!-- I18NEXT TRANSLATION SYSTEM JAVASCRIPT -->
<script src="{{asset('assets/js/i18next.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-i18next.min.js')}}"></script>
<script src="{{asset('assets/js/i18nextXHRBackend.min.js')}}"></script>
<script src="{{asset('assets/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<!--DROPZONE JAVASCRIPT -->
<script src="{{asset('assets/js/dropzone.js')}}"></script>
<script>
    Dropzone.autoDiscover = false;
</script>
<!-- JQUERY DRAG AND DROP JAVASCRIPT -->
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
<!-- CALENDAR JAVASCRIPT -->
<script src="{{asset('assets/js/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/js/gcal.min.js')}}"></script>
<script src="{{asset('assets/js/locale-all.js')}}"></script>
<!-- DATE PICKER JAVASCRIPT -->
<script src="{{asset('assets/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- INPUT MASK JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<!-- IE COVER JAVASCRIPT -->
<script src="{{asset('assets/js/modernizr-custom.min.js')}}"></script>
<!-- SPARKLINE JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.sparkline.min.js')}}"></script>
<!-- BOOTSTRAP AND APPLICATION JAVASCRIPT -->
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/application.js')}}"></script>
<!-- CUSTOM SCROLLBAR JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- CHART.JS JAVASCRIPT -->
<script src="{{asset('assets/js/Chart.bundle.min.js')}}"></script>
<!-- FLOT CHART JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/flotchartplugins/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('assets/flotchartplugins/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('assets/flotchartplugins/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('assets/flotchartplugins/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('assets/flotchartplugins/jquery.flot.pie.min.js')}}"></script>
<!-- TOASTR FEEDBACK JAVASCRIPT -->
<script src="{{asset('assets/js/toastr.min.js')}}"></script>
<script src="{{asset('assets/js/promise.min.js')}}"></script>
<!-- SWEETALERT FEEDBACK JAVASCRIPT -->
<script src="{{asset('assets/js/sweetalert2.min.js')}}"></script>
<!-- MAPS JAVASCRIPT -->
<script src="{{asset('assets/js/jquery.vmap.min.js')}}"></script>
<script src="{{asset('assets/js/gmaps.min.js')}}"></script>

<script src="{{asset('assets/maps/jquery.vmap.usa.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>
<script>

    var data = [],
        totalPoints = 300;
    var updateInterval = 300;
    function getRandomData() {

        if (data.length > 0)
            data = data.slice(1);

        // Do a random walk

        while (data.length < totalPoints) {

            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;

            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }

            data.push(y);
        }

        // Zip the generated y values with the x values

        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }

        return res;
    }

    var plot = $.plot("#flotchartrealtime", [getRandomData()], {
        grid: {
            borderColor: '#f3f3f3',
            borderWidth: 1,
            tickColor: '#f3f3f3'
        },
        series: {
            shadowSize: 0, // Drawing is faster without shadows
            color: '#448AFF'
        },
        lines: {
            fill: true, //Converts the line chart to area chart
            color: '#448AFF'
        },
        yaxis: {
            min: 0,
            max: 100,
            show: true
        },
        xaxis: {
            show: true
        }
    });

    function update() {

        plot.setData([getRandomData()]);

        // Since the axes don't change, we don't need to call plot.setupGrid()

        plot.draw();
        setTimeout(update, updateInterval);
    }

    update();

    function escapeXml(string) {
        return string.replace(/[<>]/g, function (c) {
            switch (c) {
                case '<': return '\u003c';
                case '>': return '\u003e';
            }
        });
    }
    var pins = {
        mo: escapeXml('<div class="map-pin pin-black"><span>MO</span></div>'),
        ca: escapeXml('<div class="map-pin pin-black"><span>CA</span></div>'),
        or: escapeXml('<div class="map-pin pin-black"><span>OR</span></div>')
    };
    jQuery(document).ready(function () {
        jQuery('#vmap').vectorMap({
            map: 'usa_en',
            pins: pins,
            color: '#fff',
            pinMode: 'content',
            backgroundColor: '#ffffff',
            color: '#448AFF',
            hoverOpacity: 0.7,
            selectedColor: '#ffffff',
            enableZoom: false,
            zoomButtons: false,
            showTooltip: true,
            scaleColors: ['#448AFF', '#254b8a'],
            onRegionClick: function (element, code, region) {
                var message = 'You clicked "'
                    + region
                    + '" which has the code: '
                    + code.toUpperCase();

                toastr.success(message, 'Success!');
            },
            normalizeFunction: 'polynomial'
        });
    });

    var deviceData = {
        labels: ['Ios', 'Android', 'Blackberry', 'Symbian', 'Others'],
        datasets: [{
            label: 'Device',
            backgroundColor: [
                window.chartColors.octonary,
                window.chartColors.info,
                window.chartColors.quinary,
                window.chartColors.quaternary,
                window.chartColors.primary,
            ],
            data: [30, 20, 10, 5, 40]
        }]

    };

    var color = Chart.helpers.color;
    var cpuChartData = {
        labels: ['01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
        datasets: [{
            label: 'Cpu Usage',
            backgroundColor: color(window.chartColors.primary).alpha(0.5).rgbString(),
            borderColor: window.chartColors.primary,
            borderWidth: 1,
            data: [2, 28, 44, 74, 96, 16, 88, 68, 24, 32, 48, 68, 47, 26, 47, 58, 67, 89]
        }]

    };


    var cpuTasksData = {
        labels: ['01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
        datasets: [{
            label: 'Hourly Stats',
            backgroundColor: color(window.chartColors.white).alpha(0.5).rgbString(),
            borderColor: window.chartColors.white,
            borderWidth: 1,
            data: [2, 28, 44, 74, 96, 16, 88, 68, 24, 32, 48, 68, 47, 26, 47, 58, 67, 89]
        }]
    };

    var d1 = [];
    for (var i = 0; i <= 10; i += 1) {
        d1.push([i, parseInt(Math.random() * 30)]);
    }

    var d2 = [];
    for (var i = 0; i <= 10; i += 1) {
        d2.push([i, parseInt(Math.random() * 30)]);
    }

    var d3 = [];
    for (var i = 0; i <= 10; i += 1) {
        d3.push([i, parseInt(Math.random() * 30)]);
    }

    var stack = 0,
        bars = false,
        lines = true,
        steps = false;

    function plotWithOptions() {
        $.plot("#stackingchart", [d1, d2, d3], {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.6
                }
            },
            grid: {
                borderColor: '#f3f3f3',
                borderWidth: 1,
                tickColor: '#f3f3f3'
            },
            colors:  ['#448AFF','#536DFE', '#40C4FF']
        });
    }

    plotWithOptions();

    window.onload = function () {

        var ctx1 = document.getElementById('canvas-cpu').getContext('2d');
        window.myBar = new Chart(ctx1, {
            type: 'bar',
            data: cpuChartData,
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        display: false,
                        gridLines: {
                            display: false
                        },
                    }],
                    xAxes: [{
                        display: false,
                        gridLines: {
                            display: false
                        },
                    }]
                }
            }
        });
        var ctx2 = document.getElementById('devicechart').getContext('2d');
        window.myPie = new Chart(ctx2, {
            type: 'pie',
            data: deviceData,
            options: {
                responsive: true,
                legend: {
                    display: false,
                }
            }
        });

        var ctx3 = document.getElementById('canvas-stats').getContext('2d');
        window.myBar = new Chart(ctx3, {
            type: 'bar',
            data: cpuTasksData,
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        display: false,
                        gridLines: {
                            display: false
                        },
                    }],
                    xAxes: [{
                        display: false,
                        gridLines: {
                            display: false
                        },
                    }]
                }
            }
        });

    };

</script>
