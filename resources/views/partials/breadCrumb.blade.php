<div class="d-none d-md-flex bg-white d-print-none breadcrumb-container">
    <div class="row no-gutters w-100">
        <div class="col-md-6 d-flex justify-content-start">
            <div class="my-auto d-flex flex-row">
                <div class="my-auto text-dark">
                    <i class="size-24 ion ion-ios-speedometer-outline"></i>
                </div>
                <div class="ml-4 my-auto  border-left border-dark">&nbsp;
                </div>
                <div class="my-auto ml-3 font-weight-bold text-dark">Mini Sidebar</div>
            </div>
        </div>
        <div class="col-md-6 d-flex">
            <ul class="ml-md-auto breadcrumb bg-white my-auto p-0">
                <li class="breadcrumb-item">
                    <i class="ion size-16 ion-ios-home-outline text-dark mr-2"></i>
                    <a href="#" class="text-dark font-weight-light" data-i18n="breadcrumb.home"></a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#" class="text-dark font-weight-light mr-2">NETBLUE</a>
                </li>
                <li class="breadcrumb-item active font-weight-light m-0 p-0" aria-current="page">Mini Sidebar
                </li>
            </ul>
        </div>
        <!-- end ./col-md-6 -->
    </div>
    <!-- end ./row -->
</div>
