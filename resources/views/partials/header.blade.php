<header class="app-header app-header-white">
  <span itemscope itemtype="https://schema.org/Organization">
    <!-- Begin brand meta props for seo -->
      <!-- change absolute url in gulp.config or here -->
    <meta itemprop="logo" content="http://www.themacart.com/demo/melvin/html/img/logo-mobile-light.png">
    <meta itemprop="name" content="Themacart">
      <!-- End seo brand meta -->
    <span class="d-none d-md-flex h-100">
      <a class="app-header-brand my-auto mx-auto" href="#" itemprop="url">
        <img src="{{url('/')}}/assets/img/logo-desktop-light.png" height="25px" width="90px" class="img-fluid" alt="Themacart premium template" id="desktopimage">
      </a>
    </span>
    <span class="d-flex d-md-none h-100 px-1">
      <a class="app-header-brand my-auto mx-auto" href="#" itemprop="url">
        <img src="{{url('/')}}/assets/img/logo-mobile-light.png" class="img-fluid" alt="" id="mobileimage">
      </a>
    </span>
  </span>
    <!-- Sidebar toggle button-->
    <button class="app-header-toggler" type="button" data-toggle="sidebar" aria-label="Hide Sidebar">
        <span class="app-header-toggler-icon"></span>
    </button>
    <!-- Navbar Right Menu-->
    <ul class="list-inline app-header-nav" itemscope itemtype="https://schema.org/WebSite">
       <!--  <li class="app-header-form d-none d-md-flex">
            <meta itemprop="url" content="http://www.themacart.com/demo/melvin/html">
            <form itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction">
                <meta itemprop="target" content="http://www.themacart.com/demo/melvin/html/?q={query}">
                <input itemprop="query-input" class="app-header-form-input" type="search" data-i18n="[placeholder]input.placeholder" name="query">
                <button class="app-header-form-button">
                    <i class="ion ion-ios-search h5"></i>
                </button>
            </form>
        </li> -->
        <li class="list-inline-item dropdown">
            <a class="nav-link d-flex" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="small text-uppercase font-weight-light my-auto">
          <span id="langFlag"></span>
          <span class="d-none d-md-inline-flex" id="langText" data-i18n="header.language"></span>
        </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow-3 py-0 min-width-300">
                <a class="dropdown-item font-weight-light lang" data-lang="en-GB" itemprop="url" href="javascript:void(0);">
                    <img src="{{url('/')}}/assets/img/flags/GB.png" alt="English translate" class=" mr-2"> English
                </a>
                <a class="dropdown-item font-weight-light lang" data-lang="fr-FR" itemprop="url" href="javascript:void(0);">
                    <img src="{{url('/')}}/assets/img/flags/FR.png" alt="French translate" class=" mr-2"> French
                </a>
                <a class="dropdown-item font-weight-light lang" data-lang="de-DE" itemprop="url" href="javascript:void(0);">
                    <img src="{{url('/')}}/assets/img/flags/DE.png" alt="German translate" class=" mr-2"> German
                </a>
            </div>
        </li>
        <!-- User Menu-->
        <li class="list-inline-item dropdown">
            <a class="nav-link d-flex" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
        <span class="my-auto ie-cover-fit" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
          <img class="img-sq-35 rounded-circle img-profile-light" src="{{url('/')}}/assets/img/60x60.png" alt="User Image">
          <meta itemprop="url" content="img/60x60.png">
        </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-0 min-width-300 shadow-3">
                <div class="d-flex flex-column rounded">
                    <div class="px-4 py-2 bg-white border-bottom">
                        <div itemscope itemtype="https://schema.org/Person" class="media">
            <span class="ie-cover-fit d-flex mr-3" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                <img class="img-sq-50 rounded-circle img-profile-light" src="{{url('/')}}/assets/img/60x60.png" alt="User Image">
                <meta itemprop="url" content="img/60x60.png">
            </span>
                            <i class="bullet bullet-avatar bullet-warning"></i>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span itemprop="name" class="font-weight-bold h5">John Frank</span>
                                </p>
                                <span itemprop="email" class="h6 font-weight-light">
                    john.frank@example.com
                </span>
                            </div>
                        </div>
                    </div>
                    <div class="p-2">
                        <ul class="list-group list-group-flush w-100 m-0 p-0">
                            <li class="list-group-item list-group-item-action w-100 p-0 border-0">
                                <a href="#" class="d-flex w-100 px-4">
                                    <i class="ion size-21 ion-ios-toggle-outline my-auto mr-3"></i>
                                    <span data-i18n="header.settings"></span>
                                </a>
                            </li>
                            <li class="list-group-item list-group-item-action w-100 p-0 border-0">
                                <a href="profile.html" class="d-flex w-100 px-4">
                                    <i class="ion size-24 ion-ios-person-outline my-auto mr-3"></i>
                                    <span data-i18n="header.profile"></span>
                                </a>
                            </li>
                            <li class="list-group-item list-group-item-action w-100 p-0 border-0">
                                <a href="project-details.html" class="d-flex w-100 px-4">
                                    <i class="ion size-21 ion-ios-albums-outline my-auto mr-3"></i>
                                    <span data-i18n="header.project"></span>
                                </a>
                            </li>
                            <li class="list-group-item list-group-item-action w-100 p-0 border-0">
                                <a href="inbox.html" class="d-flex w-100 px-4">
                                    <i class="ion size-24 ion-ios-email-outline my-auto mr-3"></i>
                                    <span data-i18n="header.inbox"></span>
                                    <span class="numberCircle bg-primary text-white ml-auto my-auto ">12</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="px-4 bg-white border-top">
                        <div class="row no-gutters">
                            <div class="col-6 text-center p-2">
                                <a href="#" class="btn btn-primary btn-block text-white">
                                    <span data-i18n="header.newaccount"></span>
                                </a>
                            </div>
                            <div class="col-6 text-center p-2">
                                <a href="{{route('logoutT')}}" class="btn btn-success btn-block text-white">
                                    <span data-i18n="header.logout"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <!--Notification Menu-->
        <li class="list-inline-item dropdown">
            <a class="nav-link d-flex" href="#">
        <span class="my-auto">
          <i class="ion size-24 ion-android-notifications-none mr--1"></i>
        </span>
                <span class="my-auto">
          <span class="numberCircle bg-danger text-white mt--1">6</span>
        </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-0 min-width-300 shadow-3">
                <div class="d-flex flex-column align-items-stretch h-100">
                    <div class="mb-auto">
                        <div class="p-4 text-center ">
                            <span class="font-weight-bold text-uppercase h6 my-0" data-i18n="header.newnots"></span>
                        </div>
                    </div>
                    <div class="mCustomScrollbar h-75" data-mcs-theme="dark-thin">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media">
                    <span class="d-flex mr-3">
                        <span class="numberCircle bg-warning p-3">
                            <i class="ion ion-ios-people size-24 mt-0 text-white"></i>
                        </span>
                    </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            15 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-erlenmeyer-flask size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            21 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quaternary p-3">
                                    <i class="ion ion-eye size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            35 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-success p-3">
                                    <i class="ion ion-flash size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            47 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-info p-3">
                                    <i class="ion ion-funnel size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 hour ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-danger p-3">
                                    <i class="ion ion-hammer size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 day ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quinary p-3">
                                    <i class="ion ion-images size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            15 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-warning p-3">
                                    <i class="ion ion-ios-analytics size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            21 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-ios-bell size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            35 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-danger p-3">
                                    <i class="ion ion-ios-briefcase size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            47 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quinary p-3">
                                    <i class="ion ion-ios-camera size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 hour ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-ios-people size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 day ago
                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="mt-auto">
                        <div class="p-4 text-center">
                            <a href="#" class="h6 text-center" data-i18n="header.seeallnots"></a>
                        </div>
                    </div>
                </div>

            </div>
        </li>

        <!-- Begin message -->
        <!-- <li class="list-inline-item dropdown">
            <a class="nav-link d-flex" href="#">
          <span class="my-auto">
            <i class="ion size-24 ion-at  mr--1"></i>
          </span>
                <span class="my-auto">
            <span class=" numberCircle bg-info text-white mt--1">2</span>
          </span>
            </a>
        </li> -->
        <!-- End message -->

        <!--Settings Menu-->
        <li class="list-inline-item dropdown">
            <a class="nav-link d-flex" href="#">
          <span class="my-auto">
            <i class="ion size-24 ion-gear-a"></i>
          </span>
            </a>
        </li>

    </ul>
</header>
