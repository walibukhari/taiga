<div class="collapse-sidebar bg-banner border-left text-white ">
    <div class="collapse-sidebar-content d-flex flex-column">
        <button class="ml-3 mr-auto btn btn-link" type="button" data-toggle="collapsesidebar" aria-label="Hide Sidebar">
            <i class="ion ion-android-arrow-forward size-24"></i>
        </button>
        <!-- Nav tabs -->
        <ul class="nav nav-pills-primary mb-4" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active text-white" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications"
                   aria-selected="false">Notification</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Mails</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content collapse-tab-content">
            <div class="tab-pane active h-100" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
                <div class="d-flex flex-column align-items-stretch h-100">
                    <div class="mb-auto">
                        <div class="p-4 text-center ">
                            <span class="font-weight-bold text-uppercase h6 my-0" data-i18n="header.newnots"></span>
                        </div>
                    </div>
                    <div class="mCustomScrollbar h-75" data-mcs-theme="dark-thin">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media">
                    <span class="d-flex mr-3">
                        <span class="numberCircle bg-warning p-3">
                            <i class="ion ion-ios-people size-24 mt-0 text-white"></i>
                        </span>
                    </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            15 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-erlenmeyer-flask size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            21 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quaternary p-3">
                                    <i class="ion ion-eye size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            35 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-success p-3">
                                    <i class="ion ion-flash size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            47 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-info p-3">
                                    <i class="ion ion-funnel size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 hour ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-danger p-3">
                                    <i class="ion ion-hammer size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 day ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quinary p-3">
                                    <i class="ion ion-images size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            15 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-warning p-3">
                                    <i class="ion ion-ios-analytics size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            21 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-ios-bell size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            35 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-danger p-3">
                                    <i class="ion ion-ios-briefcase size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            47 mns ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-quinary p-3">
                                    <i class="ion ion-ios-camera size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 hour ago
                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="d-flex mr-3">
                                <span class="numberCircle bg-primary p-3">
                                    <i class="ion ion-ios-people size-24 text-white"></i>
                                </span>
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small pull-right">
                            1 day ago
                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="mt-auto">
                        <div class="p-4 text-center">
                            <a href="#" class="h6 text-center" data-i18n="header.seeallnots"></a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane h-100" id="messages" role="tabpanel" aria-labelledby="messages-tab">
                <div class="d-flex flex-column align-items-stretch h-100">
                    <div class="mb-auto">
                        <div class="p-4 text-center ">
                            <span class="font-weight-bold text-uppercase h6 my-0" data-i18n="header.newnots"></span>
                        </div>
                    </div>
                    <div class="mCustomScrollbar h-75" data-mcs-theme="dark-thin">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-info size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-warning"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @frankwilliams
                            </span>
                                        <span class="font-weight-light small pull-right">
                                15 mns ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-success size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-danger"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @mollysims
                            </span>
                                        <span class="font-weight-light small pull-right">
                                21 mns ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-danger size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-primary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @johndoe
                            </span>
                                        <span class="font-weight-light small pull-right">
                                35 mns ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-warning size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-quinary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @frankwilliams
                            </span>
                                        <span class="font-weight-light small pull-right">
                                47 mns ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-info size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-quaternary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @nellyfrank
                            </span>
                                        <span class="font-weight-light small pull-right">
                                1 hour ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                        <span class="text-success size-32 d-flex mr-3 ie-cover-fit">
                            <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                        </span>
                                    <i class="bullet bullet-avatar bullet-success"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                @bertkramp
                            </span>
                                        <span class="font-weight-light small pull-right">
                                1 day ago
                            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-info size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <i class="bullet bullet-avatar bullet-warning"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @frankwilliams
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    15 mns ago
                                </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-success size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <i class="bullet bullet-avatar bullet-danger"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @mollysims
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    21 mns ago
                                </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-danger size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <i class="bullet bullet-avatar bullet-primary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @johndoe
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    35 mns ago
                                </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-warning size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <i class="bullet bullet-avatar bullet-warning"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @frankwilliams
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    47 mns ago
                                </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-info size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <i class="bullet bullet-avatar bullet-primary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @nellyfrank
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    1 hour ago
                                </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
                            <span class="text-success size-32 d-flex mr-3 ie-cover-fit">
                                <img src="{{url('/')}}/assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-dark" alt="User Avatar">
                            </span>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                        </p>
                                        <span class="font-weight-light small">
                                    @bertkramp
                                </span>
                                        <span class="font-weight-light small pull-right">
                                    1 day ago
                                </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="mt-auto">
                        <div class="p-4 text-center">
                            <a href="#" class="h6 text-center" data-i18n="header.seeallnots"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane h-100" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                <div class="d-flex flex-column align-items-stretch h-100">
                    <div class="p-2 mCustomScrollbar h-100" data-mcs-theme="dark-thin">

                        <h4 class="mb-4 border-bottom">Header Color</h4>
                        <div class="row size-12">
                            <div class="col-md-3">
                                <a href="#" class="changeHeaderBackground" data-background="white" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)">
                                    <div>
                                        <span class="bg-gray-200" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">White</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="changeHeaderBackground" data-background="dark">
                                    <div>
                                        <span class="bg-dark" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span class style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Dark</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="changeHeaderBackground" data-background="primary">
                                    <div>
                                        <span class="bg-primary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Primary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="changeHeaderBackground" data-background="danger">
                                    <div>
                                        <span class="bg-danger" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Danger</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="success">
                                    <div>
                                        <span class="bg-success" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Success</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="info">
                                    <div>
                                        <span class="bg-info" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Info</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="warning">
                                    <div>
                                        <span class="bg-warning" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Warning</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="quaternary">
                                    <div>
                                        <span class="bg-quaternary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Quaternary</p>
                            </div>


                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="quinary">
                                    <div>
                                        <span class="bg-quinary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Quinary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="senary">
                                    <div>
                                        <span class="bg-senary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Senary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="septenary">
                                    <div>
                                        <span class="bg-septenary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Septenary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="octonary">
                                    <div>
                                        <span class="bg-primary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Octonary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="nonary">
                                    <div>
                                        <span class="bg-nonary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Nonary</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeHeaderBackground" data-background="denary">
                                    <div>
                                        <span class="bg-denary" style="display:block; width: 100%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px;background: #4B64A6;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Denary</p>
                            </div>
                        </div>

                        <h4 class=" mt-5 mb-4 border-bottom">Sidebar Color</h4>
                        <div class="row size-12">
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeSidebarBackground" data-background="dark">
                                    <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                        <span class="bg-gray-200" style="display:block; width: 20%; float: left; height: 7px;"></span>
                                        <span class="bg-gray-200" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span class="bg-dark" style="display:block; width: 20%; float: left; height: 20px;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">Dark</p>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="mb-2 changeSidebarBackground" data-background="white">
                                    <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                        <span class="bg-gray-200" style="display:block; width: 20%; float: left; height: 7px;"></span>
                                        <span class="bg-gray-200" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                    </div>
                                    <div>
                                        <span class="bg-gray-200 border-dark border-right" style="display:block; width: 20%; float: left; height: 20px;"></span>
                                        <span class="bg-gray-100" style="display:block; width: 80%; float: left; height: 20px;"></span>
                                    </div>
                                </a>
                                <p class="text-center m-0">White</p>
                            </div>
                        </div>
                        <h4 class=" mt-5 mb-4 border-bottom">Sidebar Layout</h4>
                        <p>Toggle the left sidebar's state (open or collapse)</p>
                        <button class="btn btn-pill btn-primary mb-4" data-toggle="sidebar">Toggle Sidebar</button>
                        <p>Sowh hide Sidebar in minimum size</p>
                        <button class="btn btn-pill btn-primary mb-4" data-toggle="toggleSidebarMini">Toggle Sidebar mini</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
