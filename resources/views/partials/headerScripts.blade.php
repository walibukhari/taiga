<!-- CSS -->
<link rel="stylesheet" href="{{asset('assets/css/application.min.css')}}" media="all">
<link rel="stylesheet" href="{{asset('assets/css/inputmask.css')}}" media="all">
<link rel="stylesheet" href="{{asset(('assets/css/jquery.mCustomScrollbar.min.css'))}}" media="all">
<link rel="stylesheet" href="{{asset('assets/css/tempusdominus-bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/fullcalendar.print.min.css')}}" media="print">
<link rel="stylesheet" href="{{asset('assets/css/jqvmap.min.css')}}" media="all">
<link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/ionicons.min.css')}}" rel="stylesheet" type="text/css">
<!-- FONTS -->
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,400italic,700'>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600">
<!-- TWITTER META -->
<meta name="twitter:site" content="@themacart">
<meta name="twitter:creator" content="@themacart">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="Template for admin style with sidebar">
<meta name="twitter:description" content="Template for admin with navbar and a sidebar. Fully responsive and many options.">
<meta name="twitter:image" content="http://www.themacart.com/demo/melvin/html/img/presentation-1.jpg">
<!-- OPEN GRAPH META -->
<meta property="og:url" content="http://www.themacart.com/demo/melvin/html/index.html">
<meta property="og:title" content="Template for admin style with sidebar">
<meta property="og:description" content="Template for admin with navbar and a sidebar. Fully responsive and many options.">
<meta property="og:image" content="http://www.themacart.com/demo/melvin/html/img/presentation-1.jpg">
<meta property="og:type" content="article">
<meta property="fb:admins" content="1777918968915806" />
<!-- OTHER META -->
<meta name="description" content="Template for admin with navbar and a sidebar. Fully responsive and many options.">
<meta name="author" content="themacart.com">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
<link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
