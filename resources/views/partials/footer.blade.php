<footer class="footer border-top d-print-none" id="footer">
    <div class="row">
        <div class="col-md-6 d-flex">
            <div class="my-auto text-gray-600 small text-uppercase">
                <span data-i18n="dashboard.allright"></span>
                <a href="https://netblue.nl/" class="text-dark">Netblue IT</a>
            </div>
        </div>
        <div class="col-md-6 d-flex">
            <div class="ml-md-auto my-auto text-gray-600 small text-uppercase">
                <span data-i18n="dashboard.designby"></span>
                <a href="https://netblue.nl/" class="text-dark">Netblue</a>
            </div>
        </div>
    </div>
</footer>

