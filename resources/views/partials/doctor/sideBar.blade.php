<nav class="main-nav" style="background-color: #3576AF;;">
    <div class="d-flex justify-content-end">
        <button type="button" class="cross-btn">
            &times;
        </button>
    </div>
    <div class="logo-con">
        <a href="index.html">
            <div class="logo-img-con">
                <img src="{{asset('doctor_patient/image/dashboard-logo.png')}}" alt="logo-img" class="img-fluid" />
            </div>
        </a>
    </div>
    <div class="user-profile">
        <a href="#" class="d-flex align-items-start">
            <div class="user-profile-img-con">
                <img src="{{asset('doctor_patient/image/man.jpg')}}" alt="user-profile" class="img-fluid" />
            </div>
            <span class="d-inline-block mt-2 ml-3 user-name">hi, charles</span>
        </a>
    </div>
    <div class="nav-links">
        <ul class="nav-links-ul">
            <li>
                <a href="#">
                    <i class="fa fa-calendar" aria-hidden="true"></i> Appointments
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="far fa-hospital" aria-hidden="true"></i>Virtual Care
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-upload" aria-hidden="true"></i> Questionnaires
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-medkit" aria-hidden="true"></i> Files
                </a>
            </li>
        </ul>
        <button type="button" class="see-dctr-btn">Manually Add Appointment</button>
    </div>
    <div class="bottom-links">
        <ul>
            <li>
                <a href="#" class="w-100 d-flex justify-content-between">
                    My account settings
                    <i class="fa fa-cog" aria-hidden="true"></i>
                </a>
            </li>
            <li><a href="#"> membership </a></li>
            <li><a href="#"> Help & Getting started </a></li>
            <li><a href="#"> Notications </a></li>
            <li>
                <form action="{{route('userLogout')}}" method="post">
                    @csrf
                    <input type="hidden" value="doctor" name="guard" />
                    <button style="color:#fff; font-weight: normal; background-color: transparent;border: 0px;" type="submit">sign out</button>
                </form>
            </li>
        </ul>
    </div>
</nav>
