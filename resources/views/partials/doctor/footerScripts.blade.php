<script src="{{asset('doctor_patient/jquery.filterizr.min.js ')}}"></script>
<script src="{{asset('doctor_patient/jquery-3.4.1.min.js ')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin="anonymous "></script>
<script src="{{asset('doctor_patient/bootstrap.min.js ')}}"></script>
<script  src="{{asset('doctor_patient/script.js')}}"></script>
<script id="rendered-js">
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["jun", "jun", "jun", "jun", "jun", "jun", "jun", "jun", "jun", "jun"],
            datasets: [{
                label: 'Booking',
                data: [40, 85, 3, 17, 28, 24, 7, 15, 15, 15],
                backgroundColor: "#7DC580"
            }, {
                label: 'Apoinment',
                data: [50, 85, 65, 35, 28, 53, 30, 45, 39, 35],
                backgroundColor: "#40AAF4"
            }, {
                label: 'No-Show',
                data: [69, 36, 56, 25, 29, 46, 37, 85, 55, 65],
                backgroundColor: "#117CBA"
            }, {
                label: 'Apoinment Slots',
                data: [39, 58, 55, 75, 50, 33, 49, 35, 45, 59],
                backgroundColor: "#CDCCCC"
            }, {

                label: 'Cancel',
                fill: false,
                data: [20, 85, 5, 5, 20, 3, 10, 15, 15, 15],
                backgroundColor: "#FF0000"
            }]
        }

    });
    //# sourceURL=pen.js
</script>

