@extends('layouts.default')
@push('css')
@endpush
@section('content')

    <link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
    <div class="x-card p-4 h-100">

         @if(session()->has('success_message'))
         <div class="row">
             <div class="col-md-12">
                 <div class="alert alert-success">
                     {{session()->get('success_message')}}
                 </div>
             </div>
         </div>
     @endif
     
        <table id="ajax_source_example" class="table table-striped table-borderless dt-responsive nowrap w-100">
            <thead class="text-dark">
            <tr>
                <th data-i18n="table.Sr#">Sr#</th>
                <th data-i18n="table.Title">Title</th>
                <th data-i18n="table.Slug">Slug</th>
                <th data-i18n="table.aktion">Action</th>
            </tr>
            </thead>
            <tbody>

                @foreach($pages as $page)
                <tr>
                  <td>{{$page->id}}</td>
                  <td>{{$page->page_title}}</td>
                  <td><a href="#">{{$page->page_slug}}</a></td>
                  <td>
                        @if($page->id == 1)
                        @else
                        <a href="" style="padding:6px;" class="btn btn-primary btn-sm btn-pill text-white delete-user">Delete</a>
                        <a href="" style="padding:6px;" class="btn btn-success btn-sm btn-pill text-white dz-details">Detail</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
           <!--  <tr>
               <th data-i18n="table.Sr#">Sr#</th>
                <th data-i18n="table.Title">Title</th>
                <th data-i18n="table.Slug">Slug</th>
                <th data-i18n="table.aktion">Action</th>
            </tr> -->
            </tfoot>
        </table>
    </div>

@endsection
@push('js')

    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>

@endpush
