@extends('layouts.default')
@push('css')
@endpush
@section('content')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
<link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
 <div class="x-card p-4 h-100">


            <table id="ajax_source_example" class="table table-striped table-borderless dt-responsive nowrap w-100">
                <thead class="text-dark">
                    <tr>
                        <th data-i18n="table.Sr#">Sr#</th>
                        <th data-i18n="table.Customer">Customer</th>
                        <th data-i18n="table.Total">Total</th>
                        <th data-i18n="table.Invoice Date">Invoice Date</th>
                        <th data-i18n="table.Status">Status</th>
                        <th data-i18n="table.Action">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="{{url('super-admin/invoice-detail/1')}}"> INV123 </a></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><a href="#" class="btn btn-primary btn-sm btn-pill text-white delete-user">Dekete</a></td>
                    </tr>
                </tbody>
                <tfoot>
                   <!--  <tr>
                      <th data-i18n="table.Sr#">Sr#</th>
                        <th data-i18n="table.Customer">Customer</th>
                        <th data-i18n="table.Total">Total</th>
                        <th data-i18n="table.Invoice Date">Invoice Date</th>
                        <th data-i18n="table.Status">Status</th>
                        <th data-i18n="table.Action">Action</th>

                    </tr> -->
                </tfoot>
            </table>
        </div>

@endsection
@push('js')

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>

  @endpush
