@extends('layouts.default')
@push('css')
    <link rel="stylesheet" href="{{asset('assets/css/questionnaire.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('jqTree/jqtree.css')}}" media="all">
    <style>
        .customSR > .col-md-1{
            display: flex;
        }
    </style>
@endpush
@section('content')
    <div class="x-card p-4 h-100">

        @if(session()->has('message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('message')}}
                    </div>
                </div>
            </div>
        @endif
        <div class="container-fluid" id="customSRR">
            <div class="row customSR">
                <div class="col-md-1">
                    <button class="btn btn-outline-primary" onclick="openBoxMenu('a',0)">New</button>
                    <div class="box-menu" id="box-menu" style="display: none;">
                        <a href="javascript:;" onclick="openModal('a')" id="folderOutSideQuestionnaire"><i class="fa fa-file" aria-hidden="true"></i>
                             New Questionnaire</a>
                        <a href="javascript:;" id="newFolder" onclick="openModal('b')">
                            <i class="fa fa-folder" aria-hidden="true"></i>
                            New Folder</a>
                    </div>
                </div>
                <div class="col-md-11">
                    <input type="search" name="search" onchange="searchData()" class="form-control" placeholder="Search..." />
                </div>
            </div>
            <br>
            <br>
            <!-- Folder Listing -->
            <div class="row">
                <div class="col-md-12">
                    <h1 class="foldersH">Folders</h1>
                </div>
            </div>
            <div class="row" id="folder-view">
                @foreach($questionnaire as $questionnaires)
                    <div class="col-md-3" style="margin-bottom:15px;">
                        <div class="folder-box">
                            <div class="row">
                                <div class="col-md-10" onclick="goToQuestionnaireList('{{$questionnaires->id}}')">
                                    <span class="inside-box">
                                        <i class="fa fa-folder" aria-hidden="true"></i>
                                        {{$questionnaires->folder_name}}
                                    </span>
                                </div>
                                <div class="col-md-2 text-center" onclick="openMenuList(3,'{{$questionnaires->id}}')">
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    <div class="boxList" id="boxFolder-{{$questionnaires->id}}" style="display:none;">
                                        <ul class="customUl" style="left:-187px !important;top: 16px !important;">
                                            <li style="text-align: initial !important;" onclick="openFolderModal(1,'{{$questionnaires->folder_name}}','{{$questionnaires->id}}')">
                                                <i class="fa fa-pencil-square-o"></i>
                                                Rename Folder
                                            </li>
                                            <li style="text-align: initial !important;" onclick="openFolderModal(2,'{{$questionnaires->folder_name}}','{{$questionnaires->id}}')">
                                                <i class="fa fa fa-trash"></i>
                                                Delete Folder
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row" id="folderInsideList">
                @foreach($questionnaires_questions as $questions)
                    <div style="margin-bottom: 30px;" class="col-md-3">
                        <div class="box-questionnaire">
                            <div class="date-section">
                                {{\Carbon\Carbon::parse($questions->created_at)->format('M d Y')}}
                            </div>
                            <div class="shareable-section">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                <i class="fa fa-laptop" aria-hidden="true"></i>
                                <i class="fa fa-ellipsis-v" onclick="openMenuList(1,'{{$questions->id}}')" aria-hidden="true"></i>
                            </div>
                            <div class="boxList" id="boxList2-{{$questions->id}}" style="display: none;">
                                <ul class="customUl" style="top:105px !important;">
                                    <li onclick="liSelected('a',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-paper-plane" aria-hidden="true"></i> Send Questionnaire</li>
                                    <li onclick="liSelected('b',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Questionnaire</li>
                                    <li onclick="liSelected('c',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-laptop" aria-hidden="true"></i> Patient Preview</li>
                                    <li onclick="liSelected('d',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-files-o" aria-hidden="true"></i> Duplicate Questionnaire</li>
                                    <li onclick="liSelected('e',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-folder" aria-hidden="true"></i> Move to Folder</li>
                                    <li onclick="liSelected('f',{{$questions->id}})"><i style="cursor:pointer;" class="fa fa-trash" aria-hidden="true"></i> Delete Questionnaire</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- see inside folder -->
        <input type="hidden" id="folderId" />
        <input type="hidden" id="counter_value" />
        @include('superadmin.questionnaire.list')
    </div>

    <!-- Modal -->
    <div id="addFolder" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="customH1">New Folder</h1>
                </div>
                <div class="modal-body">
                    <input type="text" id="folder_name" class="form-control" placeholder="Folder Name" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="saveFolderData()" id="saveFolder" class="btn btn-outline-primary">Confirm</button>
                    <button type="button" onclick="saveFolderInsideFolder()" id="saveFolderInsideNewFolder" style="display: none;" class="btn btn-outline-primary">Confirm</button>
                </div>
            </div>

        </div>
    </div>

    <div id="rename_folder" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="customH1">Rename Folder</h1>
                </div>
                <div class="modal-body">
                    <input type="text" id="folder_name_pre_filled" class="form-control" placeholder="Folder Name" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="renameFolder()" class="btn btn-outline-primary">Save</button>
                </div>
            </div>

        </div>
    </div>

    <div id="delete_folder" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="customH1">Delete Folder</h1>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">Are You Sure You Want To Delete.....!</div>
                    <input type="hidden" id="folder_name_pre_filled_id" />
                    <input type="hidden" id="folder_name_pre_fill_id" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="deleteFolder()" class="btn btn-outline-danger">Delete</button>
                </div>
            </div>

        </div>
    </div>

    <div id="delete_questionnaire" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="customH1">Delete Questionnaire</h1>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">Are You Sure You Want To Delete Questionnaire.....!</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="deleteQuestionnaire()" class="btn btn-outline-danger">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" id="questionnaire_id" />
    <input type="hidden" id="questionnaire_action" />
    <input type="hidden" id="folderName" />
    <input type="hidden" id="folderid" />
    <div id="move_to_folder" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="customH1">Move To Another Folder</h1>
                </div>
                <div class="modal-body">
                    <div id="myTree"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="moveQuestionnaire()" class="btn btn-outline-success">Save</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script src="{{asset('jqTree/tree.jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            treeData();
        });
    </script>
    <script>
        var count = 1;
        function deleteFolder(){
            let folder_id = $('#folder_name_pre_filled_id').val();
            let id = $('#folderId').val();
            let counter_value = $('#counter_value').val();
            $.ajax({
                url:'/super-admin/delete-folder/'+folder_id,
                method:'GET',
                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $('#delete_folder').modal('hide');
                    if(response.status == true) {
                        treeData();
                        toastr.success('Folder Deleted Successfully');
                        treeFolderData(id,counter_value);
                        $('#box-menu').hide();
                        $('#folder-view').html('');
                        $('#folder-view').append('');
                        $('#rename_folder').modal('hide');
                        let responseData = response.data;
                        getLatestFolderRecord(responseData);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    toastr.error(error);
                }
            });
        }
        function renameFolder(){
            let folder_name = $('#folder_name_pre_filled').val();
            let folder_id = $('#folder_name_pre_fill_id').val();
            let id = $('#folderId').val();
            let counter_value = $('#counter_value').val();
            let form = new FormData();
            form.append('_token','{{csrf_token()}}');
            form.append('folder_id',folder_id);
            form.append('folder_name',folder_name);
            $.ajax({
                url:'{{route('renameFolder')}}',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.status == true) {
                        treeData();
                        toastr.success('Folder Renamed Successfully');
                        treeFolderData(id,counter_value);
                        $('#box-menu').hide();
                        $('#folder-view').html('');
                        $('#folder-view').append('');
                        $('#rename_folder').modal('hide');
                        let responseData = response.data;
                        getLatestFolderRecord(responseData);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    toastr.error(error);
                }
            });
        }
        function openFolderModal(val,name,id){
            console.log('val , name , id');
            console.log(val , name , id);
            if(val == 1) {
                $('#rename_folder').modal('show');
                $('#folder_name_pre_filled').val(name);
                $('#folder_name_pre_fill_id').val(id);
            }
            if(val == 2) {
                $('#folder_name_pre_filled_id').val(id);
                $('#delete_folder').modal('show');
            }
        }
        function saveFolderInsideFolder(){
            let folder_name = $('#folder_name').val();
            let folder_id = $('#folderId').val();
            let form = new FormData();
            form.append('_token', '{{csrf_token()}}');
            form.append('folder_name',folder_name);
            form.append('folder_id',folder_id);
            let counter_value = $('#counter_value').val();
            $.ajax({
                url:'{{route('addFolderInsideFolder')}}',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    $('#folder-view-0').html('');
                    $('#folder-view-0').append('');
                    console.log('response');
                    console.log(response);
                    console.log(response.status);
                    if(response.status == true) {
                        $('#box-menu').hide();
                        $('#addFolder').modal('hide');
                        toastr.success('New Folder Created Successfully');
                        let responseData = response.data;
                        treeFolderData(folder_id,counter_value);
                    }
                } ,
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    toastr.success(error);
                }
            });
        }
        function goToQuestionnaireList(id){
            //going to append tree data
            $('#customSR-0').show();
            $('#customSR-0').html('');
            $('#customSR-0').append('');
            let value = count++;
            let html = '<div class="row customSR" id="customSRR-'+value+'">' +
                '        <div class="col-md-1">' +
                '            <i class="fa fa-arrow-left faLeftArrow" onclick="backToFolder('+value+')" id="faLeftArrow-'+value+'" aria-hidden="true"></i>' +
                '            <button class="btn btn-outline-primary" onclick="openBoxMenu(\'b\','+value+')">New</button>' +
                '            <div class="box-menu" id="box-menu-'+value+'" style="display: none;">' +
                '                <a href="javascript:;" style="display: none;" id="folderOInsideQuestionnaire" onclick="openModal(\'c\')"><i class="fa fa-file" aria-hidden="true"></i>' +
                '                    New Questionnaire</a>' +
                '                <a href="javascript:;" id="insideFolderNewFolder" style="display:none;" onclick="openModal(\'d\')">' +
                '                    <i class="fa fa-folder" aria-hidden="true"></i>' +
                '                    New Folder</a>' +
                '            </div>' +
                '        </div>' +
                '        <div class="col-md-11">' +
                '            <input type="search" name="search" onchange="searchData('+value+')" class="form-control" placeholder="Search..." />' +
                '        </div>' +
                '    </div>' +
                '    <br>' +
                '    <br>' +
                '    <!-- Folder Listing -->' +
                '    <div class="row">' +
                '        <div class="col-md-12">' +
                '            <h1 class="foldersH">Folders</h1>' +
                '        </div>' +
                '    </div>' +
                '    <div class="row" style="margin-bottom:12px;' +
                '    border-bottom:1px solid #eee;" id="folder-view-'+value+'-'+id+'">' +
                '    </div>' +
                '    <div class="row" id="insideFolder-'+value+'-'+id+'">' +
                '    </div>';
            $('#counter_value').val(value);
            treeFolderData(id,value);
            $('#customSR-0').append(html);


            $('#insideFolder').show();
            $('#box-menu').hide();
            $('#folder-view').hide();
            $('#folderInsideList').hide();
            $('#faLeftArrow-'+value).show();
            $('#folderOInsideQuestionnaire').show();
            $('#folderOutSideQuestionnaire').hide();
            $('#folderId').val(id);
            $('#insideFolderNewFolder').show();
            $('#newFolder').hide();
            $('#saveFolderInsideNewFolder').show();
            $('#saveFolder').hide();
            $('#customSRR').hide();
            $('#folder_name').val('');
        }
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return month_name(new Date(date))+' '+ day +' '+year;
        }
        function month_name(dt){
            mlist = [ "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];
            return mlist[dt.getMonth()];
        }
        function day_name(dt){
            dlist = [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
            return dlist[dt.getDay()];
        }
        //js dynamic tree structure for creating folders and questionnaire
        function treeFolderData(id,value){
            console.log('going to update tree chid data');
            console.log(id,value);
            console.log('#folder-view-'+value+'-'+id);
            console.log('#insideFolder-'+value+'-'+id);
            console.log('going to update tree chid data');
            $('#folder_name').val('');
            let url = '/super-admin/get-folder-data/'+id;
            $.ajax({
               url:url,
               method:'GET',
               processData: false,
               contentType: false,
               cache: false,
               success: function (response) {
                   console.log('response folder data');
                   console.log(response);
                   $('#folder-view-'+value+'-'+id).html('');
                   $('#folder-view-'+value+'-'+id).append('');
                   $('#insideFolder-'+value+'-'+id).html('');
                   $('#insideFolder-'+value+'-'+id).append('');
                   console.log('response.data');
                   //this code is dynamic using javascript
                   if(response.data.length > 0) {
                       response.data.map((d) => {
                           let html = '<div class="col-md-3" style="margin-bottom:15px;">' +
                               '                        <div class="folder-box">' +
                               '                            <div class="row">' +
                               '                                <div class="col-md-10" onclick="goToQuestionnaireList(' + d.id + ')">' +
                               '                                    <span class="inside-box">' +
                               '                                        <i class="fa fa-folder" aria-hidden="true"></i>' +
                               d.folder_name +
                               '                                    </span>' +
                               '                                </div>' +
                               '                                <div class="col-md-2 text-center" onclick="openMenuList(4,' + d.id + ')">' +
                               '                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>' +
                               '                                    <div class="boxList" id="boxFolder-' + d.id + '" style="display:none;">' +
                               '                                        <ul class="customUl" style="left:-187px;top: 16px;">' +
                               '                                            <li style="text-align: initial !important;" ' +
                               'onclick="openFolderModal(1, \'' +d.folder_name + '\' , ' + d.id + ')">' +
                               '                                                <i class="fa fa-pencil-square-o"></i>' +
                               '                                                Rename Folder' +
                               '                                            </li>' +
                               '                                            <li style="text-align: initial !important;" ' +
                               'onclick="openFolderModal(2, \'' +d.folder_name + '\' , ' + d.id + ')">' +
                               '                                                <i class="fa fa fa-trash"></i>' +
                               '                                                Delete Folder' +
                               '                                            </li>' +
                               '                                        </ul>' +
                               '                                    </div>' +
                               '                                </div>' +
                               '                            </div>' +
                               '          </div>' +
                               '        </div>';
                           $('#folder-view-' + value+'-'+id).show();
                           $('#folder-view-' + value+'-'+id).append(html);
                       });
                   } else {
                       $('#folder-view-' + value+'-'+id).hide();
                   }

                   //this code is dynamic using javascript
                   response.questionnaire_questions.map((d) => {
                      let html2 = '<div class="col-md-3" style="margin-bottom: 30px;">' +
                          '        <div class="box-questionnaire">' +
                          '            <div class="date-section">' +
                          formatDate(d.created_at) +
                          '            </div>' +
                          '            <div class="shareable-section">' +
                          '                <i style="cursor:pointer;" class="fa fa-paper-plane" aria-hidden="true"></i>' +
                          '                <i style="cursor:pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
                          '                <i style="cursor:pointer;" class="fa fa-laptop" aria-hidden="true"></i>' +
                          '                <i style="cursor:pointer;" class="fa fa-ellipsis-v" onclick="openMenuList(6,'+d.id+')" aria-hidden="true"></i>' +
                          '            </div>' +
                          '            <div class="boxList" id="boxListT-'+d.id+'" style="display: none;">' +
                          '                <ul class="customUl">' +
                          '                    <li onclick="liSelected(\'a\','+d.id+')"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Questionnaire</li>' +
                          '                    <li onclick="liSelected(\'b\','+d.id+')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Questionnaire</li>' +
                          '                    <li onclick="liSelected(\'c\','+d.id+')"><i class="fa fa-laptop" aria-hidden="true"></i> Patient Preview</li>' +
                          '                    <li onclick="liSelected(\'d\','+d.id+')"><i class="fa fa-files-o" aria-hidden="true"></i> Duplicate Questionnaire</li>' +
                          '                    <li onclick="liSelected(\'e\','+d.id+')"><i class="fa fa-folder" aria-hidden="true"></i> Move to Folder</li>' +
                          '                    <li onclick="liSelected(\'f\','+d.id+')"><i class="fa fa-trash" aria-hidden="true"></i> Delete Questionnaire</li>' +
                          '                </ul>' +
                          '            </div>' +
                          '        </div>' +
                          '    </div>';
                       $('#insideFolder-'+value+'-'+id).append(html2);
                   });
               },
               error: function (error) {
                   console.log('error');
                   console.log(error);
               }
            });
        }
        function backToFolder(value){
            count--;
            let id = $('#folderId').val();
            console.log('value checking....');
            console.log(id);
            $.ajax({
                url:'/super-admin/get-previous-id/'+id,
                method:'GET',

                success: function (response){
                    console.log('response');
                    console.log(response);
                    if(response.questionnaire.folder_id != null) {
                        console.log(response.questionnaire.folder_id);
                        goToQuestionnaireList(response.questionnaire.folder_id);
                    } else {
                        $('#folder_name').val('');
                        $('#folder-view').show();
                        $('#insideFolder').hide();
                        $('#faLeftArrow-'+value).hide();
                        $('#folderInsideList').show();
                        $('#folderOInsideQuestionnaire').hide();
                        $('#folderOutSideQuestionnaire').show();
                        $('#folderId').val('');
                        $('#insideFolderNewFolder').hide();
                        $('#newFolder').show();
                        $('#saveFolderInsideNewFolder').hide();
                        $('#saveFolder').show();
                        $('#box-menu').hide();
                        $('#customSRR').show();
                        $('#customSR-0').hide();
                    }
                },
                error: function (error){
                    console.log('error');
                    console.log(error);
                }
            });
        }
        function openBoxMenu(val,num){
            if(val == 'b') {
                $('#box-menu-'+num).toggle();
            } else {
                $('#box-menu').toggle();
            }
        }
        //js code for detect outside body
        $(document).mouseup(function(e) {
            var container = $("#box-menu-0");
            var container2 = $(".box-menu");
            var container3 = $(".boxList");
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.hide();
            }
            if (!container2.is(e.target) && container2.has(e.target).length === 0)
            {
                container2.hide();
            }
            if (!container3.is(e.target) && container3.has(e.target).length === 0)
            {
                container3.hide();
            }
        });
        function openModal(val){
            if(val == 'a') {
                window.location.href='/super-admin/add-questionnaire/'+'{{\Auth::user()->id}}';
            } else if(val == 'b') {
                console.log('value b');
                $('#folder_name').val('');
                $('#addFolder').modal('show');
                $('#box-menu').hide();
                $('.box-menu').hide();
            } else if(val == 'd') {
                $('#addFolder').modal('show');
                $('#box-menu-0').hide();
                $('#box-menu').hide();
                $('.box-menu').hide();
            } else {
                let id = $('#folderId').val();
                window.location.href='/super-admin/add-folder-questionnaire/'+'{{\Auth::user()->id}}'+'/'+id;
            }
        }
        function searchData(){
            console.log('working...');
            $('#box-menu').hide();
        }
        function saveFolderData(){
            let folder_name = $('#folder_name').val();
            let form = new FormData();
            form.append('_token', '{{csrf_token()}}');
            form.append('folder_name',folder_name);
            $.ajax({
                url:'{{route('addFolder')}}',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    $('#folder-view').html('');
                    $('#folder-view').append('');
                   console.log('response');
                   console.log(response);
                   console.log(response.status);
                   if(response.status == true) {
                       treeData();
                       $('#box-menu').hide();
                       $('#addFolder').modal('hide');
                       toastr.success('New Folder Created Successfully');
                       let responseData = response.data;
                       responseData.map((data) => {
                           console.log('data');
                           console.log(data);
                           let html = '<div class="col-md-3" style="margin-bottom:15px;">' +
                               '                        <div class="folder-box">' +
                               '                            <div class="row">' +
                               '                                <div class="col-md-10" onclick="goToQuestionnaireList('+data.id+')">' +
                               '                                    <span class="inside-box">' +
                               '                                        <i class="fa fa-folder" aria-hidden="true"></i>' +
                               data.folder_name +
                               '                                    </span>' +
                               '                                </div>' +
                               '                                <div class="col-md-2 text-center" onclick="openMenuList(3,'+data.id+')">' +
                               '                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>' +
                               '                                    <div class="boxList" id="boxFolder-'+data.id+'" style="display:none;">' +
                               '                                        <ul class="customUl" style="left:-187px;top: 16px;">' +
                               '                                            <li style="text-align: initial !important;" ' +
                               'onclick="openFolderModal(1, \'' +data.folder_name + '\' , ' + data.id + ')">' +
                               '                                                <i class="fa fa-pencil-square-o"></i>' +
                               '                                                Rename Folder' +
                               '                                            </li>' +
                               '                                            <li style="text-align: initial !important;" ' +
                               'onclick="openFolderModal(2, \'' +data.folder_name + '\' , ' + data.id + ')">' +
                               '                                                <i class="fa fa fa-trash"></i>' +
                               '                                                Delete Folder' +
                               '                                            </li>' +
                               '                                        </ul>' +
                               '                                    </div>' +
                               '                                </div>' +
                               '                            </div>' +
                               '                        </div>' +
                               '                    </div>';
                           $('#folder-view').append(html);
                       });
                   }
                } ,
                error: function (error) {
                   console.log('error');
                   console.log(error);
                   toastr.error(error);
                }
            });
        }
        function openMenuList(val,Dv){
            if(val == 1) {
                $('#boxList2-'+Dv).toggle();
            } else if(val == 2) {
                $('#boxList-'+Dv).toggle();
            } else if(val == 3) {
                $('#boxFolder-'+Dv).toggle();
            } else if(val == 6) {
                $('#boxListT-'+Dv).toggle();
            } else {
                $('#boxFolder-'+Dv).toggle();
            }
        }
        function detectDiv(e,selector) {
            var container = selector;
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.hide();
            }
        }
        function liSelected(val,id){
            let action;
            if(val == 'd') {
                duplicateQuestionnaire(id);
                return ;
            } else if(val == 'f') {
                action = 'delete';
            } else if(val == 'e') {
                action = 'move_folder';
            } else if(val == 'c') {
                action = 'review';
            } else if(val == 'b') {
                action = 'edit';
            } else if(val == 'a') {
                action = 'send';
            }
            actionPerformQuestionnaire(id,action)
        }
        function actionPerformQuestionnaire(id,action){
            $('#questionnaire_id').val(id);
            $('#questionnaire_action').val(action);
            $('.boxList').hide();
            if(action == 'delete') {
                $('#delete_questionnaire').modal('show');
            } else if(action == 'move_folder') {
                $('#move_to_folder').modal('show');
            } else if(action == 'review'){
                alert(id+'  '+action + 'going to Patient review');
            } else if(action == 'edit'){
                window.location.href='/super-admin/edit-questionnaire/'+id;
            } else if(action == 'send'){
                alert(id+'  '+action);
            }
        }
        function duplicateQuestionnaire(id){
            let form = new FormData();
            form.append('_token','{{csrf_token()}}');
            form.append('id',id);
            $.ajax({
                url:'{{route('duplicateQuestionnaire')}}',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $('#folderInsideList').html('');
                    $('#folderInsideList').append('');
                    if(response.status == true) {
                        toastr.success('Questionnaire Duplicated Successfully');
                        getLatestQuestionnaireRecord(response.questionnaires_questions);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    toastr.error(error);
                }
            });
        }
        function deleteQuestionnaire(){
            let id = $('#questionnaire_id').val();
            let action = $('#questionnaire_action').val();
            sendActionQuestionnaireRequest(id,action);
        }
        function moveQuestionnaire(){
            let id = $('#questionnaire_id').val();
            let action = $('#questionnaire_action').val();
            sendActionQuestionnaireRequest(id,action);
        }
        function sendActionQuestionnaireRequest(id,action){
            let form = new FormData();
            form.append('_token','{{csrf_token()}}');
            form.append('id',id);
            form.append('action',action);
            if(action == 'move_folder') {
                let folder_name = $('#folderName').val();
                let folder_id = $('#folderId').val();
                form.append('folder_name',folder_name);
                form.append('folder_id',folder_id);
            }
            $.ajax({
                url:'{{route('actionPerformQuestionnaire')}}',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.status == true) {
                        if(response.action == 'delete') {
                            toastr.success(response.message);
                            getLatestQuestionnaireRecord(response.data);
                            $('#delete_questionnaire').modal('hide');
                        }
                        if(response.action == 'move_folder') {
                            toastr.success(response.message);
                            getLatestQuestionnaireRecord(response.data);
                            $('#move_to_folder').modal('hide');
                        }
                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                    toastr.error(error);
                }
            })
        }
        //get questionnaire latest record after perform any action against questionnaire
        function getLatestQuestionnaireRecord(data){
            treeData();
            let id_d = $('#folderId').val();
            let counter_value = $('#counter_value').val();
            treeFolderData(id_d,counter_value);
            $('#folderInsideList').html('');
            $('#folderInsideList').append('');
            data.map((d) => {
                let html2 = '<div class="col-md-3" style="margin-bottom: 30px;">' +
                    '        <div class="box-questionnaire">' +
                    '            <div class="date-section">' +
                    formatDate(d.created_at) +
                    '            </div>' +
                    '            <div class="shareable-section">' +
                    '                <i style="cursor:pointer;" class="fa fa-paper-plane" aria-hidden="true"></i>' +
                    '                <i style="cursor:pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
                    '                <i style="cursor:pointer;" class="fa fa-laptop" aria-hidden="true"></i>' +
                    '                <i style="cursor:pointer;" class="fa fa-ellipsis-v" onclick="openMenuList(2,' + d.id + ')" aria-hidden="true"></i>' +
                    '            </div>' +
                    '            <div class="boxList" id="boxList-' + d.id + '" style="display: none;">' +
                    '                <ul class="customUl" >' +
                    '                    <li onclick="liSelected(\'a\',' + d.id + ')"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Questionnaire</li>' +
                    '                    <li onclick="liSelected(\'b\',' + d.id + ')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Questionnaire</li>' +
                    '                    <li onclick="liSelected(\'c\',' + d.id + ')"><i class="fa fa-laptop" aria-hidden="true"></i> Patient Preview</li>' +
                    '                    <li onclick="liSelected(\'d\',' + d.id + ')"><i class="fa fa-files-o" aria-hidden="true"></i> Duplicate Questionnaire</li>' +
                    '                    <li onclick="liSelected(\'e\',' + d.id + ')"><i class="fa fa-folder" aria-hidden="true"></i> Move to Folder</li>' +
                    '                    <li onclick="liSelected(\'f\',' + d.id + ')"><i class="fa fa-trash" aria-hidden="true"></i> Delete Questionnaire</li>' +
                    '                </ul>' +
                    '            </div>' +
                    '        </div>' +
                    '    </div>';
                $('#folderInsideList').append(html2);
            })
        }
        function getLatestFolderRecord(data){
            treeData();
            let id_d = $('#folderId').val();
            let counter_value = $('#counter_value').val();
            treeFolderData(id_d,counter_value);
            $('#box-menu').hide();
            $('#folder-view').html('');
            $('#folder-view').append('');
            data.map((data) => {
                console.log('data');
                console.log(data);
                let html = '<div class="col-md-3" style="margin-bottom:15px;">' +
                    '                        <div class="folder-box">' +
                    '                            <div class="row">' +
                    '                                <div class="col-md-10" onclick="goToQuestionnaireList(' + data.id + ')">' +
                    '                                    <span class="inside-box">' +
                    '                                        <i class="fa fa-folder" aria-hidden="true"></i>' +
                    data.folder_name +
                    '                                    </span>' +
                    '                                </div>' +
                    '                                <div class="col-md-2 text-center" onclick="openMenuList(3,' + data.id + ')">' +
                    '                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>' +
                    '                                    <div class="boxList" id="boxFolder-' + data.id + '" style="display:none;">' +
                    '                                        <ul class="customUl" style="left:-187px;top: 16px;">' +
                    '                                            <li style="text-align: initial !important;" ' +
                    'onclick="openFolderModal(1, \'' +data.folder_name + '\' , ' + data.id + ')">' +
                    '                                                <i class="fa fa-pencil-square-o"></i>' +
                    '                                                Rename Folder' +
                    '                                            </li>' +
                    '                                            <li style="text-align: initial !important;" ' +
                    'onclick="openFolderModal(2, \'' +data.folder_name + '\' , ' + data.id + ')">' +
                    '                                                <i class="fa fa fa-trash"></i>' +
                    '                                                Delete Folder' +
                    '                                            </li>' +
                    '                                        </ul>' +
                    '                                    </div>' +
                    '                                </div>' +
                    '                            </div>' +
                    '                        </div>' +
                    '                    </div>';
                $('#folder-view').append(html);
            });
        }
        function treeData(){
            var $tree = $('#myTree');
            $.get( "{{route('treeData')}}", function( data ) {
                console.log('tree updated');
                console.log(data);
                $tree.tree('loadData', data);
            });
            $tree.tree({
                dataUrl: '{{route('treeData')}}'
            });
            $tree.bind(
                'tree.select',
                function(event) {
                    var node = event.node;
                    console.log(node);
                    $('#folderId').val(node.id);
                    $('#folderName').val(node.folder_name);
                }
            );
        }
    </script>
@endpush
