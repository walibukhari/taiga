<div class="box-questionnaire-form" id="questionnaireForm">
    <div class="row" id="ifNoQuestionnaire" style="display:none;">
        <div class="col-md-12">
            <p class="customPP">This form is empty, Add an element below!</p>
        </div>
        <div class="text-center">
                <div class="elements-box-2" id="elements-box-2-0" style="display: none;">
                    <ul class="customUlLi">
                        <li onclick="clickElements('a','0')"><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>
                        <li onclick="clickElements('b','0')"><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>
                        <li onclick="clickElements('c','0')"><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>
                        <li onclick="clickElements('d','0')"><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>
                        <li onclick="clickElements('e','0')"><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>
                        <li onclick="clickElements('f','0')"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>
                    </ul>
                </div>
            <button type="button" class="customBtnAddElement btn btn-outline-info" onclick="openDropBox('bb',0)">Add Element</button>
        </div>
    </div>
    <div id="inner-form" class="inner-form-0">
        <div class="btnArrowLeft" id="arrowLeft">
            <button class="mainBtn btn btn-outline-dark customArrowLeftBtn" type="button" onclick="openDropBox('a',0)"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
            <div class="elements-box" id="elements-box" style="display: none;">
                <ul class="customUlLi">
                    <li><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>
                    <li><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>
                    <li><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>
                    <li><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>
                    <li><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>
                    <li><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>
                </ul>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <select class="customInput form-control" onchange="changeOptions(this,0);">
                        <option value="0">Single Line Text</option>
                        <option value="1">Multi Line Text</option>
                        <option value="2">radio</option>
                        <option value="3">scoring</option>
                        <option value="4">send communication</option>
                        <option value="5">referral network</option>
                        <option value="6">checkbox</option>
                        <option value="7">select</option>
                        <option value="8">grid</option>
                        <option value="9">physical grid</option>
                        <option value="10">yes / no</option>
                        <option value="11">multi yes / no</option>
                        <option value="12">priority</option>
                        <option value="13">number</option>
                        <option value="14">date</option>
                        <option value="15">time</option>
                        <option value="16">year month day</option>
                        <option value="17">date range</option>
                        <option value="18">email</option>
                        <option value="19">range</option>
                        <option value="20">link</option>
                        <option value="21">five star</option>
                        <option value="22">drawing</option>
                        <option value="23">diagram</option>
                        <option value="24">signature</option>
                        <option value="25">address. location</option>
                        <option value="26">phone</option>
                        <option value="27">photograph</option>
                        <option value="28">video</option>
                        <option value="29">doctors</option>
                        <option value="30">blood pressure</option>
                        <option value="31">medical conditions</option>
                        <option value="32">height & weight</option>
                        <option value="33">medication allergies</option>
                        <option value="34">surgical history</option>
                        <option value="35">medication</option>
                        <option value="36">autocomplete</option>
                        <option value="37">pdf</option>
                        <option value="38">questionnaire</option>
                        <option value="39">paragraph</option>
                        <option value="40">collect payment</option>
                    </select>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Question Title*</label>
                    <input type="text" placeholder="Question title*" class="customInput form-control" />
                </div>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div id="singleLine-0">
                @include('superadmin.questionnaire.include.single_line')
            </div>
            <div id="multiLine-0" style="display: none;">
                @include('superadmin.questionnaire.include.multi_line')
            </div>
            <div id="radioSection-0" style="display: none;">
                @include('superadmin.questionnaire.include.radio')
            </div>
            <div id="scoringSection-0" style="display: none;">
                @include('superadmin.questionnaire.include.scoring')
            </div>
            <div id="sendCommunication-0" style="display: none;">
                @include('superadmin.questionnaire.include.send_communication')
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Note Section</label>
                    <select class="customInput form-control">
                        <option>None</option>
                        <option>Triage</option>
                        <option>Patient Profile</option>
                        <option>Employment History</option>
                        <option>Chief Complaint</option>
                        <option>HPI</option>
                        <option>Medications</option>
                        <option>Medication Allergies</option>
                        <option>Medical Conditions</option>
                        <option>Review of Systems</option>
                        <option>Lab Tests</option>
                        <option>Diagnostic Imaging</option>
                        <option>Mental Health History</option>
                        <option>Sexual Health History</option>
                        <option>Past Medical History</option>
                        <option>Past Psychiatric History</option>
                        <option>Past Surgical History</option>
                        <option>Obstetric History</option>
                        <option>Family Medical History</option>
                        <option>Family Psychiatric History</option>
                        <option>Social History / Risk Factors</option>
                        <option>Past Personal History</option>
                        <option>Preventive Health/Screening Activities</option>
                        <option>Vaccination History</option>
                        <option>Mental Status Examination</option>
                        <option>Physical Exam</option>
                        <option>Impression</option>
                        <option>Assessment</option>
                        <option>Recommendations</option>
                        <option>Plan</option>
                        <option>Assessment & Plan</option>
                        <option>Miscellaneous</option>
                        <option>Scoring Tests</option>
                        <option>%%-Header</option>
                        <option>%%-Footer</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Integration ID</label>
                    <input type="text" class="customInput form-control" placeholder="Integration ID" />
                </div>
            </div>
        </div>
        <br>
        <div class="text-center">
            <button type="button" onclick="copyPasteBox()" class="mainBtn btn-outline-primary"><i class="fa fa-copy" style="font-size:24px"></i>
            </button>
            <button type="button" class="mainBtn btn-outline-danger" onclick="deleteBox(0)"><i class="fa fa-trash" style="font-size:24px"></i>
            </button>
        </div>
        <br>
        <br>
        <div class="text-center">
                <div class="elements-box-2" id="elements-box-2" style="display: none;">
                    <ul class="customUlLi">
                        <li onclick="clickElements('a','0')"><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>
                        <li onclick="clickElements('b','0')"><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>
                        <li onclick="clickElements('c','0')"><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>
                        <li onclick="clickElements('d','0')"><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>
                        <li onclick="clickElements('e','0')"><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>
                        <li onclick="clickElements('f','0')"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>
                    </ul>
                </div>
            <button type="button" class="customBtnAddElement btn btn-outline-info" onclick="openDropBox('b',0)">Add Element</button>
        </div>
    </div>
</div>
