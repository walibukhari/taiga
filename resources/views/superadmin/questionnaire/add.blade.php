@extends('layouts.default')
@push('css')
    <style>
        .customUlAdd{
            display: flex;
            border-bottom: 1px solid #efefef;
            padding-left: 0px;
        }
        .customUlAdd > li {
            cursor: pointer;
            list-style: none;
            padding:12px 30px;
            color: rgba(0, 0, 0, 0.87);
        }
        .customUlAdd > li:active{
            background-color: rgba(205, 231, 252, 0.8);
        }
        .activeLi{
            background-color: rgba(205, 231, 252, 0.8) !important;
            border-bottom: 2px solid #289FDF !important;
        }
        .tab-view-area{

        }
        #customRow{
            display: flex;
            justify-content: flex-end;
        }
        .alert-success {
            color: #08522e;
            background-color: #cfebde;
            border-color: #bce4d0;
            width: 100% !important;
        }
        #inner-form {
            border: 1px solid #efefef;
            padding: 40px;
        }
        .btnArrowLeft{
            text-align: right;
            z-index: 9999;
        }
        .mainBtn{
            cursor: pointer;
            background-color: #fff;
            padding: 10px 13px;
            color: #000;
            border-radius: 51px;
            border: 1px solid #c0c0c0;
            margin-right: 10px;
            margin-left: 10px;
        }
        .customInput:focus{
            outline: transparent !important;
            border-color: transparent !important;
        }
        .customInput:active{
            outline: transparent !important;
            border-color: transparent !important;
        }
        .mainBtn:focus{
            outline: transparent !important;
        }
        .customInput{
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }
        .elements-box{
            background-color: #fff;
            position: absolute;
            right: 20px;
            z-index: 9999;
        }
        .elements-box-2{
            position: absolute;
            margin-top: -264px;
            left: 44%;
            background-color: #fff;
        }
        .width-100{
            text-align: center;
            position: absolute;
            bottom: 110px;
            width: 12%;
            margin: 0 auto;
            left: 44%;
        }
        .customUlLi{
            background-color: #fff;
            border: 1px solid #efefef;
            width: 166px;
        }
        .customUlLi {
            list-style:none;
            text-align:left;
            padding-left:0px;
            padding: 0px 16px;
        }
        .customUlLi > li {
            cursor: pointer;
            color: #000;
            padding-top:10px;
            padding-bottom:10px;
        }
        .customUlLi li > .fa {
            margin-right: 10px;
        }
        .customBtnAddElement{
            border-radius: 12px;
        }
        .customPP{
            font-size: 30px;
            color: #000;
            font-weight: 400;
            line-height: 32px;
            margin-top: 24px;
            margin-bottom: 16px;
            font-weight: bold;
            text-align: center;
        }
        #ifNoQuestionnaire {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .customArrowLeftBtn {
            position: absolute;
            margin-top: -50px;
        }
        .btnDark{
            margin-right: 15px;
            margin-left: 15px;
        }
        .timesMd2{
            box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
            background: #fff;
            border: 2px solid #efefef;
            border-radius: 100px;
            cursor: pointer;
            width: 40px;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 40px;
        }
        .customRowbtns{
            text-align: center;
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .customBox{
            display: flex;
            align-items: center;
            margin-top: 12px;
        }
        .ck.ck-editor__main>.ck-editor__editable:not(.ck-focused) {
            border-color: var(--ck-color-base-border);
            height: 200px;
        }
        .customH2{
            font-size: 15px;
            font-weight: bold;
            color: #000;
        }
    </style>
@endpush
@section('content')
    <div class="x-card p-4 h-100">

        @if(session()->has('message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('message')}}
                    </div>
                </div>
            </div>
        @endif
        <div class="container-fluid" style="padding:0px;">
            @if(session()->has('success_message'))
            <div class="row">
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            </div>
            @endif
            <form action="{{route('postAddQuestionnaire')}}" method="POST">
                @csrf
                <input type="hidden" name="folder_id" value="{{@$folder_id}}" />
                <input type="hidden" name="user_id" value="{{@$id}}" />
                <div class="row" id="customRow">
                    <button type="button" class="btn btn-outline-primary" style="position:relative;right:12px;">Patient View</button>
                    <button type="submit" class="btn btn-outline-info">Save</button>
                </div>
                <div class="tab-view-area">
                    <ul class="customUlAdd">
                        <li onclick="changeActiveClass('a')" class="liCustomA activeLi">Builder</li>
                        <li onclick="changeActiveClass('b')" class="liCustomB">PDFs</li>
                        <li onclick="changeActiveClass('c')" class="liCustomC">Personalization</li>
                    </ul>
                </div>

                <div class="tab-content-area">
                    <div id="tabBuilder" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Questionnaire Name*</label>
                                    <input type="text" name="questionnaire_name" class="form-control" placeholder="Enter Questionnaire Name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Questionnaire Tags*</label>
                                    <input type="text" name="questionnaire_tag" class="form-control" placeholder="Add Questionnaire Name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>Page 1 of 1</p>
                                </div>
                                <div class="col-md-3">
                                    <p>
                                        <input type="checkbox" name="allow_patient"  />
                                        Allow Patient Printing
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>
                                        <input type="checkbox" name="feedback"  />
                                        Feedback
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>
                                        <input type="checkbox" name="save_questionnaire" />
                                        Save Questionnaire Data
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /questionnaire form/ -->
                            @include('superadmin.questionnaire.questionnaire_form')
                        <!-- /questionnaire form/ -->
                    </div>
                    <div id="tabPDFs" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>PDF ID</label>
                                    <input type="text" name="pdf_id" class="form-control" placeholder="Enter PDF ID" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Default PDF Email</label>
                                    <input type="text" name="pdf_email" class="form-control" placeholder="Enter Default PDF Email" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Default PDF Fax Number</label>
                                    <input type="text" name="pdf_fax_number" class="form-control" placeholder="Enter Default PDF Fax Number" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabPersonalization" style="display: none;">
                        <h1>On Way</h1>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('js')
    <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <script>

        function openDropBox(val,id){
            if(val == 'a') {
                $('#elements-box').toggle();
            }
            if(val == 'b') {
                $('#elements-box-2').toggle();
            }
            if(val == 'bb') {
                $('#elements-box-2-'+id).toggle();
            }
            if(val == 'c') {
                $('#elements-box-2-'+id).toggle();
            }
            if(val == 'cc') {
                $('#elementsArrowBox-'+id).toggle();
            }
            if(val == 'd') {
                $('#elements-box-3-'+id).toggle();
            }
        }
        var counter = 1;
        function copyPasteBox(){
            let value = counter++;
            let html = '<div id="inner-form" class="inner-form-'+value+'" style="margin-top:20px;">' +
                '        <div class="btnArrowLeft" id="arrowLeft-'+value+'">' +
                '            <button class="mainBtn btn btn-outline-dark customArrowLeftBtn" type="button" onclick="openDropBox(\'cc\','+value+')"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>' +
                '            <div class="elements-box" id="elementsArrowBox-'+value+'" style="display: none;">' +
                '                <ul class="customUlLi">' +
                '                    <li onclick="clickElements(\'a\')" ><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>' +
                '                    <li onclick="clickElements(\'b\')" ><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>' +
                '                    <li onclick="clickElements(\'c\')" ><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>' +
                '                    <li onclick="clickElements(\'d\')" ><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>' +
                '                    <li onclick="clickElements(\'e\')" ><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>' +
                '                    <li onclick="clickElements(\'f\')" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>' +
                '                </ul>' +
                '            </div>' +
                '        </div>' +
                '        <div class="form-group">' +
                '            <div class="row">' +
                '                <div class="col-md-12">' +
                '                    <select class="customInput form-control" onchange="changeOptions(this,'+value+');">' +
                '                        <option value="0">Single Line Text</option>' +
                '                        <option value="1">Multi Line Text</option>' +
                '                        <option value="2">radio</option>' +
                '                        <option value="3">scoring</option>' +
                '                        <option value="4">send communication</option>' +
                '                        <option value="5">referral network</option>' +
                '                        <option value="6">checkbox</option>' +
                '                        <option value="7">select</option>' +
                '                        <option value="8">grid</option>' +
                '                        <option value="9">physical grid</option>' +
                '                        <option value="10">yes / no</option>' +
                '                        <option value="11">multi yes / no</option>' +
                '                        <option value="12">priority</option>' +
                '                        <option value="13">number</option>' +
                '                        <option value="14">date</option>' +
                '                        <option value="15">time</option>' +
                '                        <option value="16">year month day</option>' +
                '                        <option value="17">date range</option>' +
                '                        <option value="18">email</option>' +
                '                        <option value="19">range</option>' +
                '                        <option value="20">link</option>' +
                '                        <option value="21">five star</option>' +
                '                        <option value="22">drawing</option>' +
                '                        <option value="23">diagram</option>' +
                '                        <option value="24">signature</option>' +
                '                        <option value="25">address. location</option>' +
                '                        <option value="26">phone</option>' +
                '                        <option value="27">photograph</option>' +
                '                        <option value="28">video</option>' +
                '                        <option value="29">doctors</option>' +
                '                        <option value="30">blood pressure</option>' +
                '                        <option value="31">medical conditions</option>' +
                '                        <option value="32">height & weight</option>' +
                '                        <option value="33">medication allergies</option>' +
                '                        <option value="34">surgical history</option>' +
                '                        <option value="35">medication</option>' +
                '                        <option value="36">autocomplete</option>' +
                '                        <option value="37">pdf</option>' +
                '                        <option value="38">questionnaire</option>' +
                '                        <option value="39">paragraph</option>' +
                '                        <option value="40">collect payment</option>' +
                '                    </select>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '        <br>' +
                '        <div class="form-group">' +
                '            <div class="row">' +
                '                <div class="col-md-12">' +
                '                    <label>Question Title*</label>' +
                '                    <input type="text" placeholder="Question title*" class="customInput form-control" />' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '        <br>' +
                '        <div class="form-group">' +
                '<div id="singleLine-'+value+'"><div class="row">' +
                '    <div class="col-md-12">' +
                '        <input type="text" readonly placeholder="Single Line Text" style="border-bottom:dashed;width:90%;margin: 0 auto;" class="customInput form-control" />' +
                '    </div>' +
                '</div></div>'+
                '<div id="multiLine-'+value+'" style="display: none;"><div class="row">' +
                '    <div class="col-md-12">' +
                '        <textarea readonly placeholder="Multi Line Text" style="border-bottom:dashed;width:90%;margin: 0 auto;" class="customInput form-control"></textarea>' +
                '    </div>' +
                '</div></div>'+
                '<div id="radioSection-'+value+'" style="display: none;">' +
                    '<div class="row" id="radioSectionAdd-'+value+'" style="align-items:center;">' +
                    '    <div class="row" id="row-radio-'+value+'-0" style="align-items:center;width:100%;">' +
                    '        <div class="col-md-11">' +
                    '            <input type="text" class="form-control" placeholder="" />' +
                    '        </div>' +
                    '        <div class="col-md-1">' +
                    '            <div class="timesMd2" onclick="deleteRadioSectionOtherOption2('+value+',0)" id="times-0">' +
                    '                <i class="fa fa-times"></i>' +
                    '            </div>' +
                    '        </div>' +
                    '    </div>' +
                    '</div>' +
                    '<div class="row" id="otherRadioSection-'+value+'" style="display:none;align-items:center;width:100%;margin-top:30px;">' +
                    '    <div class="col-md-11">' +
                    '        <input type="text" name="radio_other" id="radio_other-'+value+'" placeholder="Other:" class="form-control" />' +
                    '    </div>' +
                    '    <div class="col-md-1">' +
                    '        <div class="timesMd2" onclick="deleteSectionAddOther2('+value+')" id="times-0">' +
                    '            <i class="fa fa-times"></i>' +
                    '        </div>' +
                    '    </div>' +
                    '</div>' +
                    '<div class="row customRowbtns">' +
                    '    <button type="button" class="btn btn-outline-dark btnDark" onclick="radioSectionOtherOption2('+value+')">Add Option</button>' +
                    '    <button type="button" class="btn btn-outline-dark btnDark" id="radioSectionAddOtherbtn-'+value+'" onclick="radioSectionAddOthe2('+value+')">Add Other</button>' +
                    '</div>'+
                '</div>'+
                '<div id="scoringSection-'+value+'" style="display: none;"><div class="row" id="rowScoring-'+value+'">' +
                        '<div class="row" id="row-scoring-'+value+'-0" style="align-items:center;width: 100%;">' +
                        '<div class="col-md-8">' +
                        '     <label>.</label>' +
                        '   <input type="text" class="form-control" name="scoring_text[]" placeholder="" />' +
                        '</div>' +
                        '<div class="col-md-3">' +
                        '    <label>Score*</label>' +
                        '    <input type="number" class="form-control" name="scoring_number[]" placeholder="Score*" />' +
                        '</div>' +
                        '        <div class="col-md-1">' +
                        '           <div class="timesMd2" style="position:relative;top: 12px;" onclick="deleteSocringRow2('+value+',0)" id="times-0">' +
                        '         <i class="fa fa-times"></i>' +
                        '      </div>' +
                        '  </div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="row customRowbtns">' +
                        '    <button type="button" class="btn btn-outline-dark btnDark" onclick="addMoreScoring2('+value+')">Add Option</button>' +
                        '</div>' +
                '</div>'+
                '        </div>' +
                '        <br>' +
                '        <div class="form-group">' +
                '            <div class="row">' +
                '                <div class="col-md-6">' +
                '                    <label>Note Section</label>' +
                '                    <select class="customInput form-control">' +
                '                        <option>None</option>' +
                '                        <option>Triage</option>' +
                '                        <option>Patient Profile</option>' +
                '                        <option>Employment History</option>' +
                '                        <option>Chief Complaint</option>' +
                '                        <option>HPI</option>' +
                '                        <option>Medications</option>' +
                '                        <option>Medication Allergies</option>' +
                '                        <option>Medical Conditions</option>' +
                '                        <option>Review of Systems</option>' +
                '                        <option>Lab Tests</option>' +
                '                        <option>Diagnostic Imaging</option>' +
                '                        <option>Mental Health History</option>' +
                '                        <option>Sexual Health History</option>' +
                '                        <option>Past Medical History</option>' +
                '                        <option>Past Psychiatric History</option>' +
                '                        <option>Past Surgical History</option>' +
                '                        <option>Obstetric History</option>' +
                '                        <option>Family Medical History</option>' +
                '                        <option>Family Psychiatric History</option>' +
                '                        <option>Social History / Risk Factors</option>' +
                '                        <option>Past Personal History</option>' +
                '                        <option>Preventive Health/Screening Activities</option>' +
                '                        <option>Vaccination History</option>' +
                '                        <option>Mental Status Examination</option>' +
                '                        <option>Physical Exam</option>' +
                '                        <option>Impression</option>' +
                '                        <option>Assessment</option>' +
                '                        <option>Recommendations</option>' +
                '                        <option>Plan</option>' +
                '                        <option>Assessment & Plan</option>' +
                '                        <option>Miscellaneous</option>' +
                '                        <option>Scoring Tests</option>' +
                '                        <option>%%-Header</option>' +
                '                        <option>%%-Footer</option>' +
                '                    </select>' +
                '                </div>' +
                '                <div class="col-md-6">' +
                '                    <label>Integration ID</label>' +
                '                    <input type="text" class="customInput form-control" placeholder="Integration ID" />' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '        <br>' +
                '        <div class="text-center">' +
                '            <button type="button" onclick="copyPasteBox()" class="mainBtn btn-outline-primary"><i class="fa fa-copy" style="font-size:24px"></i>' +
                '            </button>' +
                '            <button type="button" class="mainBtn btn-outline-danger" onclick="deleteBox('+value+')"><i class="fa fa-trash" style="font-size:24px"></i>' +
                '            </button>' +
                '        </div>' +
                '        <br>' +
                '        <br>' +
                '        <div class="text-center">' +
            '                <div class="elements-box-2" id="elements-box-2-'+value+'" style="display: none;">' +
            '                    <ul class="customUlLi">' +
            '                    <li onclick="clickElements(\'a\','+value+')" ><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>' +
            '                    <li onclick="clickElements(\'b\','+value+')" ><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>' +
            '                    <li onclick="clickElements(\'c\','+value+')" ><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>' +
            '                    <li onclick="clickElements(\'d\','+value+')" ><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>' +
            '                    <li onclick="clickElements(\'e\','+value+')" ><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>' +
            '                    <li onclick="clickElements(\'f\','+value+')" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>' +
            '                    </ul>' +
            '                </div>' +
                '            <button type="button" class="customBtnAddElement btn btn-outline-info" onclick="openDropBox(\'c\','+value+')">Add Element</button>' +
                '        </div>' +
                '    </div>';
            $('#questionnaireForm').append(html);
        }
        $(document).mouseup(function(e) {
            var container = $(".elements-box");
            var container2 = $(".elements-box-2");
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.hide();
            }
            if (!container2.is(e.target) && container2.has(e.target).length === 0)
            {
                container2.hide();
            }
        });
        var countElement = 1;
        function clickElements(val) {
            $('#ifNoQuestionnaire').hide();
            if(val == 'e') {
                let value = countElement++;
                let html = '<div id="inner-form" class="inner-form-'+value+'" style="margin-top:20px;">' +
                    '        <div class="btnArrowLeft" id="arrowLeft-'+value+'">' +
                    '            <button class="mainBtn btn btn-outline-dark customArrowLeftBtn" type="button" onclick="openDropBox(\'c\','+value+')"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>' +
                    '            <div class="elements-box" id="elementsArrowBox-'+value+'" style="display: none;">' +
                    '                <ul class="customUlLi">' +
                    '                    <li onclick="clickElements(\'a\','+value+')" ><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>' +
                    '                    <li onclick="clickElements(\'b\','+value+')" ><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>' +
                    '                    <li onclick="clickElements(\'c\','+value+')" ><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>' +
                    '                    <li onclick="clickElements(\'d\','+value+')" ><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>' +
                    '                    <li onclick="clickElements(\'e\','+value+')" ><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>' +
                    '                    <li onclick="clickElements(\'f\','+value+')" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>' +
                    '                </ul>' +
                    '            </div>' +
                    '        </div>' +
                    '        <div class="form-group">' +
                    '            <div class="row">' +
                    '                <div class="col-md-12">' +
                    '                    <select class="customInput form-control">' +
                    '                        <option>Single Line Text</option>' +
                    '                        <option>Multi Line Text</option>' +
                    '                        <option>radio</option>' +
                    '                        <option>scoring</option>' +
                    '                        <option>send communication</option>' +
                    '                        <option>referral network</option>' +
                    '                        <option>checkbox</option>' +
                    '                        <option>select</option>' +
                    '                        <option>grid</option>' +
                    '                        <option>physical grid</option>' +
                    '                        <option>yes / no</option>' +
                    '                        <option>multi yes / no</option>' +
                    '                        <option>priority</option>' +
                    '                        <option>number</option>' +
                    '                        <option>date</option>' +
                    '                        <option>time</option>' +
                    '                        <option>year month day</option>' +
                    '                        <option>date range</option>' +
                    '                        <option>email</option>' +
                    '                        <option>range</option>' +
                    '                        <option>link</option>' +
                    '                        <option>five star</option>' +
                    '                        <option>drawing</option>' +
                    '                        <option>diagram</option>' +
                    '                        <option>signature</option>' +
                    '                        <option>address. location</option>' +
                    '                        <option>phone</option>' +
                    '                        <option>photograph</option>' +
                    '                        <option>video</option>' +
                    '                        <option>doctors</option>' +
                    '                        <option>blood pressure</option>' +
                    '                        <option>medical conditions</option>' +
                    '                        <option>height & weight</option>' +
                    '                        <option>medication allergies</option>' +
                    '                        <option>surgical history</option>' +
                    '                        <option>medication</option>' +
                    '                        <option>autocomplete</option>' +
                    '                        <option>pdf</option>' +
                    '                        <option>questionnaire</option>' +
                    '                        <option>paragraph</option>' +
                    '                        <option>collect payment</option>' +
                    '                    </select>' +
                    '                </div>' +
                    '            </div>' +
                    '        </div>' +
                    '        <br>' +
                    '        <div class="form-group">' +
                    '            <div class="row">' +
                    '                <div class="col-md-12">' +
                    '                    <label>Question Title*</label>' +
                    '                    <input type="text" placeholder="Question title*" class="customInput form-control" />' +
                    '                </div>' +
                    '            </div>' +
                    '        </div>' +
                    '        <br>' +
                    '        <div class="form-group">' +
                    '            <div class="row">' +
                    '                <div class="col-md-12">' +
                    '                    <input type="text" readonly placeholder="Single Line Text" style="border-bottom:dashed;width:90%;margin: 0 auto;" class="customInput form-control" />' +
                    '                </div>' +
                    '            </div>' +
                    '        </div>' +
                    '        <br>' +
                    '        <div class="form-group">' +
                    '            <div class="row">' +
                    '                <div class="col-md-6">' +
                    '                    <label>Note Section</label>' +
                    '                    <select class="customInput form-control">' +
                    '                        <option>None</option>' +
                    '                        <option>Triage</option>' +
                    '                        <option>Patient Profile</option>' +
                    '                        <option>Employment History</option>' +
                    '                        <option>Chief Complaint</option>' +
                    '                        <option>HPI</option>' +
                    '                        <option>Medications</option>' +
                    '                        <option>Medication Allergies</option>' +
                    '                        <option>Medical Conditions</option>' +
                    '                        <option>Review of Systems</option>' +
                    '                        <option>Lab Tests</option>' +
                    '                        <option>Diagnostic Imaging</option>' +
                    '                        <option>Mental Health History</option>' +
                    '                        <option>Sexual Health History</option>' +
                    '                        <option>Past Medical History</option>' +
                    '                        <option>Past Psychiatric History</option>' +
                    '                        <option>Past Surgical History</option>' +
                    '                        <option>Obstetric History</option>' +
                    '                        <option>Family Medical History</option>' +
                    '                        <option>Family Psychiatric History</option>' +
                    '                        <option>Social History / Risk Factors</option>' +
                    '                        <option>Past Personal History</option>' +
                    '                        <option>Preventive Health/Screening Activities</option>' +
                    '                        <option>Vaccination History</option>' +
                    '                        <option>Mental Status Examination</option>' +
                    '                        <option>Physical Exam</option>' +
                    '                        <option>Impression</option>' +
                    '                        <option>Assessment</option>' +
                    '                        <option>Recommendations</option>' +
                    '                        <option>Plan</option>' +
                    '                        <option>Assessment & Plan</option>' +
                    '                        <option>Miscellaneous</option>' +
                    '                        <option>Scoring Tests</option>' +
                    '                        <option>%%-Header</option>' +
                    '                        <option>%%-Footer</option>' +
                    '                    </select>' +
                    '                </div>' +
                    '                <div class="col-md-6">' +
                    '                    <label>Integration ID</label>' +
                    '                    <input type="text" class="customInput form-control" placeholder="Integration ID" />' +
                    '                </div>' +
                    '            </div>' +
                    '        </div>' +
                    '        <br>' +
                    '        <div class="text-center">' +
                    '            <button type="button" onclick="copyPasteBox()" class="mainBtn btn-outline-primary"><i class="fa fa-copy" style="font-size:24px"></i>' +
                    '            </button>' +
                    '            <button type="button" class="mainBtn btn-outline-danger" onclick="deleteBox('+value+')"><i class="fa fa-trash" style="font-size:24px"></i>' +
                    '            </button>' +
                    '        </div>' +
                    '        <br>' +
                    '        <br>' +
                    '        <div class="text-center">' +
                    '                <div class="elements-box-2" id="elements-box-3-'+value+'" style="display: none;">' +
                    '                    <ul class="customUlLi">' +
                    '                    <li onclick="clickElements(\'a\','+value+')" ><i class="fa fa-question-circle" aria-hidden="true"></i>Question</li>' +
                    '                    <li onclick="clickElements(\'b\','+value+')" ><i class="fa fa-paragraph" aria-hidden="true"></i>Paragraph</li>' +
                    '                    <li onclick="clickElements(\'c\','+value+')" ><i class="fa fa-picture-o" aria-hidden="true"></i>Image</li>' +
                    '                    <li onclick="clickElements(\'d\','+value+')" ><i class="fa fa-video-camera" aria-hidden="true"></i>Video</li>' +
                    '                    <li onclick="clickElements(\'e\','+value+')" ><i class="fa fa-files-o" aria-hidden="true"></i>Questionnaire</li>' +
                    '                    <li onclick="clickElements(\'f\','+value+')" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</li>' +
                    '                    </ul>' +
                    '                </div>' +
                    '            <button type="button" class="customBtnAddElement btn btn-outline-info" onclick="openDropBox(\'d\','+value+')">Add Element</button>' +
                    '        </div>' +
                    '    </div>';
                $('.elements-box').hide();
                $('.elements-box-2').hide();
                $('#questionnaireForm').append(html);
            }
        }
        function deleteBox(id){
            if(id == 0) {
                $('#ifNoQuestionnaire').show();
            } else {
                $('#ifNoQuestionnaire').hide();
            }
            $('.inner-form-'+id).remove();
        }

        //onChange function start
        function changeOptions(sel,id){
            if(sel.value == 0) {
                changeFormData('single_line_text',id);
            } else if(sel.value == 1) {
                changeFormData('multi_line_text',id);
            } else if(sel.value == 2) {
                changeFormData('radio',id);
            } else if(sel.value == 3) {
                changeFormData('scoring',id);
            } else if(sel.value == 4) {
                changeFormData('send_communication',id);
            } else if(sel.value == 5) {
                changeFormData('referral_network',id);
            } else if(sel.value == 6) {
                changeFormData('checkbox',id);
            } else if(sel.value == 7) {
                changeFormData('select',id);
            } else if(sel.value == 8) {
                changeFormData('grid',id);
            } else if(sel.value == 9) {
                changeFormData('physical_grid',id);
            }
        }

        function changeFormData(val,id){
            if(val == 'single_line_text') {
                $('#singleLine-'+id).show();
                $('#multiLine-'+id).hide();
                $('#radioSection-'+id).hide();
                $('#scoringSection-'+id).hide();
                $('#sendCommunication-'+id).hide();
            } else if(val == 'multi_line_text') {
                $('#singleLine-'+id).hide();
                $('#multiLine-'+id).show();
                $('#radioSection-'+id).hide();
                $('#scoringSection-'+id).hide();
                $('#sendCommunication-'+id).hide();
            } else if(val == 'radio') {
                $('#sendCommunication-'+id).hide();
                $('#singleLine-'+id).hide();
                $('#multiLine-'+id).hide();
                $('#radioSection-'+id).show();
                $('#scoringSection-'+id).hide();
            } else if(val == 'scoring') {
                $('#sendCommunication-'+id).hide();
                $('#singleLine-'+id).hide();
                $('#multiLine-'+id).hide();
                $('#radioSection-'+id).hide();
                $('#scoringSection-'+id).show();
            } else if(val == 'send_communication') {
                $('#singleLine-'+id).hide();
                $('#multiLine-'+id).hide();
                $('#radioSection-'+id).hide();
                $('#scoringSection-'+id).hide();
                $('#sendCommunication-'+id).show();
            } else if(val == 'referral_network') {

            } else if(val == 'checkbox') {

            } else if(val == 'select') {

            } else if(val == 'grid') {

            } else if(val == 'physical_grid') {

            }
        }
        //onChange function end

        //scoring option
        var scoring_count = 0;
        function addMoreScoring(){
            let value = scoring_count++;
            let html = '<div class="row" id="row-scoring-'+value+'" style="align-items:center;width:100%;">' +
                '        <div class="col-md-8">' +
                '            <label>.</label>' +
                '            <input type="text" class="form-control" name="scoring_text[]" placeholder="" />' +
                '        </div>' +
                '        <div class="col-md-3">' +
                '            <label>Score*</label>' +
                '            <input type="number" class="form-control" name="scoring_number[]" placeholder="Score*" />' +
                '        </div>' +
                '        <div class="col-md-1">' +
                '            <div class="timesMd2" style="position:relative;top:12px;" onclick="deleteSocringRow('+value+')" id="times-0">' +
                '                <i class="fa fa-times"></i>' +
                '            </div>' +
                '        </div>' +
                '    </div>';
            $('#rowScoring').append(html);
        }
        var scoring_counter2 = 1;
        function addMoreScoring2(val){
            let value = scoring_counter2++;
            let html = '<div class="row" id="row-scoring-'+val+'-'+value+'" style="align-items:center;width:100%;">' +
                '        <div class="col-md-8">' +
                '            <label>.</label>' +
                '            <input type="text" class="form-control" name="scoring_text[]" placeholder="" />' +
                '        </div>' +
                '        <div class="col-md-3">' +
                '            <label>Score*</label>' +
                '            <input type="number" class="form-control" name="scoring_number[]" placeholder="Score*" />' +
                '        </div>' +
                '        <div class="col-md-1">' +
                '            <div class="timesMd2" style="position:relative;top:12px;" onclick="deleteSocringRow2('+val+','+value+')" id="times-0">' +
                '                <i class="fa fa-times"></i>' +
                '            </div>' +
                '        </div>' +
                '    </div>';
            $('#rowScoring-'+val).append(html);
        }
        //delete scoring data
        function deleteSocringRow(id){
            $('#row-scoring-'+id).remove();
        }
        function deleteSocringRow2(val,value){
            $('#row-scoring-'+val+'-'+value).remove();
        }
        //scoring section end

        //radio section add other option
        function radioSectionAddOther(val){
            $('#radioSectionAddOtherbtn-'+val).hide();
            $('#otherRadioSection-'+val).show();
        }
        function radioSectionAddOthe2(val){
            $('#radioSectionAddOtherbtn-'+val).hide();
            $('#otherRadioSection-'+val).show();
        }
        function deleteSectionAddOther(val){
            $('#radioSectionAddOtherbtn-'+val).show();
            $('#otherRadioSection-'+val).hide();
            $('#radio_other-'+val).val('');
        }
        function deleteSectionAddOther2(val){
            $('#radioSectionAddOtherbtn-'+val).show();
            $('#otherRadioSection-'+val).hide();
            $('#radio_other-'+val).val('');
        }
        var radioCounter = 0;
        function radioSectionOtherOption(){
            let value = radioCounter++;
            let html = '<div class="row" id="row-radio-'+value+'" style="align-items:center;width:100%;margin-top:20px;">' +
                '        <div class="col-md-11">' +
                '            <input type="text" class="form-control" placeholder="" />' +
                '        </div>' +
                '        <div class="col-md-1">' +
                '            <div class="timesMd2" onclick="deleteRadioSectionOtherOption('+value+')" id="times-0">' +
                '                <i class="fa fa-times"></i>' +
                '            </div>' +
                '        </div>' +
                '    </div>';
            $('#radioSectionAdd').append(html);

        }
        var radioCounter2 = 1;
        function radioSectionOtherOption2(val){
            let value = radioCounter2++;
            let html = '<div class="row" id="row-radio-'+val+'-'+value+'" style="align-items:center;width:100%;margin-top:20px;">' +
                '        <div class="col-md-11">' +
                '            <input type="text" class="form-control" placeholder="" />' +
                '        </div>' +
                '        <div class="col-md-1">' +
                '            <div class="timesMd2" onclick="deleteRadioSectionOtherOption2('+val+','+value+')" id="times-0">' +
                '                <i class="fa fa-times"></i>' +
                '            </div>' +
                '        </div>' +
                '    </div>';
            $('#radioSectionAdd-'+val).append(html);
        }
        function deleteRadioSectionOtherOption(id){
            $('#row-radio-'+id).remove();
        }
        function deleteRadioSectionOtherOption2(val,value){
            $('#row-radio-'+val+'-'+value).remove();
        }
        //end radio and nested radio section

        //send communication section start
        function selectEmailRadio(val){
            if(val == 'a') {
                $('#email_template-0').prop('checked',true);
                $('#custom_email-0').prop('checked',false);
                $('#use_custom_email').show();
                $('#custom_email_editor').hide();
                $('#emailSubjectsTemplateId').show();
            } else {
                $('#email_template-0').prop('checked',false);
                $('#custom_email-0').prop('checked',true);
                $('#use_custom_email').show();
                $('#custom_email_editor').show();
                $('#emailSubjectsTemplateId').hide();
            }
        }

        function emailOptions(val){
            if(val == 'a') {
                $('#custom_email_opt').prop('checked',true);
                $('#custom_patient_opt').prop('checked',false);
                $('#custom_clinic_opt').prop('checked',false);
                $('#emailTo').show();
            }
            if(val == 'b') {
                $('#custom_email_opt').prop('checked',false);
                $('#custom_patient_opt').prop('checked',true);
                $('#custom_clinic_opt').prop('checked',false);
                $('#emailTo').hide();
            }
            if(val == 'c') {
                $('#custom_email_opt').prop('checked',false);
                $('#custom_patient_opt').prop('checked',false);
                $('#custom_clinic_opt').prop('checked',true);
                $('#emailTo').hide();
            }
        }
    </script>
    <script>
        $(document).ready(function () {
           changeActiveClass('a');
        });
        function changeActiveClass(val){
            if(val == 'a') {
                $('#tabBuilder').show();
                $('#tabPDFs').hide();
                $('#tabPersonalization').hide();
                $('.liCustomA').addClass('activeLi');
                $('.liCustomB').removeClass('activeLi');
                $('.liCustomC').removeClass('activeLi');
            }
            if(val == 'b') {
                $('#tabBuilder').hide();
                $('#tabPDFs').show();
                $('#tabPersonalization').hide();
                $('.liCustomA').removeClass('activeLi');
                $('.liCustomB').addClass('activeLi');
                $('.liCustomC').removeClass('activeLi');
            }
            if(val == 'c') {
                $('#tabBuilder').hide();
                $('#tabPDFs').hide();
                $('#tabPersonalization').show();
                $('.liCustomA').removeClass('activeLi');
                $('.liCustomB').removeClass('activeLi');
                $('.liCustomC').addClass('activeLi');
            }
        }
    </script>
@endpush
