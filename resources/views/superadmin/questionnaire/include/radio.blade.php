<div class="row" id="radioSectionAdd" style="align-items:center;">
    <div class="row" id="row-radio-0" style="align-items:center;width:100%;">
        <div class="col-md-11">
            <input type="text" class="form-control" placeholder="" />
        </div>
        <div class="col-md-1">
            <div class="timesMd2" onclick="deleteRadioSectionOtherOption(0)" id="times-0">
                <i class="fa fa-times"></i>
            </div>
        </div>
    </div>
</div>
<div class="row" id="otherRadioSection-0" style="display:none;align-items:center;width:100%;margin-top:30px;">
    <div class="col-md-11">
        <input type="text" name="radio_other-0" id="radio_other" placeholder="Other:" class="form-control" />
    </div>
    <div class="col-md-1">
        <div class="timesMd2" onclick="deleteSectionAddOther(0)" id="times-0">
            <i class="fa fa-times"></i>
        </div>
    </div>
</div>
<div class="row customRowbtns">
    <button type="button" class="btn btn-outline-dark btnDark" onclick="radioSectionOtherOption(0)">Add Option</button>
    <button type="button" class="btn btn-outline-dark btnDark" id="radioSectionAddOtherbtn-0" onclick="radioSectionAddOther(0)">Add Other</button>
</div>
