<div class="row" id="rowScoring">
    <div class="row" id="row-send_communication-0" style="align-items:center;width: 100%;">
        <div class="col-md-8">
            <h2 class="customH2">Email Type:</h2>
            <div class="customBox">
                <input type="radio" style="width:30px;" id="email_template-0" onclick="selectEmailRadio('a')" name="email_template[]" class="form-control" />
                <span>Use Template Email</span>
                <input type="radio" style="width:30px;" id="custom_email-0" onclick="selectEmailRadio('b')" name="custom_email[]" class="form-control" />
                <span>Write Custom Email</span>
            </div>
        </div>
    </div>
    <div class="row" id="use_custom_email" style="display:none;">
        <div class="col-md-12" style="margin-top:30px;">
            <h2 class="customH2">Email Options:</h2>
            <div class="customBox">
                <input type="radio" style="width:30px;" id="custom_email_opt" onclick="emailOptions('a')" name="email_template[]" class="form-control" />
                <span>Use Custom Email</span>
                <input type="radio" style="width:30px;" id="custom_patient_opt" onclick="emailOptions('b')" name="custom_email[]" class="form-control" />
                <span>Use Patient's Email</span>
                <input type="radio" style="width:30px;" id="custom_clinic_opt" onclick="emailOptions('b')" name="custom_email[]" class="form-control" />
                <span>Use Clinic Email</span>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:30px;">
            <h2 class="customH2">Other Options:</h2>
            <div class="customBox">
                <input type="checkbox" style="width:30px;" id="send_questionnaire_checkbox" onclick="sendQuestionnaire('a')" name="email_template[]" class="form-control" />
                <span>Send Questionnaire Data</span>
            </div>
        </div>
        <br>
        <br>
        <div class="col-md-12">
            <div class="customBox" id="emailTo" style="display:none;flex-direction: column;align-items: flex-start;">
                <h2 class="customH2">Email To:</h2>
                <input type="text" name="subject" class="form-control" placeholder="Email to Send the Questionnaire Results" />
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <div class="customBox" style="flex-direction: column;align-items: flex-start;">
                <h2 class="customH2">Email Subject:</h2>
                <input type="text" name="subject" class="form-control" placeholder="Email Subject" />
            </div>
        </div>
        <br>
        <div class="col-md-12" id="emailSubjectsTemplateId" style="margin-top: 30px;">
            <br>
            <div class="customBox" id="emailTemplateId" style="flex-direction: column;align-items: flex-start;">
                <h2 class="customH2">Template ID:</h2>
                <input type="text" name="template_id" class="form-control" placeholder="Template ID" />
            </div>
        </div>
        <div class="col-md-12" id="custom_email_editor" style=" display:none;margin-top:30px;">
            <h2 class="customH2">Email Body:</h2>
            <div class="customBox">
                <textarea id="editor"></textarea>
            </div>
        </div>
        <div class="col-md-12" id="template_substitution" style="margin-top: 30px;border-bottom: 1px solid #efefef;padding-bottom: 30px;">
            <h2 class="customH2">Template Substitutions:</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="box-template-1">
                        <p>Template Key</p>
                        <div class="box-body">
                            <textarea class="template_key form-control" name="template_key"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-template-1">
                        <p>Value</p>
                        <div class="box-body">
                            <textarea class="value form-control" name="value"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
