<div class="row" id="rowScoring">
    <div class="row" id="row-scoring-0" style="align-items:center;width: 100%;">
        <div class="col-md-8">
            <label>.</label>
            <input type="text" class="form-control" name="scoring_text[]" placeholder="" />
        </div>
        <div class="col-md-3">
            <label>Score*</label>
            <input type="number" class="form-control" name="scoring_number[]" placeholder="Score*" />
        </div>
        <div class="col-md-1">
            <div class="timesMd2" style="position:relative;top: 12px;" onclick="deleteSocringRow(0)" id="times-0">
                <i class="fa fa-times"></i>
            </div>
        </div>
    </div>
</div>
<div class="row customRowbtns">
    <button type="button" class="btn btn-outline-dark btnDark" onclick="addMoreScoring()">Add Option</button>
</div>
