@extends('layouts.default')
@push('css')
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
    <link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
    <style>
        .customUlAdd{
            display: flex;
            border-bottom: 1px solid #efefef;
            padding-left: 0px;
        }
        .customUlAdd > li {
            cursor: pointer;
            list-style: none;
            padding:12px 30px;
            color: rgba(0, 0, 0, 0.87);
        }
        .customUlAdd > li:active{
            background-color: rgba(205, 231, 252, 0.8);
        }
        .activeLi{
            background-color: rgba(205, 231, 252, 0.8) !important;
            border-bottom: 2px solid #289FDF !important;
        }
        .tab-view-area{

        }
        #customRow{
            display: flex;
            justify-content: flex-end;
        }
        .alert-success {
            color: #08522e;
            background-color: #cfebde;
            border-color: #bce4d0;
            width: 100% !important;
        }
    </style>
@endpush
@section('content')
    <div class="x-card p-4 h-100">

        @if(session()->has('message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('message')}}
                    </div>
                </div>
            </div>
        @endif
        <div class="container-fluid" style="padding:0px;">
            @if(session()->has('success_message'))
            <div class="row">
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            </div>
            @endif
            <form action="{{route('updateQuestionnaire',[$data->id])}}" method="POST">
                @csrf
                <input type="hidden" name="folder_id" value="{{@$folder_id}}" />
                <input type="hidden" name="user_id" value="{{@$id}}" />
                <div class="row" id="customRow">
                    <button type="button" class="btn btn-outline-primary" style="position:relative;right:12px;">Patient View</button>
                    <button type="submit" class="btn btn-outline-info">Update</button>
                </div>
                <div class="tab-view-area">
                    <ul class="customUlAdd">
                        <li onclick="changeActiveClass('a')" class="liCustomA activeLi">Builder</li>
                        <li onclick="changeActiveClass('b')" class="liCustomB">PDFs</li>
                        <li onclick="changeActiveClass('c')" class="liCustomC">Personalization</li>
                    </ul>
                </div>

                <div class="tab-content-area">
                    <div id="tabBuilder" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Questionnaire Name*</label>
                                    <input type="text" value="{{$data->name}}"  name="questionnaire_name" class="form-control" placeholder="Enter Questionnaire Name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Questionnaire Tags*</label>
                                    <input type="text" value="{{$data->tags}}"  name="questionnaire_tag" class="form-control" placeholder="Add Questionnaire Name" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabPDFs" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>PDF ID</label>
                                    <input type="text" value="{{$data->pdf_id}}"  name="pdf_id" class="form-control" placeholder="Enter PDF ID" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Default PDF Email</label>
                                    <input type="text" value="{{$data->pdf_email}}"  name="pdf_email" class="form-control" placeholder="Enter Default PDF Email" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Default PDF Fax Number</label>
                                    <input type="text" value="{{$data->pdf_fax_number}}"  name="pdf_fax_number" class="form-control" placeholder="Enter Default PDF Fax Number" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabPersonalization" style="display: none;">
                        <h1>On Way</h1>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('js')
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>
    <script>
        $(document).ready(function () {
           changeActiveClass('a');
        });
        function changeActiveClass(val){
            if(val == 'a') {
                $('#tabBuilder').show();
                $('#tabPDFs').hide();
                $('#tabPersonalization').hide();
                $('.liCustomA').addClass('activeLi');
                $('.liCustomB').removeClass('activeLi');
                $('.liCustomC').removeClass('activeLi');
            }
            if(val == 'b') {
                $('#tabBuilder').hide();
                $('#tabPDFs').show();
                $('#tabPersonalization').hide();
                $('.liCustomA').removeClass('activeLi');
                $('.liCustomB').addClass('activeLi');
                $('.liCustomC').removeClass('activeLi');
            }
            if(val == 'c') {
                $('#tabBuilder').hide();
                $('#tabPDFs').hide();
                $('#tabPersonalization').show();
                $('.liCustomA').removeClass('activeLi');
                $('.liCustomB').removeClass('activeLi');
                $('.liCustomC').addClass('activeLi');
            }
        }
    </script>
@endpush
