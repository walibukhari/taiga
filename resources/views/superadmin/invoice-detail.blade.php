@extends('layouts.default')
@push('css')
@endpush
@section('content')

   <div class="x-card">
      <div class="x-card-body text-dark container-sm font-weight-light my-4">
        <div class="row mb-5">
          <div class="col-md-6 text-center text-md-left">
            <h1>
              <a href="index.html">
                <img src="{{url('/assets')}}/img/logo-desktop-light.png" alt="Brand logo">
              </a>
            </h1>
          </div>
          <div class="col-md-3 ml-auto text-center text-md-right">
            <dl class="row">
              <dt class="col-lg-4">Invoice ID:</dt>
              <dd class="col-lg-8">#00001</dd>

              <dt class="col-lg-4">Created</dt>
              <dd class="col-lg-8">04/12/2018 04:24</dd>

              <dt class="col-lg-4">Due Date</dt>
              <dd class="col-lg-8">05/13/2018 08:00</dd>

              <dt class="col-lg-4 text-truncate">Status</dt>
              <dd class="col-lg-8 text-danger font-weight-bold">UNPAID</dd>
            </dl>
          </div>
        </div>
        <div class="mb-5">
          <div class="row">
            <div class="col-md-6 text-center text-md-left">
              <h6>Melvin Admin</h6>
              <p>
                3100 Williams Avenue
                <br> Los Angeles
                <br> 90017 California
                <br> Phone: 661-308-9659
                <br> Mobile: 562-416-9437
                <br>
              </p>
            </div>
            <div class="col-md-6 text-center text-md-right">
              <h6>Perfekt Client</h6>
              <p>
                615 Juniper Drive
                <br> Saginaw, Michigan(MI), 48601
                <br> Usa
                <br> Phone: 989-816-4622
                <br> Mobile: 989-610-4177
                <br>
              </p>
            </div>
          </div>
        </div>
        <!-- / end client details section -->
        <div class="table-responsive mb-4">
          <table class="table">
            <thead class="text-dark"
              <tr>
                <th class="h4">
                  Product
                </th>
                <th class="h4">
                  Description
                </th>
                <th class="text-right h4">
                  Quantity
                </th>
                <th class="text-right h4">
                  Price
                </th>
                <th class="text-right h4">
                  Sub Total
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Article</td>
                <td>Mid length sweater dress</td>
                <td class="text-right">1</td>
                <td class="text-right">$25.00</td>
                <td class="text-right">$25.00</td>
              </tr>
              <tr>
                <td>Template Design</td>
                <td>Short sleeve sequin dress</td>
                <td class="text-right">2</td>
                <td class="text-right">$20.00</td>
                <td class="text-right">$40.00</td>
              </tr>
              <tr>
                <td>Article</td>
                <td>Large length sweater dress</td>
                <td class="text-right">1</td>
                <td class="text-right">$25.00</td>
                <td class="text-right">$25.00</td>
              </tr>
              <tr>
                <td>Development</td>
                <td>Sleeveless long dress</td>
                <td class="text-right">3</td>
                <td class="text-right">60.00</td>
                <td class="text-right">$180.00</td>
              </tr>
              <tr>
                <td>Article</td>
                <td>Small length sweater dress</td>
                <td class="text-right">1</td>
                <td class="text-right">$25.00</td>
                <td class="text-right">$25.00</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="row text-center text-md-right mb-5">
          <div class="col-6 col-md-2 offset-md-8">
            <p>
              <strong>
                Sub Total :
                <br> TAX :
                <br> Total :
                <br>
              </strong>
            </p>
          </div>
          <div class="col-6 col-md-2">
            <strong>
              $295.00
              <br> $24.50
              <br> $319.50
              <br>
            </strong>
          </div>
        </div>
        <div class="row text-center text-md-right mb-5">
          <div class="col-md-4 offset-md-8">
            <div class="text-xs-center">
              <p>Authorized person</p>
              <!-- <img src="img/signature.png" alt="signature" class="img-fluid" /> -->
              <h6>John Doe</h6>
              <p class="text-muted">Managing Director</p>
            </div>
          </div>
        </div>
        <div class="row row-eq-height">
          <div class="col-md-6 text-center text-md-left">
            <div class="h-100 mb-4">
              <h4 class="mb-4">Bank details</h4>
              <p>Melvin Admin</p>
              <p>Bank National</p>
              <p>SWIFT : CCBOTVPPGKZR</p>
              <p>Account Number : 2589479536458 NY</p>
              <p>IBAN : FR76 2487 4796 0034 5423 9887 654</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="d-flex flex-column h-100 mb-4 text-center text-md-right">
              <h4 class="mb-4">Contact Details</h4>
              <p>Email : you@example.com </p>
              <p>Mobile : 08 24 18 95 74 </p>
              <p>Twitter :
                <a href="https://twitter.com/themacart">@themacart</a>
              </p>
              <h4 class="mt-auto">Payment should be made by Bank Transfer</h4>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
@push('js')
    <script>
  
    </script>
@endpush
