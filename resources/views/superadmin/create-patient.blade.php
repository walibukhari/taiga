@extends('layouts.default')
@push('css')
    <style>
        #ui-datepicker-div{
            background: #fff;
            padding: 20px;
            border: 2px solid #eee;
        }
    </style>
@endpush
@section('content')

    @if(session()->has('success_message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            </div>
        </div>
    @endif
    @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="row">
      <div class="col-lg-12">
        <!-- BEGIN NEW ITEM -->
        <div class="x-card">
          <div class="x-card-header">
            <div class="x-card-title h4">Create Patient</div>
            <a class="heading-elements-toggle">
              <i class="ion size-18 ion-ios-more"></i>
            </a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li class="list-inline-item">
                  <a data-action="collapse">
                    <i class="ion ion-minus-round"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="close">
                    <i class="ion ion-android-close"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="reload">
                    <i class="ion ion-android-refresh"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="expand">
                    <i class="ion ion-android-expand"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="x-card-body collapse show">
            <div class="text-dark">
               <form method="POST" action="{{route('postPatients')}}">
                   @csrf
                  <div class="row">
                    <div class="col-xl-12">



                         <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">First Name</label>
                                <input type="text" name="first_name" class="form-control" required="" id="exampleFormControlInput1" placeholder="First Name">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Middle Name</label>
                                <input type="text" name="middle_name" class="form-control" required="" id="exampleFormControlInput1" placeholder="Middle Name">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Last Name</label>
                                <input type="text" name="last_name" class="form-control" required="" id="exampleFormControlInput1" placeholder="Last Name">
                              </div>
                            </div>
                       </div>


                       <div class="row">
                         <div class="col-xl-12">
                           <label>Birthday*</label>

                         </div>
                       </div>
                       <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Year</label>
                                <input name="year_of_birth" type="text" class="form-control" required="" id="exampleFormControlInput1" placeholder="Year*">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Month</label>
                                <input name="month_of_birth" type="text" class="form-control" required="" id="exampleFormControlInput1" placeholder="Month">
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Day</label>
                                <input type="text" name="day_of_birth" class="form-control" required="" id="exampleFormControlInput1" placeholder="Day">
                              </div>
                            </div>
                       </div>


                        <div class="row">
                            <div class="col-md-6">
                               <div class="form-group">
                                <label for="exampleFormControlInput1">Email*</label>
                                <input type="email" name="email" class="form-control" required="" id="exampleFormControlInput1" placeholder="Email*">
                              </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleFormControlInput1">Confirm Email*</label>
                                <input type="text" name="confirm_email" class="form-control" required="" id="exampleFormControlInput1" placeholder="Confirm Email*">
                              </div>
                          </div>
                       </div>

                       <div class="row">
                            <div class="col-md-6">
                               <div class="form-group">
                                <label for="exampleFormControlInput1">Cell Phone Number*</label>
                                <input type="text" name="cell_phone" class="form-control" required="" id="exampleFormControlInput1" placeholder="Cell Phone Number*">
                              </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleFormControlInput1">Confirm Cell Phone Number*</label>
                                <input type="text" name="confirm_cell_phone" class="form-control" required="" id="exampleFormControlInput1" placeholder="Confirm Cell Phone Number*">
                              </div>
                          </div>
                       </div>

                       <div class="row">
                            <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Address*</label>
                                      <input name="address" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Address*">
                                    </div>
                            </div>
                       </div>


                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Country</label>
                          <select name="country" class="form-control" id="exampleFormControlSelect1">
                            <option>Select Country</option>
                            <option>Canada</option>
                            <option>Holland</option>
                            <option>United States</option>
                            <option>Mexico</option>
                          </select>
                        </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group">
                          <label for="exampleFormControlSelect2">Province/State</label>
                          <select name="state" class="form-control" id="exampleFormControlSelect2">
                            <option>Select Province/State</option>
                            <option>N/A</option>
                          </select>
                        </div>

                        </div>
                       </div>


                       <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Postal / ZIP Code *</label>
                                      <input name="zip_code" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Postal / ZIP Code *">
                                    </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">City</label>
                                   <select name="city" class="form-control" id="exampleFormControlSelect2">
                                    <option>Select City</option>
                                    <option>N/A</option>
                                  </select>
                                </div>
                            </div>
                       </div>

                        <div class="row">
                            <div class="col-md-12">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">Health Card Number</label>
                                   <input type="text" name="health_card_number" class="form-control" id="exampleFormControlInput1" placeholder="Health Card Number *">
                                  </div>
                            </div>
                       </div>

                       <div class="row">
                            <div class="col-md-12">
                                  <div class="form-group" style="text-align: center;">
                                   <label for="exampleFormControlTextarea1">Designated Gender At Birth *</label><br>
                                   <input type="radio" onclick="genderSelect('a')"  id="gender_selected_male" placeholder="Health Card Number *"> Male
                                   <input type="radio" onclick="genderSelect('b')" id="gender_selected_female" placeholder="Health Card Number *"> Female
                                      <input type="hidden" name="gender" id="gender_selected" />
                                  </div>
                            </div>
                       </div>

                       <hr>
                       <div class="x-card-title h4">Preferred Pharmacy</div>
<br>
                      <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Pharmacy Name *</label>
                                      <input type="text" name="pharmacy_name" class="form-control" id="exampleFormControlInput1" placeholder="Pharmacy Name *">
                                    </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">Pharmacy Fax Number *</label>
                                   <input type="text" name="pharmacy_fax" class="form-control" id="exampleFormControlInput1" placeholder="Pharmacy Fax Number *">
                                </div>
                            </div>
                       </div>


                        <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Password *</label>
                                      <input type="password" name="password" class="form-control" id="exampleFormControlInput1" placeholder="Password *">
                                    </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">Confirm Password *</label>
                                   <input type="password" name="confirm_password" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Password *">
                                </div>
                            </div>
                       </div>
                       <br>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                  </div>
              </form>
            </div>
          </div>
        </div>
        <!-- END NEW ITEM -->
      </div>


    </div>


@endsection
@push('js')
    <script>
        function genderSelect(val){
            if(val == 'a') {
                $('#gender_selected_male').prop('checked',true);
                $('#gender_selected_female').prop('checked',false);
                $('#gender_selected').val(0);
            } else {
                $('#gender_selected_male').prop('checked',false);
                $('#gender_selected_female').prop('checked',true);
                $('#gender_selected').val(1);
            }
        }
    $('.year-own').datepicker();
    </script>
@endpush
