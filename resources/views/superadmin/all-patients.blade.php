@extends('layouts.default')
@push('css')
@endpush
@section('content')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
<link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
 <div class="x-card p-4 h-100">
     @if(session()->has('success_message'))
         <div class="row">
             <div class="col-md-12">
                 <div class="alert alert-success">
                     {{session()->get('success_message')}}
                 </div>
             </div>
         </div>
     @endif

            <table id="ajax_source_example" class="table table-striped table-borderless dt-responsive nowrap w-100">
                <thead class="text-dark">
                    <tr>
                        <th data-i18n="table.name">Sr#</th>
                        <th data-i18n="table.first_name">First Name</th>
                        <th data-i18n="table.email">Email</th>
                        <th data-i18n="table.aktion">Phone Number</th>
                        <th data-i18n="table.aktion">Country</th>
                        <th data-i18n="table.aktion">City</th>
                        <th data-i18n="table.aktion">Health Card Number</th>
                        <th data-i18n="table.aktion">Gender</th>
                        <th data-i18n="table.aktion">Pharmacy Name</th>
                        <th data-i18n="table.aktion">Pharmacy Fax Number</th>
                        <th data-i18n="table.aktion">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($patient as $patients)
                    <tr>
                        <td>{{$patients->id}}</td>
                        <td>{{$patients->first_name}}</td>
                        <td>{{$patients->email}}</td>
                        <td>{{$patients->cell_phone_number}}</td>
                        <td>{{$patients->country}}</td>
                        <td>{{$patients->city}}</td>
                        <td>{{$patients->health_care_number}}</td>
                        <td>{{$patients->gender}}</td>
                        <td>{{$patients->pharmacy_name}}</td>
                        <td>{{$patients->pharmacy_fax_number}}</td>
                        <td>
                            <a href="{{route('deletePatients',[$patients->id])}}" style="padding:6px;" class="btn btn-primary btn-sm btn-pill text-white delete-user">Delete</a>
                            <a href="{{route('detailPatients',[$patients->id])}}" style="padding:6px;" class="btn btn-success btn-sm btn-pill text-white delete-user">Detail</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                  <!--   <tr>
                        <th data-i18n="table.name">Sr#</th>
                        <th data-i18n="table.position">First Name</th>
                        <th data-i18n="table.salary">Email</th>
                        <th data-i18n="table.aktion">Phone Number</th>
                        <th data-i18n="table.aktion">Country</th>
                        <th data-i18n="table.aktion">City</th>
                        <th data-i18n="table.aktion">Health Card Number</th>
                        <th data-i18n="table.aktion">Gender</th>
                        <th data-i18n="table.aktion">Pharmacy Name</th>
                        <th data-i18n="table.aktion">Pharmacy Fax Number</th>
                        <th data-i18n="table.aktion">Action</th>

                    </tr> -->
                </tfoot>
            </table>
        </div>

@endsection
@push('js')

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>

  @endpush
