@extends('layouts.default')
@push('css')
@endpush
@section('content')

  
  <div class="row">
      <div class="col-lg-12">
        <!-- BEGIN NEW ITEM -->
        <div class="x-card">
          <div class="x-card-header">

             @if(session()->has('success_message'))
         <div class="row">
             <div class="col-md-12">
                 <div class="alert alert-success">
                     {{session()->get('success_message')}}
                 </div>
             </div>
         </div>
     @endif
            <div class="x-card-title h4">Create New Page</div>
            <a class="heading-elements-toggle">
              <i class="ion size-18 ion-ios-more"></i>
            </a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li class="list-inline-item">
                  <a data-action="collapse">
                    <i class="ion ion-minus-round"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="close">
                    <i class="ion ion-android-close"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="reload">
                    <i class="ion ion-android-refresh"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="expand">
                    <i class="ion ion-android-expand"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="x-card-body collapse show">
            <div class="text-dark">
               <form method="POST" action="{{url('super-admin/create-new-page-post')}}" enctype="multipart/form-data">
                   @csrf
                  <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Page Title</label>
                          <input name="page_title" type="text" class="form-control" required="" id="exampleFormControlInput1" placeholder="Page Title">
                        </div>

                        <div class="form-group">
                          <label for="exampleFormControlInput1">Page Slug</label>
                          <input name="page_slug" type="text" class="form-control" required="" id="exampleFormControlInput1" placeholder="Page Slug">
                        </div>

                    
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Page Content</label>

                        	  <textarea id="editor" name="page_content"></textarea>
                        </div>
                    

                       <br>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                  </div>
              </form>
            </div>
          </div>
        </div>
        <!-- END NEW ITEM -->
      </div>


    </div>

@endsection
@push('js')
<script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
  <script>
        ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
    </script>
@endpush
