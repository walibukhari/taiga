@extends('layouts.default')
@push('css')
@endpush
@section('content')

    @if(session()->has('success_message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            </div>
        </div>
    @endif
    @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="row">
      <div class="col-lg-12">
        <!-- BEGIN NEW ITEM -->
        <div class="x-card">
          <div class="x-card-header">
            <div class="x-card-title h4">Create Doctor</div>
            <a class="heading-elements-toggle">
              <i class="ion size-18 ion-ios-more"></i>
            </a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li class="list-inline-item">
                  <a data-action="collapse">
                    <i class="ion ion-minus-round"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="close">
                    <i class="ion ion-android-close"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="reload">
                    <i class="ion ion-android-refresh"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="expand">
                    <i class="ion ion-android-expand"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="x-card-body collapse show">
            <div class="text-dark">
               <form method="POST" action="{{route('postDoctor')}}" enctype="multipart/form-data">
                   @csrf
                  <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Clinic or Organization Name</label>
                          <input name="clinic_organization" type="text" class="form-control" required="" id="exampleFormControlInput1" placeholder="Clinic or Organization Name">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="exampleFormControlSelect1">Country</label>
                                <select name="country" class="form-control" id="exampleFormControlSelect1">
                                  <option>Select Country</option>
                                  <option>Canada</option>
                                  <option>Holland</option>
                                  <option>United States</option>
                                  <option>Mexico</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleFormControlSelect2">Province/State</label>
                              <select class="form-control" name="state" id="exampleFormControlSelect2">
                                <option>Select Province/State</option>
                                <option>N/A</option>
                              </select>
                            </div>
                           </div>
                       </div>

                       <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Main Location / Address</label>
                                      <input type="text" name="address" class="form-control" id="exampleFormControlInput1" placeholder="Main Location / Address *">
                                    </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">City</label>
                                   <select name="city" class="form-control" id="exampleFormControlSelect2">
                                    <option>Select City</option>
                                    <option>N/A</option>
                                  </select>
                                </div>
                            </div>
                       </div>

                        <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">First Name</label>
                                   <input type="text" name="first_name" class="form-control" id="exampleFormControlInput1" placeholder="First Name *">
                                  </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlTextarea1">Last Name</label>
                                  <input type="text" name="last_name" class="form-control" id="exampleFormControlInput1" placeholder="Last Name *">
                                </div>
                            </div>
                       </div>


                        <div class="row">
                            <div class="col-md-12">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">Email</label>
                                   <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Email *">
                                  </div>
                            </div>
                       </div>


                     <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">Cell Phone Number</label>
                                   <input type="number" name="cell_phone" class="form-control" id="exampleFormControlInput1" placeholder="Cell Phone Number *">
                                  </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlTextarea1">Confirm Cell Phone Number</label>
                                  <input type="number" name="confirm_cell_phone" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Cell Phone Number *">
                                </div>
                            </div>
                       </div>

                       <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">Clinic Phone Number</label>
                                   <input type="number" name="clinic_phone_number" class="form-control" id="exampleFormControlInput1" placeholder="Clinic Phone Number *">
                                  </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlTextarea1">Confirm Clinic Phone Number</label>
                                  <input type="number" name="confirm_clinic_phone_number" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Clinic Phone Number *">
                                </div>
                            </div>
                       </div>

                       <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                   <label for="exampleFormControlTextarea1">Password</label>
                                   <input type="password" name="password" class="form-control" id="exampleFormControlInput1" placeholder="Password *">
                                  </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleFormControlTextarea1">Confirm Password</label>
                                  <input type="password" name="confirm_password" class="form-control" id="exampleFormControlInput1" placeholder="Confirm Password *">
                                </div>
                            </div>
                       </div>
                       <br>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                  </div>
              </form>
            </div>
          </div>
        </div>
        <!-- END NEW ITEM -->
      </div>


    </div>


@endsection
@push('js')
    <script>

    </script>
@endpush
