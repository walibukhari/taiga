@extends('layouts.default')
@push('css')
@endpush
@section('content')
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
    <link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
    <div class="x-card p-4 h-100">

        @if(session()->has('message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('message')}}
                    </div>
                </div>
            </div>
        @endif
        <table id="ajax_source_example" class="table table-striped table-borderless dt-responsive nowrap w-100">
            <thead class="text-dark">
            <tr>
                <th data-i18n="table.name">Sr#</th>
                <th data-i18n="table.position">Organization Name</th>
                <th data-i18n="table.office">Country</th>
                <th data-i18n="table.startdate">Address</th>
                <th data-i18n="table.aktion">First Name</th>
                <th data-i18n="table.aktion">Last Name</th>
                <th data-i18n="table.aktion">Email</th>
                <th data-i18n="table.aktion">Phone Number</th>
                <th data-i18n="table.aktion">Clinic Number</th>
                <th data-i18n="table.aktion">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($doctor as $doctors)
                <tr>
                    <td>{{$doctors->id}}</td>
                    <td>{{$doctors->clinic_name}}</td>
                    <td>{{$doctors->country}}</td>
                    <td>{{$doctors->address}}</td>
                    <td>{{$doctors->first_name}}</td>
                    <td>{{$doctors->last_name}}</td>
                    <td>{{$doctors->email}}</td>
                    <td>{{$doctors->cell_phone}}</td>
                    <td>{{$doctors->clinic_phone}}</td>
                    <td>
                        <a href="{{route('doctorDelete',[$doctors->id])}}" style="padding:6px;" class="btn btn-primary btn-sm btn-pill text-white delete-user">Delete</a>
                        <a href="{{route('doctorDetail',[$doctors->id])}}" style="padding:6px;" class="btn btn-success btn-sm btn-pill text-white dz-details">Detail</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <!-- <tr>
                <th data-i18n="table.name">Sr#</th>
                <th data-i18n="table.position">Organization Name</th>
                <th data-i18n="table.office">Country</th>
                <th data-i18n="table.startdate">Address</th>
                <th data-i18n="table.aktion">First Name</th>
                <th data-i18n="table.aktion">Last Name</th>
                <th data-i18n="table.aktion">Email</th>
                <th data-i18n="table.aktion">Phone Number</th>
                <th data-i18n="table.aktion">Clinic Number</th>
                <th data-i18n="table.aktion">Action</th>
            </tr> -->
            </tfoot>
        </table>
    </div>

@endsection
@push('js')

    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
    <script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>

@endpush
