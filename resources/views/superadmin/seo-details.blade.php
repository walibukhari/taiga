@extends('layouts.default')
@push('css')
@endpush
@section('content')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" media="all"> -->
<link rel="stylesheet" href="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/css/dataTables.bootstrap4.min.css" media="all">
 <div class="x-card p-4 h-100">

    <div class="x-card-header">
            <div class="x-card-title h4">SEO Settings</div>
            <a class="heading-elements-toggle">
              <i class="ion size-18 ion-ios-more"></i>
            </a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li class="list-inline-item">
                  <a data-action="collapse">
                    <i class="ion ion-minus-round"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="close">
                    <i class="ion ion-android-close"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="reload">
                    <i class="ion ion-android-refresh"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a data-action="expand">
                    <i class="ion ion-android-expand"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>

     @if(session()->has('success_message'))
         <div class="row">
             <div class="col-md-12">
                 <div class="alert alert-success">
                     {{session()->get('success_message')}}
                 </div>
             </div>
         </div>
     @endif


            <div class="x-card-body collapse show">
            <div class="text-dark">

            <table id="ajax_source_example" class="table table-striped table-borderless dt-responsive nowrap w-100">
                <thead class="text-dark">
                    <tr>
                        <th data-i18n="table.Sr#">Sr#</th>
                        <th data-i18n="table.Name">Name</th>
                        <th data-i18n="table.Meta Title">Meta Title</th>
                        <th data-i18n="table.META DESCRIPTION">META DESCRIPTION</th>
                        <th data-i18n="table.KEYWORDS">KEYWORDS</th>
                        <th data-i18n="table.EDIT">EDIT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Seo_setting as $Seo_settings)
                    <tr>
                        <td>{{$Seo_settings->id}}</td>
                        <td>{{$Seo_settings->Page_title->page_title}}</td>
                        <td>{{$Seo_settings->meta_author}}</td>
                        <td>{{$Seo_settings->meta_description}}</td>
                        <td>{{$Seo_settings->keywords}}</td>
                        <td><a    data-toggle="modal" data-target="#myModal{{$Seo_settings->id}}" style="padding:6px;" class="btn btn-success btn-sm btn-pill text-white ">Edit</a>

                             <!-- Modal -->
                          <div class="modal fade" id="myModal{{$Seo_settings->id}}" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Update SEO Settings</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  
                                  <form action="{{url('super-admin/seo-details-post')}}" method="POST">
                                    @csrf 
                                    <input type="hidden" name="id" value="{{$Seo_settings->id}}">

                                    <div class="form-group">
                                     <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="exampleFormControlSelect1">Page Name</label>
                                            <input type="text" name="" class="form-control" id="exampleFormControlInput1" placeholder="Page Name" readonly="" value="{{$Seo_settings->Page_title->page_title}}">
                                          </div>
                                        </div>

                                        <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="exampleFormControlSelect2">Meta Title</label>
                                           <input type="text" name="meta_author" class="form-control" id="exampleFormControlInput1" placeholder="Meta Title"  value="{{$Seo_settings->meta_author}}">
                                        </div>
                                       </div>
                                    </div> 
                                   </div>


                                   <div class="form-group">
                                     <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="exampleFormControlSelect1">Meta Description</label>
                                            <textarea class="form-control" name="meta_description">{{$Seo_settings->meta_description}}</textarea>
                                          </div>
                                        </div>
                                    </div> 
                                   </div>

                                   <div class="form-group">
                                     <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="exampleFormControlSelect1">Keywords</label>
                                            <input type="text"  class="form-control" id="exampleFormControlInput1" placeholder="Keywords" name="keywords" value="{{$Seo_settings->keywords}}">
                                          </div>
                                        </div>
                                    </div> 
                                   </div>

                                    <button type="submit" class="btn btn-primary">Update</button>
                                  </form>

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                   <!--  <tr>
                       <th data-i18n="table.Sr#">Sr#</th>
                        <th data-i18n="table.Name">Name</th>
                        <th data-i18n="table.Meta Title">Meta Title</th>
                        <th data-i18n="table.META DESCRIPTION">META DESCRIPTION</th>
                        <th data-i18n="table.KEYWORDS">KEYWORDS</th>
                        <th data-i18n="table.EDIT">EDIT</th>

                    </tr> -->
                </tfoot>
            </table>


        </div>
    </div>
        </div>

@endsection
@push('js')

<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->

<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/datatables.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.responsive.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.buttons.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/buttons.bootstrap4.min.js"></script>
<script src="https://softsuitetechnologies.com/DEMO/Hospital_dahaboard/HTML-VERSION/js/dataTables.checkboxes.min.js"></script>

  @endpush
