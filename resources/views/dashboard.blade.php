@extends('layouts.default')
@push('css')
@endpush
@section('content')
<div class="row">
    <div class="col-xl-6">
        <!-- Begin item -->
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span data-i18n="dashboard.visitors"></span>
                    <span class="h6 font-weight-light ml-2 mt-2" data-i18n="dashboard.pageviews"></span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body py-4 collapse show">
                <div class="mb-2 mt-1">
                    <div id="stackingchart" style="height:300px;"></div>
                </div>
                <div class="d-flex flex-row mb-3">
                    <div class="align-self-start w-50">
                        <div class="d-flex flex-column">
                            <div class="mb-auto mr-auto py-3">
                                <div class="h5 text-uppercase text-dark">Conversion</div>
                                <span class="h5"> $
                        <span class="counter" data-count="816">0</span> today
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="align-self-start d-flex w-50 py-3">
                        <div class="sparkreveal ml-auto mb-auto"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 d-flex">
                        <span class="badge badge-danger my-auto p-2 mr-2">5%</span> variation this hour
                        <i class="ion ion-chevron-down ml-auto"></i>
                    </div>
                    <div class="col-4 d-flex">
                        <div class="btn-group  ml-auto">
                            <button class="btn btn-primary btn-sm pl-2">Buy</button>
                            <button class="btn btn-primary btn-sm pr-2">Sell</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Item -->
    </div>
    <div class="col-xl-6">
        <!-- Begin Counter stats -->
        <div class="row">
            <!-- Begin item -->
            <div class="col-md-6">
                <div class="x-card bg-primary">
                    <div class="x-card-header border-0">
                        <span class="text-white text-uppercase h5" data-i18n="dashboard.pageviews"></span>
                        <span class="pull-right"><i class="ion ion-social-vimeo-outline size-24 text-white"></i></span>
                        <div class="text-white h6" data-i18n="dashboard.hourlystats"></div>
                    </div>
                    <div class="x-card-body d-flex bg-opacity-100">
                        <div class="my-auto w-100 text-white">
                            <span class="counter h1" data-count="5250">4,000</span>
                            <span class="pull-right">
                        <span>5</span>%
                        <i class="fa fa-caret-up"></i>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End item -->
            <!-- Begin item -->
            <div class="col-md-6">
                <div class="x-card bg-quaternary">
                    <div class="x-card-header border-0">
                        <span class="text-white text-uppercase h5" data-i18n="dashboard.pageviews"></span>
                        <span class="pull-right"><i class="ion ion-social-tumblr-outline size-24 text-white"></i></span>
                        <div class="text-white h6" data-i18n="dashboard.dailystats"></div>
                    </div>
                    <div class="x-card-body d-flex bg-opacity-100">
                        <div class="my-auto w-100 text-white">
                            <span class="counter h1" data-count="120587">100,000</span>
                            <span class="pull-right">
                        <span>15</span>%
                        <i class="fa fa-caret-up"></i>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End item -->
            <!-- Begin item -->
            <div class="col-md-6">
                <div class="x-card bg-quinary">
                    <div class="x-card-header border-0">
                        <span class="text-white text-uppercase h5" data-i18n="dashboard.pageviews"></span>
                        <span class="pull-right"><i class="ion ion-social-euro-outline size-24 text-white"></i></span>
                        <div class="text-white h6" data-i18n="dashboard.weeklystats"></div>
                    </div>
                    <div class="x-card-body d-flex bg-opacity-100">
                        <div class="my-auto w-100 text-white">
                            <span class="counter h1" data-count="855789">800,000</span>
                            <span class="pull-right">
                        <span>8</span>%
                        <i class="fa fa-caret-up"></i>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End item -->
            <!-- Begin item -->
            <div class="col-md-6">
                <div class="x-card bg-octonary">
                    <div class="x-card-header border-0">
                        <span class="text-white text-uppercase h5" data-i18n="dashboard.pageviews"></span>
                        <span class="pull-right"><i class="ion ion-social-facebook-outline size-24 text-white"></i></span>
                        <div class="text-white h6" data-i18n="dashboard.monthlystats"></div>
                    </div>
                    <div class="x-card-body d-flex bg-opacity-100">
                        <div class="my-auto w-100 text-white">
                            <span class="counter h1" data-count="985000">7,000,000</span>
                            <span class="pull-right">
                        <span>12</span>%
                        <i class="fa fa-caret-up"></i>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End item -->
        </div>
        <!-- End Odometer stats -->
        <div class="alert alert-info mb-4" role="alert">
            <h4 class="alert-heading">Welcome Back John Doe!</h4>
            <p>What pages do your users visit?. Curabitur et vestibulum sem. Pellentesque
                leo ligula, convallis nec vehicula convallis, pharetra sed mauris. Curabitur neque purus, luctus
                sit amet nunc non, pellentesque interdum sapien.</p>
            <hr>
            <p class="mb-0">Mauris libero erat, ultricies vitae elit ut, pellentesque suscipit ex. Sed porta malesuada turpis a tincidunt.</p>
        </div>
    </div>
    <!-- Begin item -->
    <div class="col-xl-8">
        <!-- Begin item -->
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span>Project Status</span>
                    <span class="h6 font-weight-light ml-2 mt-2" data-i18n="dashboard.stabletoday">Stable today</span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="close">
                                <i class="ion ion-android-close"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="reload">
                                <i class="ion ion-android-refresh"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body table-responsive">
                <table class="table table-borderless w-100">
                    <thead class="text-dark">
                    <tr>
                        <th class="d-none d-md-block">
                            Avatar
                        </th>
                        <th>
                            Team
                        </th>
                        <th>
                            Progress
                        </th>
                        <th class="text-right">
                            Amount
                        </th>
                        <th class="text-right">
                            Deadline
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="py-1 d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            John Doe
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $ 102.95
                        </td>
                        <td class="text-right">
                            Avril 21, 2018
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Frank Williams
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $172.355
                        </td>
                        <td class="text-right">
                            Febr 6, 2018
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Melly Simson
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $472.00
                        </td>
                        <td class="text-right">
                            Mars 21, 2018
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Bert Kampf
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-octonary" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $ 101.65
                        </td>
                        <td class="text-right">
                            Dec 25, 2017
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Ann Smith
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $ 204.65
                        </td>
                        <td class="text-right">
                            April 16, 2018
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Will Jones
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $ 305.45
                        </td>
                        <td class="text-right">
                            April 18, 2018
                        </td>
                    </tr>
                    <tr>
                        <td class="py-1  d-none d-md-block">
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="image"></span>
                            <span class="ie-cover-fit"><img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light image-team" alt="image"></span>
                        </td>
                        <td class="text-dark">
                            Mark Twain
                        </td>
                        <td>
                            <div class="progress progress-sm mt-2">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </td>
                        <td class="text-right">
                            $ 215.00
                        </td>
                        <td class="text-right">
                            Jan 21, 2018
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="d-flex mt-5">
                    <span class="text-uppercase text-dark" data-i18n="dashboard.time"></span>
                    <div class="ml-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#" title="Day" class="text-dark" data-i18n="dashboard.day"></a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">
                                <a href="#" title="Week" class="text-dark" data-i18n="dashboard.week"></a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">
                                <a href="#" title="Month" class="text-dark" data-i18n="dashboard.month"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End item -->
        <div class="row">
            <div class="col-lg-6">
                <!-- Begin item -->
                <div class="x-card">
                    <div class="x-card-header">
                        <div class="x-card-title h4">
                            <span data-i18n="dashboard.cpuusage"></span>
                        </div>
                        <a class="heading-elements-toggle">
                            <i class="ion size-18 ion-ios-more"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a data-action="close">
                                        <i class="ion ion-android-close"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="reload">
                                        <i class="ion ion-android-refresh"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="expand">
                                        <i class="ion ion-android-expand"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="x-card-body d-flex flex-column">
                        <canvas id="canvas-cpu" class="stats-height"></canvas>
                        <div class="d-flex mt-5">
                            <span class="text-uppercase" data-i18n="dashboard.time"></span>
                            <div class="ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" title="Day" data-i18n="dashboard.day"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Week" data-i18n="dashboard.week"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Month" data-i18n="dashboard.month"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
            </div>
            <div class="col-lg-6">
                <!-- Begin item -->
                <div class="x-card">
                    <div class="x-card-header">
                        <div class="x-card-title h4" data-i18n="dashboard.visitors">
                        </div>
                        <a class="heading-elements-toggle">
                            <i class="ion size-18 ion-ios-more"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a data-action="collapse">
                                        <i class="ion ion-minus-round"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="close">
                                        <i class="ion ion-android-close"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="reload">
                                        <i class="ion ion-android-refresh"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="expand">
                                        <i class="ion ion-android-expand"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="x-card-body collapse show">
                        <div class="stats-height">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="mb-2 font-weight-bold text-dark" data-i18n="dashboard.chart"></div>
                                    <canvas id="devicechart" style="height:200px; width:200px"></canvas>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="mb-2 font-weight-bold text-dark" data-i18n="dashboard.device"></div>
                                    <ul class="list-unstyled">
                                        <li class="py-2 text-dark">
                                            <i class="fa fa-square text-octonary mr-2"></i>IOS
                                        </li>
                                        <li class="py-2 text-dark">
                                            <i class="fa fa-square text-info mr-2"></i>Android
                                        </li>
                                        <li class="py-2 text-dark">
                                            <i class="fa fa-square text-quinary mr-2"></i>Blackber.
                                        </li>
                                        <li class="py-2 text-dark">
                                            <i class="fa fa-square text-quaternary mr-2"></i>Symbian
                                        </li>
                                        <li class="py-2 text-dark">
                                            <i class="fa fa-square text-primary mr-2"></i>
                                            <span data-i18n="dashboard.others"></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 col-md-3">
                                    <div class="mb-2 font-weight-bold text-dark" data-i18n="dashboard.progress"></div>
                                    <ul class="list-unstyled">
                                        <li class="py-2">
                                            30%
                                        </li>
                                        <li class="py-2">
                                            20%
                                        </li>
                                        <li class="py-2">
                                            10%
                                        </li>
                                        <li class="py-2">
                                            5%
                                        </li>
                                        <li class="py-2">
                                            40%
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex mt-5">
                            <span class="text-uppercase text-dark" data-i18n="dashboard.time"></span>
                            <div class="ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" title="Day" class="text-dark" data-i18n="dashboard.day"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Week" class="text-dark" data-i18n="dashboard.week"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Month" class="text-dark" data-i18n="dashboard.month"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
            </div>
            <div class="col-lg-6">
                <!-- Begin item -->
                <div class="x-card">
                    <div class="x-card-header">
                        <div class="x-card-title h4">
                            <span data-i18n="dashboard.lastmessage"></span>
                        </div>

                        <a class="heading-elements-toggle">
                            <i class="ion size-18 ion-ios-more"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a data-action="close">
                                        <i class="ion ion-android-close"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="reload">
                                        <i class="ion ion-android-refresh"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="expand">
                                        <i class="ion ion-android-expand"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="x-card-body d-flex flex-column">
                        <ul class="list-group list-group-flush stats-height">
                            <li class="list-group-item">
                                <div class="media">
          <span class="text-info size-32 d-flex mr-3">
            <img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="User Avatar">
          </span>
                                    <i class="bullet bullet-avatar bullet-warning"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="dashboard.filesuploaded"></span>
                                        </p>
                                        <span class="font-weight-light small">
              @frankwilliams
            </span>
                                        <span class="font-weight-light small pull-right">
              15 mns ago
            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
          <span class="text-success size-32 d-flex mr-3">
            <img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="User Avatar">
          </span>
                                    <i class="bullet bullet-avatar bullet-danger"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="dashboard.chatbotinit"></span>
                                        </p>
                                        <span class="font-weight-light small">
              @mollysims
            </span>
                                        <span class="font-weight-light small pull-right">
              21 mns ago
            </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media">
          <span class="text-danger size-32 d-flex mr-3">
            <img src="assets/img/60x60.png" class="img-sq-50 rounded-circle img-profile-light" alt="User Avatar">
          </span>
                                    <i class="bullet bullet-avatar bullet-primary"></i>
                                    <div class="media-body my-auto">
                                        <p class="my-0">
                                            <span class="font-weight-bold" data-i18n="dashboard.serverfailed"></span>
                                        </p>
                                        <span class="font-weight-light small">
              @johndoe
            </span>
                                        <span class="font-weight-light small pull-right">
              35 mns ago
            </span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                        <div class="d-flex mt-5">
                            <span class="text-uppercase text-dark" data-i18n="dashboard.time"></span>
                            <div class="ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" title="Day" class="text-dark" data-i18n="dashboard.day"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Week" class="text-dark" data-i18n="dashboard.week"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Month" class="text-dark" data-i18n="dashboard.month"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
            </div>
            <div class="col-lg-6">
                <!-- Begin item -->
                <div class="x-card">
                    <div class="x-card-header">
                        <div class="x-card-title h4">
                            <span data-i18n="dashboard.bandwidth"></span>
                            <span class="h6 font-weight-light ml-2 mt-2" data-i18n="dashboard.stabletoday">Stable today</span>
                        </div>
                        <a class="heading-elements-toggle">
                            <i class="ion size-18 ion-ios-more"></i>
                        </a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a data-action="close">
                                        <i class="ion ion-android-close"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="reload">
                                        <i class="ion ion-android-refresh"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a data-action="expand">
                                        <i class="ion ion-android-expand"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="x-card-body d-flex flex-column">
                        <div id="flotchartrealtime" class=" stats-height"></div>
                        <div class="d-flex mt-5">
                            <span class="text-uppercase" data-i18n="dashboard.time"></span>
                            <div class="ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" title="Day" data-i18n="dashboard.day"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Week" data-i18n="dashboard.week"></a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" title="Month" data-i18n="dashboard.month"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
            </div>
        </div>
        <!-- Begin item -->
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span data-i18n="dashboard.countries"></span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="close">
                                <i class="ion ion-android-close"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="reload">
                                <i class="ion ion-android-refresh"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body d-flex flex-column">
                <div class="vh-50">
                    <div class="row">
                        <div class="col-lg-4 text-dark d-flex flex-column">
                            <div class="my-auto">
                                <div class="mb-2">
                                    <i class="fa fa-circle text-quaternary mr-2"></i>
                                    <span data-i18n="dashboard.todaycampaign"></span>
                                </div>
                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-quaternary" role="progressbar" data-transitiongoal="80" style="width: 80%;" aria-valuenow="79"></div>
                                </div>

                                <div  class="mb-2">
                                    <i class="fa fa-circle text-info mr-2"></i>
                                    <span data-i18n="dashboard.yesterdaycampaign"></span>
                                </div>
                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-info" role="progressbar" data-transitiongoal="60" style="width: 60%;" aria-valuenow="59"></div>
                                </div>

                                <div class="mb-2">
                                    <i class="fa fa-circle text-success mr-2"></i>
                                    <span data-i18n="dashboard.thisweekcampaign"></span>
                                </div>

                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-success" role="progressbar" data-transitiongoal="40" style="width: 40%;" aria-valuenow="39"></div>
                                </div>

                                <div class="mb-2">
                                    <i class="fa fa-circle text-denary mr-2"></i>
                                    <span data-i18n="dashboard.thismonthcampaign"></span>
                                </div>
                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-denary" role="progressbar" data-transitiongoal="70" style="width: 70%;" aria-valuenow="69"></div>
                                </div>

                                <div class="mb-2">
                                    <i class="fa fa-circle text-octonary mr-2"></i>
                                    <span data-i18n="dashboard.thisyearcampaign"></span>
                                </div>
                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-octonary" role="progressbar" data-transitiongoal="50" style="width: 50%;" aria-valuenow="49"></div>
                                </div>

                                <div class="mb-2">
                                    <i class="fa fa-circle text-danger mr-2"></i>
                                    <span data-i18n="dashboard.lastyearcampaign"></span>
                                </div>
                                <div class="progress progress-sm bg-white w-xl-75">
                                    <div class="progress-bar bg-danger" role="progressbar" data-transitiongoal="65" style="width: 65%;" aria-valuenow="64"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div id="vmap" style="width: 100%; height: 420px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End item -->

    </div>
    <!-- End item -->
    <!-- Begin item -->
    <div class="col-xl-4">
        <div class="x-card bg-primary p-4 mb-4">
            <!-- Begin progress circle widgets -->
            <div class="row">
                <!-- Begin sparkline item -->
                <div class="col-md-12">
                    <div class="mb-2">
                        <div class="p-4 text-white">
                            <div class="h5 d-inline-flex">Tasks Stats</div>
                            <span class="pull-right h5">
                    <i class="ion ion-ios-arrow-down"></i>
                </span>
                            <div class="small">Down today</div>
                            <canvas id="canvas-stats" style="height:70px;width:100%;"></canvas>
                        </div>
                        <div class="px-4 my-4 d-flex text-white">
                            <span>Time</span>
                            <div class="ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="#" class="text-white" title="Hour">Hour</a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" class="text-white" title="Day">Day</a>
                                    </li>
                                    <li class="list-inline-item">|</li>
                                    <li class="list-inline-item">
                                        <a href="#" class="text-white" title="Month">Month</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End sparkline item -->
                <!-- Start progress item -->
                <div class="col-xxl-6">
                    <div class="mb-2 text-white">
                        <div class="d-flex p-3">
                            <div class="my-auto mr-auto">
                                <span class="counter h1" data-count="248">0</span>
                                <span class="h5">/520</span>
                                <h6 class="page-subtitle mt-1">Tasks Done</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End progress item -->
                <!-- Start progress item -->
                <div class="col-xxl-6">
                    <div class="text-white">
                        <div class="p-4 mb-4">
                            <div class="h5">
                                <span class="counter" data-count="52">0</span>%</div>
                            <h6 class="card-subtitle mb-1">Pending Task</h6>
                            <div class="progress mb-3" style="height: 6px;">
                                <div class="progress-bar progress-bar-counter bg-info" role="progressbar" style="width: 0;" aria-valuenow="52" aria-valuemin="0"
                                     aria-valuemax="100">
                                </div>
                            </div>
                            <div class="h5">
                                <span class="counter" data-count="61">0</span>%</div>
                            <h6 class="card-subtitle mb-1">Days Left</h6>
                            <div class="progress mb-3" style="height: 6px;">
                                <div class="progress-bar progress-bar-counter bg-info" role="progressbar" style="width: 0;" aria-valuenow="61" aria-valuemin="0"
                                     aria-valuemax="100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End progress item -->
            </div>
            <!-- end ./row -->

            <!-- Begin Odometer stats -->
            <div class="row">
                <!-- Begin item -->
                <div class="col-md-4">
                    <div class="mb-4 text-white">
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">LOGGED HOURS</h6>
                            <div class="small">
                                <span class="counter" data-count="76">0</span>
                                <span> H 00</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">TOTAL</h6>
                            <div class="small">
                                $
                                <span class="counter" data-count="5300">4000</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">VARIATION</h6>
                            <div class="small">
                                <span>5</span>%
                                <i class="fa fa-caret-up"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
                <!-- Begin item -->
                <div class="col-md-4">
                    <div class="mb-4 text-white">
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">BILLED HOURS</h6>
                            <div class="small">
                                <span class="counter" data-count="125">0</span>
                                <span> H 00</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">TOTAL</h6>
                            <div class="small">
                                $
                                <span class="counter" data-count="4500">3000</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">VARIATION</h6>
                            <div class="small">
                                <span>15</span>%
                                <i class="fa fa-caret-up"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
                <!-- Begin item -->
                <div class="col-md-4">
                    <div class="mb-4 text-white">
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">TOTAL EXPENSES</h6>
                            <div class="small">
                                <span class="counter" data-count="175">0</span>
                                <span> H 00</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">TOTAL</h6>
                            <div class="small">
                                $
                                <span class="counter" data-count="12500">10000</span>
                            </div>
                        </div>
                        <div class="px-4 mb-3">
                            <h6 class="text-uppercase">VARIATION</h6>
                            <div class="small">
                                <span>15</span>%
                                <i class="fa fa-caret-up"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End item -->
            </div>
            <!-- end ./row -->
        </div>
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span data-i18n="dashboard.visitors"></span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body collapse show">
                <div class="x-card-header">
                    <div class="x-card-title h4" data-i18n="dashboard.convertionrate">
                    </div>
                    <div class="x-card-subtitle" data-i18n="dashboard.downtoday"></div>
                </div>
                <div class="x-card-body">
                    <div class="mb-2">
                        <i class="fa fa-circle text-primary mr-2"></i>
                        <span data-i18n="dashboard.todaycampaign"></span>
                    </div>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary" role="progressbar" data-transitiongoal="80" style="width: 80%;" aria-valuenow="79"></div>
                    </div>

                    <div class="mb-2">
                        <i class="fa fa-circle text-info mr-2"></i>
                        <span data-i18n="dashboard.yesterdaycampaign"></span>
                    </div>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-info" role="progressbar" data-transitiongoal="60" style="width: 60%;" aria-valuenow="59"></div>
                    </div>

                    <div class="mb-2">
                        <i class="fa fa-circle text-quaternary mr-2"></i>
                        <span data-i18n="dashboard.weekcampaign"></span>
                    </div>

                    <div class="progress progress-sm">
                        <div class="progress-bar bg-quaternary" role="progressbar" data-transitiongoal="40" style="width: 40%;" aria-valuenow="39"></div>
                    </div>

                    <div class="mb-2">
                        <i class="fa fa-circle text-quinary mr-2"></i>
                        <span data-i18n="dashboard.monthcampaign"></span>
                    </div>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-quinary" role="progressbar" data-transitiongoal="70" style="width: 70%;" aria-valuenow="69"></div>
                    </div>

                    <div class="mb-2">
                        <i class="fa fa-circle text-success mr-2"></i>
                        <span data-i18n="dashboard.yearcampaign"></span>
                    </div>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-success" role="progressbar" data-transitiongoal="50" style="width: 50%;" aria-valuenow="49"></div>
                    </div>
                </div>
            </div>
            <!-- End item -->
        </div> <!-- Begin item -->
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span data-i18n="dashboard.origin"></span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="collapse">
                                <i class="ion ion-minus-round"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="close">
                                <i class="ion ion-android-close"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="reload">
                                <i class="ion ion-android-refresh"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body collapse show">
                <ul class="list-group list-group-flush stats-height">
                    <li class="list-group-item border-top-0">
                        <div class="w-100 row no-gutters">
                            <span class="col-4 list-group-item-title" data-i18n="dashboard.country"></span>
                            <span class="col-4 text-center list-group-item-title" data-i18n="dashboard.percentage"></span>
                            <span class="col-4 text-right list-group-item-title" data-i18n="dashboard.number"></span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="w-100 row no-gutters">
                    <span class="col-4">
                        <img src="assets/img/flags/fr.png" alt="France" class=" mr-3">
                        <span data-i18n="dashboard.france"></span>
                    </span>
                            <span class="col-4 text-center">20.2%</span>
                            <span class="col-4 text-right">202</span>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="w-100 row no-gutters">
                    <span class="col-4">
                        <img src="assets/img/flags/de.png" alt="Germany" class=" mr-3">
                        <span data-i18n="dashboard.germany"></span>
                    </span>
                            <span class="col-4 text-center">0.5%</span>
                            <span class="col-4 text-right">4</span>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="w-100 row no-gutters">
                    <span class="col-4">
                        <img src="assets/img/flags/se.png" alt="Sweden" class=" mr-3">
                        <span data-i18n="dashboard.sweden"></span>
                    </span>
                            <span class="col-4 text-center">60.5%</span>
                            <span class="col-4 text-right">1278</span>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="w-100 row no-gutters">
                    <span class="col-4">
                        <img src="assets/img/flags/nl.png" alt="Holland" class=" mr-3">
                        <span data-i18n="dashboard.holland"></span>
                    </span>
                            <span class="col-4 text-center">8.2%</span>
                            <span class="col-4 text-right">86</span>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" class="w-100 row no-gutters">
                    <span class="col-4">
                        <img src="assets/img/flags/us.png" alt="Holland" class=" mr-3">
                        <span data-i18n="dashboard.usa"></span>
                    </span>
                            <span class="col-4 text-center">12%</span>
                            <span class="col-4 text-right">122</span>
                        </a>
                    </li>
                </ul>
                <div class="d-flex mt-5">
                    <span class="text-uppercase" data-i18n="dashboard.time"></span>
                    <div class="ml-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#" title="Day" data-i18n="dashboard.day"></a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">
                                <a href="#" title="Week" data-i18n="dashboard.week"></a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">
                                <a href="#" title="Month" data-i18n="dashboard.month"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End item --> <!-- Begin item -->
        <div class="x-card">
            <div class="x-card-header">
                <div class="x-card-title h4">
                    <span data-i18n="dashboard.activity"></span>
                </div>
                <a class="heading-elements-toggle">
                    <i class="ion size-18 ion-ios-more"></i>
                </a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a data-action="close">
                                <i class="ion ion-android-close"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="reload">
                                <i class="ion ion-android-refresh"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a data-action="expand">
                                <i class="ion ion-android-expand"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="x-card-body d-flex flex-column text-dark">
                <ul class="list-group list-group-flush mt-0 vh-50">
                    <li class="list-group-item">
                        <div class="media">
                            <span class="d-flex mr-3">
                                <span class="numberCircle bg-warning p-3">
                                    <i class="ion ion-ios-people size-24 mt-0 text-white"></i>
                                </span>
                            </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.filesuploaded"></span>
                                </p>
                                <span class="font-weight-light small">
                                    15 mns ago
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="media">
                                <span class="d-flex mr-3">
                                        <span class="numberCircle bg-primary p-3">
                                            <i class="ion ion-erlenmeyer-flask size-24 text-white"></i>
                                        </span>
                                    </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.chatbotinit"></span>
                                </p>
                                <span class="font-weight-light small">
                                    21 mns ago
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="media">
                                <span class="d-flex mr-3">
                                        <span class="numberCircle bg-quaternary p-3">
                                            <i class="ion ion-eye size-24 text-white"></i>
                                        </span>
                                    </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.serverfailed"></span>
                                </p>
                                <span class="font-weight-light small">
                                    35 mns ago
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="media">
                                <span class="d-flex mr-3">
                                        <span class="numberCircle bg-success p-3">
                                            <i class="ion ion-flash size-24 text-white"></i>
                                        </span>
                                    </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.ticketswaiting"></span>
                                </p>
                                <span class="font-weight-light small">
                                    47 mns ago
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="media">
                                <span class="d-flex mr-3">
                                        <span class="numberCircle bg-info p-3">
                                            <i class="ion ion-funnel size-24 text-white"></i>
                                        </span>
                                    </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.newfiles"></span>
                                </p>
                                <span class="font-weight-light small">
                                    1 hour ago
                                </span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="media">
                                <span class="d-flex mr-3">
                                        <span class="numberCircle bg-danger p-3">
                                            <i class="ion ion-hammer size-24 text-white"></i>
                                        </span>
                                    </span>
                            <div class="media-body my-auto">
                                <p class="my-0">
                                    <span class="font-weight-bold" data-i18n="header.newproduct"></span>
                                </p>
                                <span class="font-weight-light small">
                                    1 day ago
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- End item -->
    </div>
    <!-- End item -->
</div>
@endsection
@push('js')
    <script>

    </script>
@endpush
