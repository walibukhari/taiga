@extends('layouts.auth_app')
@push('css')
@endpush

@section('content')
    <div class="col-md-4 col-lg-7 col-xl-8">
        <div class="ie-cover-fit overflow-hidden d-none d-md-flex">
            <img src="/assets/img/1920x1920.png" srcset="/assets/img/1920x1920.png 320w, /assets/img/1920x1920.png 640w, /assets/img/1920x1920.png 1024w, img/1920x1920.png 1920w"
                 sizes="(min-width: 768px) 100vh" class="img-vh-100" alt="login form">
        </div>
    </div>
    <div class="col-md-8  col-lg-5 col-xl-4">
        <div class="w-100 vh-100 d-flex flex-column bg-white">
            <div class="p-5 my-auto">
                <div class="text-center mb-4">
                    <a class="app-header-brand my-auto mx-auto" href="index.html">
                        <img src="/assets/img/logo-desktop-light.png" class="img-fluid" alt="Themacart premium template">
                    </a>
                </div>
                <div class="text-center mb-5">
                    <h4 class="font-weight-bold text-dark">Please Register, or
                        <a href="login.html" class="text-primary">
                            <u>Sign In</u>
                        </a>
                    </h4>
                </div>
                <form id="main-contact-form" name="contact-form" method="post" action="#">
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">
                                <i class="fa fa-user-o"></i>
                            </div>
                        </div>
                        <input type="text" name="firstname" class="form-control" required="required" placeholder="Your First Name">
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">
                                <i class="fa fa-user-o"></i>
                            </div>
                        </div>
                        <input type="text" name="lastname" class="form-control" required="required" placeholder="Your Last Name">
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">
                                <i class="fa fa-user-o"></i>
                            </div>
                        </div>
                        <input type="text" name="username" class="form-control" required="required" placeholder="Your User Name">
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">@</div>
                        </div>
                        <input type="email" name="email" class="form-control" required="required" placeholder="Your Email">
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">@</div>
                        </div>
                        <input type="email" name="confirmemail" class="form-control" required="required" placeholder="Confirm Your Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block shadow-3 mt-4">Register</button>
                    </div>
                </form>
                <!--/#register-form-->
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
