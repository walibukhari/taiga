<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    fonts link start here-->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <!--    fonts link end here-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('doctor_patient/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/signup.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/media.css')}}">
    <title>Dashboard</title>
</head>

<body>


<!-- =========================
sign up start here
============================= -->
<section class="signup">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-6 d-flex flex-column order-2 order-md-1">
                <div class="signup-logo-single-item">
                    <div class="signup-logo order-md=">
                        <img src="{{asset('doctor_patient/image/signup-logo.png')}}" class="img-fluid" alt="logo">
                        <p>Already a member?
                            <a href="{{route('doctorLoginForm')}}">Sign in now</a></p>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-10 offset-lg-2 col-md-12">
                        <div class="singup-form-single-item">
                            <h1 class="signup-main-title">Sign Up</h1>
                            <form action="{{route('doctorRegister')}}" method="POST">
                                @csrf
                                <!-- form single item  -->
                                <div class="form-group custom-form-group ">
                                    <label for="fullName " class="custom-label">Full Name</label>
                                    <input type="text" name="full_name" class="form-control custom-form-control" id="fullName" placeholder="Full Name" required>
                                </div>

                                <!-- form single item end here  -->


                                <div class="form-group custom-form-group">
                                    <label for="inputEmail " class="custom-label">Email Address</label>
                                    <input type="email" name="email" class="form-control custom-form-control" id="inputEmail" placeholder="Enter Your Email" required>
                                </div>
                                <div class="form-group custom-form-group">
                                    <label for="inputPass" class="custom-label">Password</label>
                                    <input type="password" name="password" class="form-control custom-form-control" id="inputPass" placeholder="Enter Password">
                                </div>


                                <div class="form-group custom-form-group">
                                    <div class="form-check custom-form-check">
                                        <input class="form-check-input" name="terms_check" type="checkbox" id="gridCheck">
                                        <label class="form-check-label" for="gridCheck">
                                            I agree to the <span><a >Terms</a> </span> and <a>Privacy Policy</a>
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn  btn-lg btn-block  custom-signup-btn">CALL NOW</button>
                            </form>

                            <div class="continue-s">
                                <p>Or Continue With</p>
                            </div>

                            <div class="signup-btn">
                                <a href="#" class="btn google-btn"> <span><i class="fab fa-google-plus-g"></i> </span>Google</a>
                                <a href="#" class="btn facebook-btn"> <span><i class="fab fa-facebook-f"></i></span>Facebook</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- another column start here  -->
            <div class="col-md-6 order-1 order-md-2" style="padding: 0;">
                <div class="single-item-signup">
                    <div class="signup-title-right">
                        <h1>Join the largest Designer Community in the world</h1>
                    </div>
                </div>
            </div>
            <!-- another column end here  -->

        </div>
    </div>
</section>
<!-- =========================
sign up start here
============================= -->


<!-- jquery plugins start here  -->
<script src="{{asset('doctor_patient/jquery.filterizr.min.js')}}"></script>
<script src="{{asset('doctor_patient/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('doctor_patient/bootstrap.min.js')}}"></script>
<script src="{{asset('doctor_patient/script.js')}}"></script>
</body>

</html>
