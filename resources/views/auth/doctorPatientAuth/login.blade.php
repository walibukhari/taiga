<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    fonts link start here-->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <!--    fonts link end here-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('doctor_patient/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/signup.css')}}">
    <link rel="stylesheet" href="{{asset('doctor_patient/media.css')}}">
    <title>Dashboard</title>
</head>

<body>


<!-- =========================
sign up start here
============================= -->
<section class="signup">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-6 d-flex flex-column order-2 order-md-1">
                <div class="signup-logo-single-item">
                    <div class="signup-logo order-md=">
                        <img src="{{asset('doctor_patient/image/signup-logo.png')}}" class="img-fluid" alt="logo">
                        <p>Not a member?
                            <a href="{{route('patientSignUpForm')}}">Sign up now</a></p>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-10 offset-lg-2 col-md-12">
                        <div class="singup-form-single-item">
                            <h1 class="signup-main-title signin-main-title">Sign in</h1>
                            <form action="{{route('userLoginPost')}}" method="POST">
                                @csrf


                                <div class="form-group custom-form-group">
                                    <label for="inputEmail" class="custom-label">Email Address</label>
                                    <input type="Email" class="form-control custom-form-control" name="email" id="inputEmail" placeholder="Enter Your Email" required>
                                </div>
                                <div class="form-group custom-form-group">
                                    <label for="inputPass" class="custom-label">Password</label>
                                    <input type="password" name="password" class="form-control custom-form-control" id="inputPass" placeholder="Enter Password">
                                </div>


                                <div class="form-group custom-form-group">
                                    <div class="form-check custom-form-check-sign-in">
                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                        <label class="form-check-label" for="gridCheck">
                                            Remember me
                                        </label>
                                        <div class="forgot-pass">
                                            <a href="#">Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn  btn-lg btn-block  custom-signup-btn custom-login-btn">SIGN IN</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <!-- another column start here  -->
            <div class="col-md-6 order-1 order-md-2 mx-auto" style="padding: 0;">
                <div id="single-item-signup" class="single-item-signup signin-single-item">
                    <div class="signup-title-right signin-title-right">
                        <h1>Join the largest Designer Community in the world</h1>
                    </div>
                </div>
            </div>
            <!-- another column end here  -->

        </div>
    </div>
</section>
<!-- =========================
sign up start here
============================= -->


<!-- jquery plugins start here  -->
<script src="{{asset('doctor_patient/jquery.filterizr.min.js')}}"></script>
<script src="{{asset('doctor_patient/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('doctor_patient/bootstrap.min.js')}}"></script>
<script src="{{asset('doctor_patient/script.js')}}"></script>
<script>
    $(document).ready(function () {
        var height = $(window).height();
        $('#single-item-signup').css('height',height);
    });
</script>
</body>

</html>
