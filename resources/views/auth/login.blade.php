@extends('layouts.auth_app')
@push('css')
@endpush
@section('content')
    <div class="col-md-4 col-lg-7 col-xl-8">
        <div class="ie-cover-fit overflow-hidden d-none d-md-flex">
            <img src="https://we-care.co.za/wp-content/uploads/2019/04/Are-EPharmacies-on-the-cards-for-SA.jpg" srcset="https://we-care.co.za/wp-content/uploads/2019/04/Are-EPharmacies-on-the-cards-for-SA.jpg 320w, https://we-care.co.za/wp-content/uploads/2019/04/Are-EPharmacies-on-the-cards-for-SA.jpg 640w, https://we-care.co.za/wp-content/uploads/2019/04/Are-EPharmacies-on-the-cards-for-SA.jpg 1024w, https://we-care.co.za/wp-content/uploads/2019/04/Are-EPharmacies-on-the-cards-for-SA.jpg 1920w"
                 sizes="(min-width: 768px) 100vh" class="img-vh-100" alt="login form">
        </div>
    </div>
    <div class="col-md-8  col-lg-5 col-xl-4">
        <div class="w-100 vh-100 d-flex flex-column bg-white">
            <div class="p-5 my-auto">
                <div class="text-center mb-4">
                    <a class="app-header-brand my-auto mx-auto" href="index.html">
                        <img src="{{url('/')}}/assets/img/logo-desktop-light.png" class="img-fluid" alt="Themacart premium template">
                    </a>
                </div>
                <form method="POST" action="{{ route('loginPost') }}">
                    @csrf
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">@</div>
                        </div>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" required="required" placeholder="Your Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <div class="input-group-text bg-white">@</div>
                        </div>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" required="required" placeholder="Password">

                        @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block shadow-3 mt-4">Login</button>
                    </div>
                </form>
                <!--/#register-form-->
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush

