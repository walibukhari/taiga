<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('password');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name');
            $table->string('year_of_birth');
            $table->string('month_of_birth');
            $table->string('day_of_birth');
            $table->string('cell_phone_number');
            $table->string('address');
            $table->string('zip_code');
            $table->string('health_care_number');
            $table->string('gender');
            $table->string('pharmacy_name');
            $table->string('pharmacy_fax_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
