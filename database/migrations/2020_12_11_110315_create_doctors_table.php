<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('password');
            $table->string('clinic_name');
            $table->string('country');
            $table->string('state')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('cell_phone');
            $table->string('clinic_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
