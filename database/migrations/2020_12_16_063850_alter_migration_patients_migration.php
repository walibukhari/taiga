<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMigrationPatientsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('country')->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('middle_name')->nullable()->change();
            $table->string('year_of_birth')->nullable()->change();
            $table->string('month_of_birth')->nullable()->change();
            $table->string('day_of_birth')->nullable()->change();
            $table->string('cell_phone_number')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('zip_code')->nullable()->change();
            $table->string('health_care_number')->nullable()->change();
            $table->string('gender')->nullable()->change();
            $table->string('pharmacy_name')->nullable()->change();
            $table->string('pharmacy_fax_number')->nullable()->change();
            $table->string('full_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            //
        });
    }
}
