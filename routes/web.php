<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page');
});

Auth::routes();


Route::get('login',[App\Http\Controllers\AuthController::class, 'userLoginForm'])->name('userLoginForm');
Route::post('login-post',[App\Http\Controllers\AuthController::class, 'userLoginPost'])->name('userLoginPost');
Route::post('user-logout',[App\Http\Controllers\AuthController::class, 'userLogout'])->name('userLogout');


Route::get('super-admin/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('showLoginFormL');

Route::prefix('super-admin')->middleware('auth')->group(function () {

    /* doctor routes */
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/create-doctor', [App\Http\Controllers\SuperAdmin\DoctorController::class, 'CreateDoctor'])->name('CreateDoctor');
    Route::get('/all-doctors', [App\Http\Controllers\SuperAdmin\DoctorController::class, 'allDoctor'])->name('AllDoctor');
    Route::post('/save-doctors', [App\Http\Controllers\SuperAdmin\DoctorController::class, 'postDoctor'])->name('postDoctor');
    Route::get('/doctor-detail/{id}', [App\Http\Controllers\SuperAdmin\DoctorController::class, 'doctorDetail'])->name('doctorDetail');
    Route::get('/delete-doctor/{id}', [App\Http\Controllers\SuperAdmin\DoctorController::class, 'doctorDelete'])->name('doctorDelete');

    /* Patient routes */
    Route::get('/create-Patient', [App\Http\Controllers\SuperAdmin\PaitentController::class, 'CreatePatient'])->name('CreatePatient');
    Route::post('/save-Patient', [App\Http\Controllers\SuperAdmin\PaitentController::class, 'postPatients'])->name('postPatients');
    Route::get('/all-patients', [App\Http\Controllers\SuperAdmin\PaitentController::class, 'AllPatients'])->name('AllPatients');
    Route::get('/delete-Patient/{id}', [App\Http\Controllers\SuperAdmin\PaitentController::class, 'deletePatients'])->name('deletePatients');
    Route::get('/Patient-details/{id}', [App\Http\Controllers\SuperAdmin\PaitentController::class, 'detailPatients'])->name('detailPatients');


    // pages routes
    Route::get('/create-new-page', [App\Http\Controllers\HomeController::class, 'CreatePage'])->name('create-new-page');
    Route::post('/create-new-page-post', [App\Http\Controllers\HomeController::class, 'CreatePagePost'])->name('create-new-page-post');

    Route::get('/all-pages', [App\Http\Controllers\HomeController::class, 'AllPages'])->name('all-page');





    /* invoices routes */
    Route::get('/create-invoice', [App\Http\Controllers\HomeController::class, 'Createinvoice'])->name('Createinvoice');
    Route::get('/all-invoices', [App\Http\Controllers\HomeController::class, 'Allinvoices'])->name('Allinvoices');
    Route::get('/invoice-detail/{ID}', [App\Http\Controllers\HomeController::class, 'InvoiceDetail'])->name('InvoiceDetail');

    /* Questionnaire routes */
    Route::get('surveys/show-surveys',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'index'])->name('questionnaire_index');
    Route::post('add-folder',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'addFolder'])->name('addFolder');
    Route::get('add-questionnaire/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'postQuestionnaire'])->name('postQuestionnaire');
    Route::get('add-folder-questionnaire/{folder_id}/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'postFolderQuestionnaire'])->name('postFolderQuestionnaire');
    Route::post('save-questionnaire',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'postAddQuestionnaire'])->name('postAddQuestionnaire');
    Route::get('edit-questionnaire/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'editQuestionnaire'])->name('editQuestionnaire');
    Route::post('update-questionnaire/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'updateQuestionnaire'])->name('updateQuestionnaire');
    Route::post('add-folder-inside-folder',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'addFolderInsideFolder'])->name('addFolderInsideFolder');
    Route::get('get-folder-data/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'getFolderData'])->name('getFolderData');
    Route::post('rename-folder',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'renameFolder'])->name('renameFolder');
    Route::get('delete-folder/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'deleteFolder'])->name('deleteFolder');
    Route::get('tree-data',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'treeData'])->name('treeData');
    Route::get('get-previous-id/{id}',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'getPreviousId'])->name('getPreviousId');
    Route::post('duplicate-questionnaire',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'duplicateQuestionnaire'])->name('duplicateQuestionnaire');
    Route::post('perform-action-questionnaire',[App\Http\Controllers\SuperAdmin\QuestionnairesController::class, 'actionPerformQuestionnaire'])->name('actionPerformQuestionnaire');


    // settings routes
    Route::get('seo-details',[App\Http\Controllers\HomeController::class, 'seoDetails'])->name('seo-details');
    Route::post('seo-details-post',[App\Http\Controllers\HomeController::class, 'seopost'])->name('seo-details-post');


});

Route::post('super-admin/login-post', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('loginPost');
Route::get('super-admin/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logoutT');
Route::get('Super-Admin/Doctors', [App\Http\Controllers\AuthController::class, 'Doctors'])->name('superadmin-Doctors');
Route::get('Super-Admin/Patient', [App\Http\Controllers\AuthController::class, 'Patient'])->name('superadmin-Patient');
