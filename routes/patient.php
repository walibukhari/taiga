<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Patient's Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Patient's routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('patient-login',[App\Http\Controllers\Patient\PatientLoginController::class, 'patientLoginForm'])->name('patientLoginForm');
//Route::post('patient-login-post',[App\Http\Controllers\Patient\PatientLoginController::class, 'patientLogin'])->name('patientLogin');
Route::get('patient-signup',[App\Http\Controllers\Patient\PatientRegisterController::class, 'patientSignUpForm'])->name('patientSignUpForm');
Route::post('patient-register-post',[App\Http\Controllers\Patient\PatientRegisterController::class, 'patientRegister'])->name('patientRegister');
//Route::get('patient-logout',[App\Http\Controllers\Patient\PatientLoginController::class, 'logoutPatient'])->name('logoutPatient');

Route::prefix('patient')->middleware('patient_middleware')->group(function () {
    Route::get('/dashboard', [App\Http\Controllers\Patient\PatientController::class, 'index'])->name('patientHome');
});
