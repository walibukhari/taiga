<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Doctor's Routes
|--------------------------------------------------------------------------
|
| Here is where you can register doctor's routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('doctor-login',[App\Http\Controllers\Doctor\DoctorLoginController::class, 'doctorLoginForm'])->name('doctorLoginForm');
//Route::post('doctor-login-post',[App\Http\Controllers\Doctor\DoctorLoginController::class, 'doctorLogin'])->name('doctorLogin');
Route::get('doctor-signup',[App\Http\Controllers\Doctor\DoctorLoginController::class, 'doctorSignUpForm'])->name('doctorSignUpForm');
Route::post('doctor-register-post',[App\Http\Controllers\Doctor\DoctorRegisterController::class, 'doctorRegister'])->name('doctorRegister');
//Route::get('doctor-logout',[App\Http\Controllers\Doctor\DoctorLoginController::class, 'logoutDoctor'])->name('logoutDoctor');

Route::prefix('doctor')->middleware('doctor_middleware')->group(function () {
    Route::get('/dashboard', [App\Http\Controllers\Doctor\DoctorController::class, 'index'])->name('doctorHome');
    Route::get('/patient-list', [App\Http\Controllers\Doctor\DoctorController::class, 'patientList'])->name('patientList');
    Route::get('/profile', [App\Http\Controllers\Doctor\DoctorController::class, 'profile'])->name('profile');
});
