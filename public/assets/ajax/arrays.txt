{
  "data": [
    [
      "Tiger Nixon",
      "System Architect",
      "Edinburgh",
      "5421",
      "2008-11-28T00:00:00+01:00",
      "320800"
    ],
    [
      "Garrett Winters",
      "Accountant",
      "Tokyo",
      "8422",
      "2009-01-12T00:00:00+01:00",
      "170750"
    ],
    [
      "Ashton Cox",
      "Junior Technical Author",
      "San Francisco",
      "1562",
      "2012-12-02T00:00:00+01:00",
      "86000"
    ],
    [
      "Cedric Kelly",
      "Senior Javascript Developer",
      "Edinburgh",
      "6224",
      "2012-03-29T00:00:00+02:00",
      "433060"
    ],
    [
      "Airi Satou",
      "Accountant",
      "Tokyo",
      "5407",
      "2008-10-16T00:00:00+02:00",
      "162700"
    ],
    [
      "Brielle Williamson",
      "Integration Specialist",
      "New York",
      "4804",
      "2009-09-15T00:00:00+02:00",
      "372000"
    ],
    [
      "Herrod Chandler",
      "Sales Assistant",
      "San Francisco",
      "9608",
      "2011-07-25T00:00:00+02:00",
      "137500"
    ],
    [
      "Rhona Davidson",
      "Integration Specialist",
      "Tokyo",
      "6200",
      "2012-12-18T00:00:00+01:00",
      "327900"
    ],
    [
      "Colleen Hurst",
      "Javascript Developer",
      "San Francisco",
      "2360",
      "2012-08-06T00:00:00+02:00",
      "205500"
    ],
    [
      "Sonya Frost",
      "Software Engineer",
      "Edinburgh",
      "1667",
      "2008-12-19T00:00:00+01:00",
      "103600"
    ],
    [
      "Jena Gaines",
      "Office Manager",
      "London",
      "3814",
      "2013-03-03T00:00:00+01:00",
      "90560"
    ],
    [
      "Quinn Flynn",
      "Support Lead",
      "Edinburgh",
      "9497",
      "2010-10-14T00:00:00+02:00",
      "342000"
    ],
    [
      "Charde Marshall",
      "Regional Director",
      "San Francisco",
      "6741",
      "2008-12-13T00:00:00+01:00",
      "470600"
    ],
    [
      "Haley Kennedy",
      "Senior Marketing Designer",
      "London",
      "3597",
      "2010-03-17T00:00:00+01:00",
      "313500"
    ],
    [
      "Tatyana Fitzpatrick",
      "Regional Director",
      "London",
      "1965",
      "2011-04-25T00:00:00+02:00",
      "385750"
    ]
  ]
}
