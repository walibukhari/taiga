/**
 * Initialize datatable object.
 */
var table;
var initialLocaleCode = 'fr';
var initialTimezone ='local';
var _DEBUG_ = false;

/**
 * Initialize popover and tooltip functions.
 */
function initBootstrapFunctions() {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
}

/**
 * Initialize Left sidebar toggle effect and treeview
 */
function initSidebar() {

    var treeviewMenu = $('.sidebar-menu');
    var alreadyToggle = false;

    // Toggle Sidebar
    $('[data-toggle="sidebar"]').click(function (event) {
        event.preventDefault();
        $('.app').toggleClass('sidenav-toggled');
  });

    // Activate sidebar treeview toggle
    $("[data-toggle='treeview']").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass('is-expanded')) {
            treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
        }
        $(this).parent().toggleClass('is-expanded');
    });

    // Set initial active toggle
    $("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

    $('[data-toggle="collapsesidebar"]').on('click', function () {
        $('.collapse-sidebar').toggleClass('open');
    });

    $('[data-toggle="collapsemessages"]').on('click', function () {
        $('.collapse-sidebar').toggleClass('open');
        $('#myTab a[href="#messages"]').tab('show'); // Select tab by name
    });

    $('[data-toggle="collapsenotifications"]').on('click', function () {
        $('.collapse-sidebar').toggleClass('open');
        $('#myTab a[href="#notifications"]').tab('show'); // Select tab by name
    });

    $('[data-toggle="collapsesettings"]').on('click', function () {
        $('.collapse-sidebar').toggleClass('open').delay(1000);
        $('#myTab a[href="#settings"]').tab('show'); // Select tab by name
    });
}


function initChangeSidebarBackground() {
     $('.changeSidebarBackground').click(function() {
         var color = $(this).attr('data-background');
        $('aside[class*="left-sidebar-"]').attr('class', function(index, attr) {
        //Return the updated string, being sure to only replace z- at the start of a class name.
        return attr.replace(/left-sidebar-(.*)?/g, 'left-sidebar-'+color);
    });
     });
}

function initChangeHeaderBackground() {
     $('.changeHeaderBackground').click(function() {
         var color = $(this).attr('data-background');
        $('header[class*="app-header-"]').attr('class', function(index, attr) {
        //Return the updated string, being sure to only replace z- at the start of a class name.
        return attr.replace(/app-header-(.*)?/g, 'app-header-'+color);
    });
        if(color == "white") {
            $("#desktopimage").attr("src","./img/logo-desktop-light.png");
            $("#mobileimage").attr("src","./img/logo-mobile-light.png");
        }
        else {
            $("#desktopimage").attr("src","./img/logo-desktop-dark.png");
            $("#mobileimage").attr("src","./img/logo-mobile-dark.png");
        }

     });
}


function initToggleSidebarMini() {
     $("[data-toggle='toggleSidebarMini']").click(function() {
         $('body').toggleClass("sidebar-mini");
     });
}

/**
 * Initialize collapse plus minus icons toogle
 */
function initCollapseIcon() {
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    });
}

/**
 * Initialize chat left scrollbar
 */
function initChatScrollBar() {
	$("#leftChatUser").mCustomScrollbar({
        autoHideScrollbar:true,
        theme:"dark-thin"
    });
}

/**
 * Initialize chat left scrollbar
 */
function initLeftSidebarScrollBar() {
	$("#leftSidebar").mCustomScrollbar({
        autoHideScrollbar:true,
        theme:"minimal-dark",
        scrollInertia: 300
    });
}

/**
 * Initialize chat left scrollbar
 */
function initXCardAction() {

        // Collapsible Card
        $('a[data-action="collapse"]').on('click',function(e){
            e.preventDefault();
            $(this).closest('.x-card').children('.x-card-body').collapse('toggle');
            $(this).closest('.x-card').find('[data-action="collapse"] i').toggleClass('ion-minus-round ion-plus-round');

        });

        // Toggle fullscreen
        $('a[data-action="expand"]').on('click',function(e){
            e.preventDefault();
            $(this).closest('.x-card').find('[data-action="expand"] i').toggleClass('icon-expand2 icon-contract');
            $(this).closest('.x-card').toggleClass('x-card-fullscreen');
        });

        // Reload Card
        $('a[data-action="reload"]').on('click',function(){
            var block_ele = $(this).closest('.x-card');

            // Block Element
            block_ele.block({
                message: '<div class="icon-spin"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw text-primary"></i></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    cursor: 'wait',
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        });

        // Close Card
        $('a[data-action="close"]').on('click',function(){
            $(this).closest('.x-card').removeClass().slideUp('fast');
        });

        // Match the height of each card in a row
        setTimeout(function(){
            $('.row.match-height').each(function() {
                $(this).find('.x-card').not('.x-card .x-card').matchHeight(); // Not .card .card prevents collapsible cards from taking height
            });
        },500);


        $('.x-card .heading-elements a[data-action="collapse"]').on('click',function(){
            var $this = $(this),
            card = $this.closest('.x-card');
            var cardHeight;

            if(parseInt(card[0].style.height,10) > 0){
                cardHeight = card.css('height');
                card.css('height','').attr('data-height', cardHeight);
            }
            else{
                if(card.data('height')){
                    cardHeight = card.data('height');
                    card.css('height',cardHeight).attr('data-height', '');
                }
            }
        });

        //card heading actions buttons small screen support
        $(".heading-elements-toggle").on("click",function(){
            $(this).parent().children(".heading-elements").toggleClass("visible");
        });
}



/**
 * Give flags name from lng code (ex: en-GB -> gb)
 * @param {string} Lang from i18next library.
 * @return {string} Flag name.
 */
function getFlagName(lng) {
    var res = lng.split("-");
    if (res.length == 2)
        return res[1];
    else
        return res;
}

/**
 * Give first symbol from lng code (ex: en-GB -> en)
 * @param {string} Lang from i18next library.
 * @return {string} Calendar value.
 */
function getCalendarName(lng) {
    var res = lng.split("-");
    if (res.length == 2)
        return res[0];
    else
        return res;
}

/**
 * Initialize Ajax Table example
 * @param {string} Lang from i18next library.
 */
function initAjaxTable(lng) {
   table = $('#ajax_source_example').DataTable({


        "deferRender": true,


    });
}

/**
 * Destroy and recreate Ajax demo Table with lang param.
 * @param {string} Lang from 18next library.
 */
function translateAjaxTable(lang) {

   table = $('#ajax_source_example').dataTable({
        "language": {
            "url": "http://127.0.0.1:8000/assets/ajax/translation." + lang + ".json"
        },
        destroy: true,
        empty: true,
        /**
         * Initialize Ajax Table example
         * @param {string} Lang from i18next library.
         */
        "ajax": "http://127.0.0.1:8000/assets/ajax/arrays.txt",
        "deferRender": true,
        columns: [{
                data: 0
            },
            {
                data: 1
            },
            {
                data: 2
            },
            {
                data: 3
            },
            {
                data: 4,
                render: function (data, type, row) {
                    if (type === 'display')
                        return moment(data).format('LL');
                    else
                        return data;
                }
            },
            {
                data: 5,
                render: function (data, type, row) {
                    if (type === 'display')
                        return new Intl.NumberFormat(i18next.language, {
                            style: 'currency',
                            currency: 'EUR'
                        }).format(data);
                    else
                        return data;
                }
            },
            {
                data: null,
                defaultContent: `<a href="#" class="btn btn-primary btn-sm btn-pill text-white  delete-user">${i18next.t('table.delete')}</a>`,
                orderable: false
            }
        ]
    });
}

/**
 * Populate color for fragable elements.
 * @return {element} Populate element.
 */
function initDragableEvents() {
   $('#external-events .fc-event').each(function () {
      // store data so the calendar knows to render an event upon drop
      $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        color: $(this).attr('data-color'),
        stick: true // maintain when user navigates (see docs on the renderEvent method)
      });
      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });
    });
}

/**
 * Initialize dropzone and fake download.
 */
function initDropZone() {
         if (document.getElementById('preview-template') != null) {
     /*
      * This is durty hack to simulate an upload server. Remove javascript to your code-->
      **/
      $('#preview-template').html('<div class="dz-preview dz-file-preview">'
        +'<div class="dz-image">'
        +'<img data-dz-thumbnail src="/" alt="thumbnail"/>'
        +'</div>'
        +'<div class="dz-details">'
        +'<div class="dz-size">'
        +'<span data-dz-size></span>'
        +'</div>'
        +'<div class="dz-filename">'
        +'<span data-dz-name></span>'
        +'</div></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span>'
        +'</div><div class="dz-error-message"><span data-dz-errormessage></span></div>'
        +'<div class="dz-success-mark"><svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
        +'<title>Check</title><desc>Created with Sketch.</desc><defs></defs>'
        +'<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage"><path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>'
        +'</g></svg></div> <div class="dz-error-mark"><svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>error</title><desc>Created with Sketch.</desc><defs></defs>'
         +'<g id="Page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage"><g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475"><path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>'
         +'</g></g></svg></div></div> ');

 Dropzone.autoDiscover = false;

 var dropzone = new Dropzone('.dropzone', {
    previewTemplate: document.querySelector('#preview-template').innerHTML,
    parallelUploads: 2,
    thumbnailHeight: 120,
    thumbnailWidth: 120,
    maxFilesize: 3,
    filesizeBase: 1000,
    thumbnail: function(file, dataUrl) {
      if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
          var thumbnailElement = images[i];
          thumbnailElement.alt = file.name;
          thumbnailElement.src = dataUrl;
        }
        setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
      }
    }

  });


  // Now fake the file upload, since GitHub does not handle file uploads
  // and returns a 404

  var minSteps = 6,
      maxSteps = 60,
      timeBetweenSteps = 100,
      bytesPerStep = 100000;
    var totalSteps;

  dropzone.uploadFiles = function(files) {
    var self = this;

    for (var i = 0; i < files.length; i++) {

      var file = files[i];
          totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

      for (var step = 0; step < totalSteps; step++) {
        var duration = timeBetweenSteps * (step + 1);
        setTimeout(function(file, totalSteps, step) {
          return function() {
            file.upload = {
              progress: 100 * (step + 1) / totalSteps,
              total: file.size,
              bytesSent: (step + 1) * file.size / totalSteps
            };

            self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
            if (file.upload.progress == 100) {
              file.status = Dropzone.SUCCESS;
              self.emit("success", file, 'success', null);
              self.emit("complete", file);
              self.processQueue();
            }
          };
        }(file, totalSteps, step), duration);
      }
    }
  };
         }
}


/**
 * Initialize Calendar element.
 * @return {element} Populate element.
 */
function initCalendar(lng) {
    $('#calendar').fullCalendar({
      themeSystem: 'bootstrap4',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
        locale: lng,
        timezone: initialTimezone,
    buttonIcons: false, // show the prev/next text
    weekNumbers: false,
    eventLimit: true, // allow "more" link when too many events
      defaultDate: '2018-03-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      selectable: true,
      droppable: true, // this allows things to be dropped onto the calendar
      height: "auto",
      drop: function () {
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
      },
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: 'ajax/events.json',
        error: function () {
          swal(
            'Failed to load Data',
            'You need to install php/json files',
            'question'
          );
        }
      },
      loading: function (bool) {
        $('#loading').toggle(bool);
      },
      eventRender: function (event, el) {
        // render the timezone offset below the event title
        if (event.start.hasZone()) {
          el.find('.fc-title').after(
            $('<div class="tzo"/>').text(event.start.format('Z'))
          );
        }
        var tooltip = event.title;
        el.attr("data-original-title", tooltip);
        el.tooltip({ container: "body"});
      },
      dayClick: function (date) {
        // for demo purpose only
      //  toastr.info(date.format(), 'dayClick');
      },
      select: function (startDate, endDate, event) {
                // Display the modal.
                // You could fill in the start and end fields based on the parameters
                var modalForm = $('#updateModal');
                modalForm.data.event = event;
                modalForm.data.action = 'create';
                modalForm.modal('show');
                if(moment(startDate).isValid())
                $('#datetimepicker1').datetimepicker('date',  startDate);
                if(moment(endDate).isValid())
                $('#datetimepicker2').datetimepicker('date',  endDate);

      },

      eventClick: function (event, element) {
               // Display the modal and set the values to the event values.
               var modalForm = $('#updateModal');
                modalForm.data.event = event;
                modalForm.data.action = 'update';
                modalForm.modal('show');
                modalForm.find('#nameInputU').val(event.title);
                if(moment(event.start).isValid())
                $('#datetimepicker1').datetimepicker('date',  event.start);
                if(moment(event.end).isValid())
                $('#datetimepicker2').datetimepicker('date',  event.end);
                modalForm.find('#urlU').val(event.url);
                modalForm.find('#descriptionU').val(event.description);
      }
    });
}


/**
 * Initialize Agenda element.
 * @return {element} Populate element.
 */
function initAgenda(lng) {
    $('#agenda').fullCalendar({
        themeSystem: 'bootstrap4',
        defaultView: 'agendaWeek',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        locale: lng,
        timezone: initialTimezone,
        buttonIcons: false, // show the prev/next text
        weekNumbers: false,
        eventLimit: true, // allow "more" link when too many events
        defaultDate: '2018-03-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        selectable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        height: "auto",
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        events: {
            url: 'ajax/events.json',
            error: function () {
                swal(
                    'Failed to load Data',
                    'You need to install php/json files',
                    'question'
                );
            }
        },
        loading: function (bool) {
            $('#loading').toggle(bool);
        },
        eventRender: function (event, el) {
            // render the timezone offset below the event title
            if (event.start.hasZone()) {
                el.find('.fc-title').after(
                    $('<div class="tzo"/>').text(event.start.format('Z'))
                );
            }
            var tooltip = event.title;
            el.attr("data-original-title", tooltip);
            el.tooltip({
                container: "body"
            });
        },
        dayClick: function (date) {
            // for demo purpose only
            //  toastr.info(date.format(), 'dayClick');
        },
        select: function (startDate, endDate, event) {
            // Display the modal.
            // You could fill in the start and end fields based on the parameters
            var modalForm = $('#updateModal');
            modalForm.data.event = event;
            modalForm.data.action = 'create';
            modalForm.modal('show');
            if (moment(startDate).isValid())
                $('#datetimepicker1').datetimepicker('date', startDate);
            if (moment(endDate).isValid())
                $('#datetimepicker2').datetimepicker('date', endDate);

        },

        eventClick: function (event, element) {
            // Display the modal and set the values to the event values.
            var modalForm = $('#updateModal');
            modalForm.data.event = event;
            modalForm.data.action = 'update';
            modalForm.modal('show');
            modalForm.find('#nameInputU').val(event.title);
            if (moment(event.start).isValid())
                $('#datetimepicker1').datetimepicker('date', event.start);
            if (moment(event.end).isValid())
                $('#datetimepicker2').datetimepicker('date', event.end);
            modalForm.find('#urlU').val(event.url);
            modalForm.find('#descriptionU').val(event.description);
        }
    });
}

/**
 * Initialize List View element.
 * @return {element} Populate element.
 */
function initListViewEvents(lng) {
    $('#listViewEvents').fullCalendar({
        themeSystem: 'bootstrap4',
        defaultView: 'listWeek',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        locale: lng,
        timezone: initialTimezone,
        buttonIcons: false, // show the prev/next text
        weekNumbers: false,
        eventLimit: true, // allow "more" link when too many events
        defaultDate: '2018-03-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        selectable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        height: "auto",
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        events: {
            url: 'ajax/events.json',
            error: function () {
                swal(
                    'Failed to load Data',
                    'You need to install php/json files',
                    'question'
                );
            }
        },
        loading: function (bool) {
            $('#loading').toggle(bool);
        },
        eventRender: function (event, el) {
            // render the timezone offset below the event title
            if (event.start.hasZone()) {
                el.find('.fc-title').after(
                    $('<div class="tzo"/>').text(event.start.format('Z'))
                );
            }
            var tooltip = event.title;
            el.attr("data-original-title", tooltip);
            el.tooltip({
                container: "body"
            });
        },
        dayClick: function (date) {
            // for demo purpose only
            //  toastr.info(date.format(), 'dayClick');
        },
        select: function (startDate, endDate, event) {
            // Display the modal.
            // You could fill in the start and end fields based on the parameters
            var modalForm = $('#updateModal');
            modalForm.data.event = event;
            modalForm.data.action = 'create';
            modalForm.modal('show');
            if (moment(startDate).isValid())
                $('#datetimepicker1').datetimepicker('date', startDate);
            if (moment(endDate).isValid())
                $('#datetimepicker2').datetimepicker('date', endDate);

        },

        eventClick: function (event, element) {
            // Display the modal and set the values to the event values.
            var modalForm = $('#updateModal');
            modalForm.data.event = event;
            modalForm.data.action = 'update';
            modalForm.modal('show');
            modalForm.find('#nameInputU').val(event.title);
            if (moment(event.start).isValid())
                $('#datetimepicker1').datetimepicker('date', event.start);
            if (moment(event.end).isValid())
                $('#datetimepicker2').datetimepicker('date', event.end);
            modalForm.find('#urlU').val(event.url);
            modalForm.find('#descriptionU').val(event.description);
        }
    });
}

/**
 * Initialize Google calendar element.
 * @return {element} Populate element.
 */
function initGoogleCalendar(lng) {
    $('#googleCalendar').fullCalendar({
        googleCalendarApiKey: 'YOUR_API_KEY',
        events: {
           // US Holidays
           googleCalendarId: 'en.usa#holiday@group.v.calendar.google.com',
        },
        themeSystem: 'bootstrap4',
        locale: lng,
        timezone: initialTimezone,
        buttonIcons: false, // show the prev/next text
        weekNumbers: false,
        eventLimit: true, // allow "more" link when too many events
        defaultDate: '2018-06-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        selectable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        height: "auto",
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        loading: function (bool) {
            $('#loading').toggle(bool);
        },
        eventRender: function (event, el) {
            // render the timezone offset below the event title
            if (event.start.hasZone()) {
                el.find('.fc-title').after(
                    $('<div class="tzo"/>').text(event.start.format('Z'))
                );
            }
            var tooltip = event.title;
            el.attr("data-original-title", tooltip);
            el.tooltip({
                container: "body"
            });
        },

        eventClick: function (event, element) {
             // opens events in a popup window
            window.open(event.url, '_blank', 'width=700,height=600');
            return false;
        }
      });
}



/**
 * Populate timezone selector with json data (possible php too).
 * @return {element} Populate element.
 */

function getTimezones() {
    // load the list of available timezones, build the <select> options
    $.getJSON('http://127.0.0.1:8000/assets/ajax/get-timezones.json', function (timezones) {
      $.each(timezones, function (i, timezone) {
        if (timezone != 'UTC') { // UTC is already in the list
          $('#timezone-selector').append(
            $("<option/>")
            .text(timezone)
            .attr('value', timezone)
            .prop('selected', timezone == initialTimezone)
          );
        }
      });
    });
}

/**
 * Initialize Datetimepicker elements with i18 language.
 * @param {string} Lang code.
 */
function initDateTimepicker(lng) {
    // Bind the dates to datetimepicker.
    // You should pass the options you need
    $('[data-target-input="nearest"]').datetimepicker({locale: lng});
  //  $("#datetimepicker1, #datetimepicker2").datetimepicker({locale: lng});
}

/**
 * Initialize Timezone switcher.
 * @return {event} Click watcher action.
 */
function initTimezoneSelector() {
    // when the timezone selector changes, dynamically change the calendar option
    $('#timezone-selector').on('change', function () {
      initialTimezone = this.value;
      $('#calendar').fullCalendar('option', 'timezone', initialTimezone || false);
    });
}

/**
 * Initialize create element modal.
 * @return {modal} Element visible.
 */
function createElementModal() {
     $('#createNewEvent').click(function() {
        var event = [];
        var modalForm = $('#updateModal');
        modalForm.data.event = event;
        modalForm.data.action = 'create';
        // Clear modal inputs
        $('.modal').find('input').val('');
        $('#descriptionU').val('');
        $('#labelSelectU').val($('#labelSelectU').find('option:first').val());
        modalForm.modal('show');

    });
}

/**
 * Create element by modal return.
 * @return {event} New element create or update.
 */
function submitModalEvenet() {
    $('#submitModalU').on('click', function (e) {
            var title = $('#nameInputU').val();

            if (title) {

                var event = $('#updateModal').data.event;

                    event.title = $("#nameInputU").val();
                    if(moment($('#datetimepicker1').datetimepicker('date')).isValid())
                    event.start = moment($('#datetimepicker1').datetimepicker('date'));
                    if(moment($('#datetimepicker2').datetimepicker('date')).isValid())
                    event.end = moment($('#datetimepicker2').datetimepicker('date'));
                    event.color = ns.getEventColor($("#labelSelectU").val());
                    event.description = $("#descriptionU").val();
                    event.url = $("#urlU").val();

               if($('#updateModal').data.action === 'update')
                $('#calendar').fullCalendar('updateEvent', event, true); // stick? = true
                else
                $('#calendar').fullCalendar('renderEvent', event, true); // stick? = true
            }
            $('#calendar').fullCalendar('unselect');

            // Clear modal inputs
            $('.modal').find('input').val('');
            $('#descriptionU').val('');
            $('#labelSelectU').val($('#labelSelectU').find('option:first').val());
            // hide modal
            $('.modal').modal('hide');
    });
}

function initDeleteUser() {
    table.on('click', '.delete-user', function () {
        swal({
            title: i18next.t('swal.areyousure'),
            text: i18next.t('swal.youwantbeable'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#448AFF',
            cancelButtonColor: '#d33',
            confirmButtonText: i18next.t('swal.yesdelete')
        }).then((result) => {
            if (result.value) {
                var li = $(this).closest('li');
                if (li.length) {
                    row = table.row(li.data('dt-row'));
                    table
                        .row(li.data('dt-row'))
                        .remove()
                        .draw();
                } else {
                    table
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();
                };

                swal(
                    i18next.t('swal.deleted'),
                    i18next.t('swal.yourclient'),
                    'success'
                );
            }
        })
    });
}

/**
 * Initialize translation system in the template.
 * Initialize multiples Datatables with translation system
 * You need to initialize table here with lng param
 * @return {Document} Document localize.
 */
function initI18Next() {

    i18next.use(i18nextXHRBackend);
    i18next.use(i18nextBrowserLanguageDetector);
    i18next.init({
        'debug': _DEBUG_,
        ns: ['translation'],
        defaultNS: 'translation',
        backend: {
            loadPath: '/assets/ajax/{{ns}}.{{lng}}.json',
            crossDomain: false,
            allowMultiLoading: false
        },
        detection: {
            // order and from where user language should be detected
            order: ['querystring', 'localStorage', 'cookie', 'navigator', 'htmlTag', 'path', 'subdomain'],

            // keys or params to lookup language from
            lookupQuerystring: 'lng',
            lookupCookie: 'i18next',
            lookupLocalStorage: 'i18nextLng',
            lookupFromPathIndex: 0,
            lookupFromSubdomainIndex: 0,

            // cache user language on
            caches: ['localStorage', 'cookie'],
            excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)

            // optional expire and domain for set cookie
            cookieMinutes: 10,
            cookieDomain: 'melvinAdmin',

            // optional htmlTag with lang attribute, the default is:
            htmlTag: document.documentElement
        }
    }, function (err, t) {
        console.warn(i18next.store.data);
        if (err) {
            console.error('error while initializing i18next ::: ' + JSON.stringify(err));
        } // for options see
        // https://github.com/i18next/jquery-i18next#initialize-the-plugin
        jqueryI18next.init(i18next, $, {
            tName: 't', // --> appends $.t = i18next.t
            i18nName: 'i18n', // --> appends $.i18n = i18next
            handleName: 'localize', // --> appends $(selector).localize(opts);
            selectorAttr: 'data-i18n', // selector for translating elements
            targetAttr: 'i18n-target', // data-() attribute to grab target element to translate (if diffrent then itself)
            optionsAttr: 'i18n-options', // data-() attribute that contains options, will load/set if useOptionsAttr = true
            useOptionsAttr: false, // see optionsAttr
            parseDefaultValueFromContent: true // parses default values from content ele.val or ele.text
        });
        // start localizing, details:
        // https://github.com/i18next/jquery-i18next#usage-of-selector-function
        $(document).localize();

            if (document.getElementById('langFlag') != null) {
        document.getElementById('langFlag').innerHTML = `<img src=\"/assets/img/flags/${getFlagName(i18next.language)}.png\" alt=\"Translate flag\" class=\"my-0 mx-2\">`;
            }

        moment.locale(i18next.language);
        initDateTimepicker(i18next.language);

        if (document.getElementById('ajax_source_example') != null) {
            initAjaxTable(i18next.language);
            initDeleteUser();
        }

        if (document.getElementById('calendar') != null) {
            initCalendar(getCalendarName(i18next.language));
        }

        if (document.getElementById('agenda') != null) {
            initAgenda(getCalendarName(i18next.language));
        }

        if (document.getElementById('listViewEvents') != null) {
            initListViewEvents(getCalendarName(i18next.language));
        }

        if (document.getElementById('googleCalendar') != null) {
            initGoogleCalendar(getCalendarName(i18next.language));
        }
    });
}

/**
 * Initialize lang switcher whith correct menu and flags.
 * @return {Document} Document localize.
 */
function initLangSwitcher() {
    $('.lang').click(function () {
        var lang = $(this).attr('data-lang');
        i18next.init({
            lng: lang
        }, function (t) {
            i18next.changeLanguage(lang);
                    if (document.getElementById('langText') != null) {
            document.getElementById('langText').innerHTML = i18next.t('header.language', {
                what: 'i18next'
            });
                    }
                    if (document.getElementById('langFlag') != null) {
            document.getElementById('langFlag').innerHTML = `<img src=\"assets/img/flags/${getFlagName(i18next.language)}.png\" alt=\"Translate flag\" class=\"my-0 mx-2\">`;
                    }
            // start localizing, details:
            // https://github.com/i18next/jquery-i18next#usage-of-selector-function
            $(document).localize();

            moment.locale(i18next.language);
            initDateTimepicker(i18next.language);

            if (document.getElementById('ajax_source_example') != null) {
                translateAjaxTable(i18next.language);
                initDeleteUser();
            }

        if (document.getElementById('calendar') != null) {
            $('#calendar').fullCalendar('option', 'locale', getCalendarName(i18next.language));
        }

        if (document.getElementById('agenda') != null) {
            $('#agenda').fullCalendar('option', 'locale', getCalendarName(i18next.language));
        }

        if (document.getElementById('listViewEvents') != null) {
            $('#listViewEvents').fullCalendar('option', 'locale', getCalendarName(i18next.language));
        }

        if (document.getElementById('googleCalendar') != null) {
            $('#googleCalendar').fullCalendar('option', 'locale', getCalendarName(i18next.language));
        }
        });


/*
 * queryParameters -> handles the query string parameters
 * queryString -> the query string without the fist '?' character
 * re -> the regular expression
 * m -> holds the string matching the regular expression
 */
var queryParameters = {}, queryString = location.search.substring(1),
    re = /([^&=]+)=([^&]*)/g, m;

// Creates a map with the query string parameters
while (m = re.exec(queryString)) {
    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
}

// Add new parameters or update existing ones
//queryParameters['newParameter'] = 'new parameter';
queryParameters['lng'] = lang;
/*
 * Replace the query portion of the URL.
 * jQuery.param() -> create a serialized representation of an array or
 *     object, suitable for use in a URL query string or Ajax request.
 */
location.search = $.param(queryParameters); // Causes page to reload with new parameters

    });
}

/**
 * Initialize Sparkline widget from class .sparreveal
 */
function initSparkline() {
    $(".sparkreveal").each(function () {

        var myvalues = [1,4,4,7,5,9,10,8,10];

        $(this).sparkline(myvalues, {
            type: 'bar',
            width: '100%',
            height: 35,
            barColor: '#5663b3',
            lineColor: '#0083CD',
            barWidth: 2,
            barSpacing: 5,
            fillColor: false,
            highlightLineColor: 'rgba(0, 0, 0, 0.8)',
            enableTagOptions: true
        });
        $.sparkline_display_visible();
    });
}

/**
 * Initialize text counters from class .counter.
 */
function initCounter() {
    $(".counter").each(function () {
        var $this = $(this),
            countTo = $this.attr('data-count');

        $({
            countNum: $this.text()
        }).animate({
            countNum: countTo
        }, {
            duration: 6000,
            easing: 'linear',
            step: function () {
                $this.text(Math.floor(this.countNum));
            },
            complete: function () {
                $this.text(this.countNum);
            }
        });
    });
}

/**
 * Initialize progress bar counter from class .progress-bar-counter.
 */
function initprogressBarCounter() {
    $(".progress-bar-counter").each(function () {
        var $element = $(this);
        var duration = 6000;
        $element.animate({
            width: $element.attr('aria-valuenow') + '%'
        }, duration);
    });
}

/**
 * Initialize progress circle from class .progress-bar-circle.
 */
function initProgressBarCircle() {
    $(".progress-bar-circle").each(function () {
        var DEFAULTS = {
            backgroundColor: 'bg-light',
            progressColor: 'bg-danger',
            percent: 75,
            duration: 2000
        };
        var $target = $(this);

        var opts = {
            backgroundColor: $target.data('backgroundcolor') ? $target.data('backgroundcolor') : DEFAULTS.backgroundColor,
            progressColor: $target.data('progresscolor') ? $target.data('progresscolor') : DEFAULTS.progressColor,
            percent: $target.data('percent') ? $target.data('percent') : DEFAULTS.percent,
            duration: $target.data('duration') ? $target.data('duration') : DEFAULTS.duration
        };

        $target.append('<div class="background"></div><div class="rotate"></div><div class="left"></div><div class="right"></div><div class=""><span>' + opts.percent + '%</span></div>');

        $target.find('.background').addClass(opts.backgroundColor);
        $target.find('.left').addClass(opts.backgroundColor);
        $target.find('.rotate').addClass(opts.progressColor);
        $target.find('.right').addClass(opts.progressColor);

        var $rotate = $target.find('.rotate');
        setTimeout(function () {
            $rotate.css({
                'transition': 'transform ' + opts.duration + 'ms linear',
                'transform': 'rotate(' + opts.percent * 3.6 + 'deg)'
            });
        }, 1);

        if (opts.percent > 50) {
            var animationRight = 'toggle ' + (opts.duration / opts.percent * 50) + 'ms step-end';
            var animationLeft = 'toggle ' + (opts.duration / opts.percent * 50) + 'ms step-start';
            $target.find('.right').css({
                animation: animationRight,
                opacity: 1
            });
            $target.find('.left').css({
                animation: animationLeft,
                opacity: 0
            });
        }
    });
}

/**
 * @param hex color code.
 * @return rgb color code.
 */
function hex2rgb(hex, opacity) {
    hex = hex.trim();
    hex = hex[0] === '#' ? hex.substr(1) : hex;
    var bigint = parseInt(hex, 16),
        h = [];
    if (hex.length === 3) {
        h.push((bigint >> 4) & 255);
        h.push((bigint >> 2) & 255);
    } else {
        h.push((bigint >> 16) & 255);
        h.push((bigint >> 8) & 255);
    }
    h.push(bigint & 255);
    if (arguments.length === 2) {
        h.push(opacity);
        return 'rgba(' + h.join() + ')';
    } else {
        return 'rgb(' + h.join() + ')';
    }
}

/**
 * Return charts Color beetween scripts.
 * @return {color} Color from our theme (primary ...).
 */
function initCartColors() {
    window.chartColors = {
        white: 'rgb(255, 255, 255)',
        black: 'rgb(0, 0, 0)',
        danger: hex2rgb('#FF5252'),
        warning: hex2rgb('#FF9100'),
        tertiary: hex2rgb('#FF0000'),
        success: hex2rgb('#00E676'),
        primary: hex2rgb('#448AFF'),
        secondary: 'rgb(204, 204, 204)',
        info: hex2rgb('#40C4FF'),
        quaternary: hex2rgb('#FF4081'),
        quinary: hex2rgb('#E040FB'),
        senary: hex2rgb('#8D6E63'),
        septenary: hex2rgb('#64FFDA'),
        octonary: hex2rgb('#536DFE'),
        nonary: hex2rgb('#EEFF41'),
        denary: hex2rgb('#B2FF59'),
        light: 'rgb(242, 242, 242)',
        dark: 'rgb(41, 43, 44)'
    };
}

/**
 * Differents functions for Datatable.
 */
function initFunctionsForTable() {

    var namespace;

    namespace = {

        getFileIcon: function (e) {
            if (e === 'text') {
                return '<i class="fa fa-file-text-o text-primary fa-2x"></i>';
            } else if (e === 'image') {
                return '<i class="fa fa-file-image-o text-danger fa-2x"></i>';
            } else if (e === 'po') {
                return '<i class="fa fa-file-powerpoint-o text-warning fa-2x"></i>';
            } else if (e === 'xs') {
                return '<i class="fa fa-file-excel-o text-success fa-2x"></i>';
            } else if (e === 'pdf') {
                return '<i class="fa fa-file-pdf-o text-warning fa-2x"></i>';
            } else if (e === 'folder') {
                return '<i class="fa fa-folder-o text-primary fa-2x"></i>';
            } else if (e === 'doc') {
                return '<i class="fa fa-file-word-o text-info fa-2x"></i>';
            } else if (e === 'code') {
                return '<i class="fa fa-file-code-o text-primary fa-2x"></i>';
            } else if (e === 'archive') {
                return '<i class="fa fa-file-archive-o text-primary fa-2x"></i>';
            } else if (e === 'video') {
                return '<i class="fa fa-file-video-o text-quinary fa-2x"></i>';
            } else if (e === 'audio') {
                return '<i class="fa fa-file-audio-o text-quaternary fa-2x"></i>';
            } else {
                return '<i class="fa fa-file-text-o text-primary fa-2x"></i>';
            }
        },
        getTicketPriorityBadge: function (e) {
            if (e === 'high') {
                return '<span class="badge badge-pill badge-danger">High</span>';
            } else if (e === 'middle') {
                return '<span class="badge badge-pill badge-primary">Middle</span>';
            } else if (e === 'low') {
                return '<span class="badge badge-pill badge-info">Low</span>';
            }
        },
        getTicketStatusBadge: function (e) {
            if (e === 'open') {
                return '<span class="badge badge-pill badge-danger">Open</span>';
            } else if (e === 'inprogress') {
                return '<span class="badge badge-pill badge-success">In Progress</span>';
            } else if (e === 'answered') {
                return '<span class="badge badge-pill badge-primary">Answered</span>';
            } else if (e === 'onhold') {
                return '<span class="badge badge-pill badge-septenary">On Hold</span>';
            } else if (e === 'closed') {
                return '<span class="badge badge-pill badge-info">Closed</span>';
            }
        },
        getFileSize: function (bytes, decimalPoint) {
            if (bytes == 0) return '0 Bytes';
            var k = 1000,
                dm = decimalPoint || 2,
                sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                i = Math.floor(Math.log(bytes) / Math.log(k));
            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        },

        getEventColor: function (e) {
            if (e === 'lunch') {
                return '#0f9d58';
            } else if (e === 'meeting') {
                return '#448AFF';
            } else if (e === 'vacancy') {
                return '#40C4FF';
            } else if (e === 'sport') {
                return '#536DFE';
            } else if (e === 'doctor') {
                return '#FF4081';
            } else if (e === 'longevent') {
                return '#b3597a';
            } else if (e === 'repeatingevent') {
                return '#FF9100';
            }
        },
        getColorEvent: function (e) {
            if (e === '#0f9d58') {
                return 'Lunch';
            } else if (e === '#448AFF') {
                return 'Meeting';
            } else if (e === '#40C4FF') {
                return 'Vacancy';
            } else if (e === '#E040FB') {
                return 'Sport';
            } else if (e === '#536DFE') {
                return 'Doctor';
            } else if (e === '#FF4081') {
                return 'Long Event';
            } else if (e === '#FF9100') {
                return 'Repeating Event';
            }
        },
    };
    window.ns = namespace;
}

$(document).ready(function () {
    initI18Next();
});

$(window).on("load",function(){

    if (document.getElementById('leftChatUser') != null)
        initChatScrollBar();
     if (document.getElementById('leftSidebar') != null)
        initLeftSidebarScrollBar();

    initXCardAction();
});

(function () {
    "use strict";
    initSidebar();
    initBootstrapFunctions();
    initCollapseIcon();
    initSparkline();
    initCounter();
    initprogressBarCounter();
    initProgressBarCircle();
    initCartColors();
    initFunctionsForTable();
    initLangSwitcher();
    initDragableEvents();
    getTimezones();
    initTimezoneSelector();
    createElementModal();
    if (document.getElementById('submitModalU') != null)
    submitModalEvenet();
    initDropZone();
    initChangeSidebarBackground();
    initChangeHeaderBackground();
    initToggleSidebarMini();

})();
